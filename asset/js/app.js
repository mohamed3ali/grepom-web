var grepomApp = angular.module('grepomApp', []);

grepomApp.controller('CotisationtypesCtrl', function ($scope, $http) {
	
	$http.get('api/cotisationtypes').success(function(data){
		$scope.cotisationtypes = data;
			console.log("# Liste types");
			console.log(data);
	}).error(function(data){
		$scope.cotisationtypes = data;
			console.log("# Error liste types");
			console.log(data);
	});
	
	$scope.refresh = function(){
		$http.get('api/cotisationtypes').success(function(data){
			$scope.cotisationtypes = data;
			console.log("# Liste types");
			console.log(data);
		}).error(function(data){
			$scope.cotisationtypes = data;
			console.log("# Error liste types");
			console.log(data);
		});
	}
	
	$scope.addCotisationtype = function(){
		var newCotisationtype = {cotisation_type: $scope.cotisation_type, valeur: $scope.valeur};
		$http.post('api/cotisationtypes', newCotisationtype).success(function(data){
			$scope.refresh();
			$scope.cotisation_type = '';
			$scope.valeur = '';
		}).error(function(data){
			alert(data.error);
		});
	}

	$scope.deleteCotisationtype = function(cotisationtype){
		$http.delete('api/cotisationtypes/' + cotisationtype.id);
		$scope.cotisationtypes.splice($scope.cotisationtypes.indexOf(cotisationtype),1);
	}
	
	$scope.updateCotisationtype = function(cotisationtype){
		console.log("# put cotisationtype");
		console.log(cotisationtype);
		$http.put('api/cotisationtypes', cotisationtype).success(function(data){
			$scope.refresh();
		}).error(function(cotisationtype){
			alert(data.error);
		});
	}

	$scope.addTask = function(){
		var newTask = {title: $scope.taskTitle};
		$http.post('api/tasks', newTask).success(function(data){
			$scope.refresh();
			$scope.taskTitle = '';
		}).error(function(data){
			alert(data.error);
		});
	}
	
	$scope.deleteTask = function(task){
		$http.delete('api/tasks/' + task.id);
		$scope.tasks.splice($scope.tasks.indexOf(task),1);
	}
	
	$scope.updateTask = function(task){
		$http.put('api/tasks', task).error(function(data){
			alert(data.error);
		});
		$scope.refresh();
	}
	
});