var grepomApp = angular.module('grepomApp', []);

// Cotisation Types >>>>
grepomApp.controller('CotisationtypesCtrl', function ($scope, $http) {
	
	$http.get('/api/cotisationtypes').success(function(data){
		$scope.cotisationtypes = data;
			console.log("# Liste types");
			console.log(data);
	}).error(function(data){
		$scope.cotisationtypes = data;
			console.log("# Error liste types");
			console.log(data);
	});
	
	$scope.refresh = function(){
		$http.get('/api/cotisationtypes').success(function(data){
			$scope.cotisationtypes = data;
			console.log("# Liste types");
			console.log(data);
		}).error(function(data){
			$scope.cotisationtypes = data;
			console.log("# Error liste types");
			console.log(data);
		});
	}
	
	$scope.addCotisationtype = function(){
		var newCotisationtype = {cotisation_type: $scope.cotisation_type, valeur: $scope.valeur};
		$http.post('/api/cotisationtypes', newCotisationtype).success(function(data){
			$scope.refresh();
			$scope.cotisation_type = '';
			$scope.valeur = '';
		}).error(function(data){
			alert(data.error);
		});
	}

	$scope.deleteCotisationtype = function(cotisationtype){
		$http.delete('/api/cotisationtypes/' + cotisationtype.id);
		$scope.cotisationtypes.splice($scope.cotisationtypes.indexOf(cotisationtype),1);
	}
	
	$scope.updateCotisationtype = function(cotisationtype){
		console.log("# put cotisationtype");
		console.log(cotisationtype);
		$http.put('/api/cotisationtypes', cotisationtype).success(function(data){
			$scope.refresh();
		}).error(function(cotisationtype){
			alert(data.error);
		});
	}
});

// Projets >>>>
grepomApp.controller('ProjetsCtrl', function ($scope, $http) {
	
	$http.get('/api/projets').success(function(data){
		$scope.projets = data;
			console.log("# Liste");
			console.log(data);
	}).error(function(data){
		$scope.projets = data;
			console.log("# Error liste");
			console.log(data);
	});
	
	$scope.refresh = function(){
		$http.get('/api/projets').success(function(data){
			$scope.projets = data;
			console.log("# Liste");
			console.log(data);
		}).error(function(data){
			$scope.projets = data;
			console.log("# Error liste");
			console.log(data);
		});
	}
	
	$scope.addProjet = function(){
		var newProjet = {cotisation_type: $scope.cotisation_type, valeur: $scope.valeur};
		$http.post('/api/projets', newProjet).success(function(data){
			$scope.refresh();
			$scope.cotisation_type = '';
			$scope.valeur = '';
		}).error(function(data){
			alert(data.error);
		});
	}

	$scope.deleteProjet = function(projet){
		$http.delete('/api/projets/' + projet.id);
		$scope.projets.splice($scope.projets.indexOf(projet),1);
	}
	
	$scope.updateProjet = function(projet){
		console.log("# put projet");
		console.log(projet);
		$http.put('/api/projets', projet).success(function(data){
			$scope.refresh();
		}).error(function(projet){
			alert(data.error);
		});
	}
});