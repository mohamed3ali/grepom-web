 
var save_method; //for save method string
var table;
 
$(document).ready(function() {
 
    //datatables
    table = $('.dt').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        
        "language": {
              url: '/assets/vendor/datatables/media/js/french.json'
          },
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "/adherent/json",
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 
});
 
 
 
function add_cotisation()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show')
                    .on('shown.bs.modal', function () {
                        $('#numrecu').focus();
                    });  
    $('.modal-title').text('Nouvelle cotisation'); // Set Title to Bootstrap modal title
}
 
function edit_cotisation(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "/cotisations/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            $('[name="id"]').val(data.id);
            $('[name="numrecu"]').val(data.numrecu);
            $('[name="adherent_id"]').val(data.adherent_id);
            $('[name="saison"]').val(data.saison);
            //$('[name="date"]').datepicker('update',data.date);
            $('[name="date"]').val(data.date);
            $('[name="typecotisation_id"]').val(data.typecotisation_id);
            $('[name="modecotisation_id"]').val(data.modecotisation_id);
            $('[name="montant"]').val(data.montant);
            $('[name="receptionpar_id"]').val(data.receptionpar_id);
            $('[name="remarques"]').val(data.remarques);
            $('#modal_form').modal('show')
                    .on('shown.bs.modal', function () {
                        $('#numrecu').focus();
                    }); // show bootstrap modal when complete loaded
            $('.modal-title').text('Modifier'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from application');
        }
    });
}
 
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}
 
function save()
{
    $('#btnSave').text('Enregistrement en cours ...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "/cotisations/ajax_add";
    } else {
        url = "/cotisations/ajax_update";
    }
 
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
 
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Erreur d\'enregistrement des données');
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}
 
function delete_cotisation(id)
{
    if(confirm('Voulez vous vraiment supprimer cette cotisation ?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "/cotisations/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression de la cotisation');
            }
        });
 
    }
}
function getAdherents(str) {
  $.ajax({
        url:'/personnel/adherents_json',
        type:'POST',
        data: 'q=' + str,
        dataType: 'json',
        success: function( json ) {
            $('#myselect').append(json);

        }
    });
};
 