<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	public $data = array();
	
	function __construct()
	{
		parent::__construct();
		
	    if( !$this->session->userdata('isLoggedIn') ) {
	        //redirect('/auth');
	    }

		$this->data["hello"] = "";
		$this->data["menu_procedures"] = null;
		//loading procedures for side-menu
		$this->load->model('procedure_model', "menu_procedures_model");
		if ($this->menu_procedures_model->as_array()->count_rows()){
			$this->data["menu_procedures"] = $this->menu_procedures_model->get_all();
		}
		
		//$this->output->enable_profiler(TRUE);
		//$this->load->model('moderateurs_model');
		//if($this->moderateurs_model->isLoggedIn()){
		//	$this->load->database();
		//} else {
		//	redirect('admin/login?location='.urlencode($_SERVER['REQUEST_URI']),'refresh');
		//}
	}
	
	public function comboarray($tab, $key = "id", $val)
	{
		$rows = R::getAssoc( "SELECT $key, $val FROM $tab" );
		//echo  "SELECT $key, $val FROM $tab<hr>"; print_r($rows);
		return $rows;
	}
	public function data2array($tab)
	{
		$rows = R::getAll( "SELECT * FROM $tab" );
		//echo  "SELECT $key, $val FROM $tab<hr>"; print_r($rows);
		//var_dump($rows);die();
		return $rows;
	}
	
	public function _output_data($outputData = null)
	{
		//var_dump($outputData);die();
		$this->load->view('admin/layout',(array)$outputData);
	}

	public function index()
	{
		redirect('admin/dashboard','refresh');	
	}
	
}