<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $TITLE ?></title>
        <style>
            @page {
                odd-footer-name: html_myFooter1;
                margin-header: 0mm;
                margin-footer: 0mm; 
                margin-top: 2mm;
                margin-bottom: 2mm;
                margin-left: 6mm;
                margin-right: 6mm;
            }
            body {
                font-family: sans-serif;
                font-size: 10pt;
                width: 749px;
            }
            table{border-collapse:collapse;border-spacing:0;}
            #header  #reference{
                float:right;
                text-align:right;
            }
            #header  #reference h3{
                margin:0;
            }
            #header  #reference h4{
                margin:0;
                font-size:85%;
                font-weight:600;
            }
            #header  #reference p{
                margin:0;
                margin-top:2%;
                font-size:85%;
            }
            #header  #logo{
                width:50%;
                float:left;
            }

            #fromto table{
                width: 750px;
            }
            #fromto table tr td.first{
                padding: 2px 5px 0 5px;
                background:#efefef;
            }
            #fromto table tr td.middle{
                width: 2%;
            }
            #fromto table tr td.last{
                padding: 2px 5px 0 5px;
                border-right:1px solid red !important;
                border-left:1px solid red !important;
                background:#efefef;
            }

            #items{
                margin-top: 10px
            }
            #items table {
                width: 750px;
                border:0;
                font-size: 9pt; 
            }
            #items table thead th { 
                background-color: #EEEEEE;
                text-align: center;
                border: 0.1mm solid #000000;
            }
            #items table tbody tr td {
                vertical-align: top; 
                border: 0.1mm solid #000000;
                padding: 3px;
            }

            #items table tfoot tr td {
            }

            #toto{
                margin-top: 10px;
                width: 750px;
            }

            .span{
                border-left: none;
            }
            .spanT{
                border:0.1mm solid #000000;
            }
            .totalTtc{
                background-color: #EEEEEE;
            }
            #note{
                width: 420px;
            }

            #note h4{
                font-size:10px;
                font-weight:600;
                font-style:italic;
                margin-bottom:4px;
            }
            #note p{
                font-size:10px;
                font-style:italic;
            }
            #footer{
                border-top: 1px solid black;
                font-size: 9px;
                text-align: center;
                padding: 0;
                margin: 0;
            }
        </style>
    </head>

    <body>
        <div id="container">
            <div id="header">
                <div id="logo">
                    <?= img("elite.png", ["class" => "user-image", "alt" => "Elite INFO", "style" => "width:100%; max-width:300px;"]) ?>
                </div>
                <div id="reference">
                    <h3><strong>Bon de Livraison</strong></h3>
                    <h4>N°Bon. : <?php echo $doc->nDoc | $doc->nDoc ?></h4>
                    <p>Date de sortie : <?php echo mdate('%d/%m/%Y', strtotime($doc->dateDoc)) ?></p>
                </div>
            </div>
            <div id="fromto">
                <table>
                    <thead>
                        <tr>
                            <th width="369px"></th>
                            <th width="11px"></th>
                            <th width="369px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="first"><h3><?php echo $sce->designation ?></h3></td>
                            <td class="middle"> </td>
                            <td class="last"><h3><?php echo isset($clnt->designation) ? $clnt->designation : '' ?></h3></td>
                        </tr>
                        <tr>
                            <td class="first"><?php echo $sce->codePostale . '    ' . $sce->ville ?></td>
                            <td class="middle"> </td>
                            <td class="last"><?php echo (isset($clnt->codePostale) ? $clnt->codePostale : '' ) . '    ' . (isset($clnt->ville) ? $clnt->ville : '') ?></td>
                        </tr>
                        <tr>
                            <td class="first">Tél.: <?php echo $sce->tel ?></td>
                            <td class="middle"> </td>
                            <td class="last">Tél.: <?php echo isset($clnt->tel) ? $clnt->tel : '' ?></td>
                        </tr>
                        <tr>
                            <td class="first">Email: <?php echo isset($sce->email) ? $sce->email : '' ?></td>
                            <td class="middle"> </td>
                            <td class="last">Email: <?php echo isset($clnt->email) ? $clnt->email : '' ?></td>
                        </tr>
                        <tr>
                            <td class="first">Site Web: <?php echo isset($sce->siteweb) ? $sce->siteweb : '' ?></td>
                            <td class="middle"> </td>
                            <td class="last">Site Web: <?php echo isset($clnt->siteweb) ? $clnt->siteweb : '' ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="items">
                <table class="items">
                    <thead>
                        <tr>
                            <?php
                            if ($ref == 'on') {
                                echo '<th width="100px">Réf</th>';
                            }
                            echo '<th width="">Désignations</th>';
                            if ($qte == 'on') {
                                $colNbr = 1;
                                echo '<th width="100px">Qté</th>';
                            }
                            if ($pu == 'on') {
                                $colNbr+=1;
                                echo ' <th width="100px">P.U.HT</th>';
                            }
                            if ($pt == 'on') {
                                $colNbr+=1;
                                echo '<th width="100px">Prix total HT</th>';
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dtls as $dtl): ?>	
                            <tr class="center">
                                <?php
                                if ($ref == 'on') {
                                    echo '<td align="center">' . $dtl->reference . '</td>';
                                }
                                echo '<td align="left">';
                                echo $dtl->name . '' . (strlen($dtl->nsr) > 0 ? ' : ' . $dtl->nsr . '.' : '.');
                                echo '</td>';

                                if ($qte == 'on') {
                                    echo '<td align="center">' . $dtl->qte . '</td>';
                                }
                                if ($pu == 'on') {
                                    echo '<td align="center">' . $dtl->prixU . '</td>';
                                }
                                if ($pt == 'on') {
                                    echo '<td align="center">' . $dtl->prixTotal . '</td>';
                                }
                                ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div id="toto">
                <div id="note" style="float: left;width: 430px;word-break: break-all;">
                    <?php echo strlen($doc->observation) > 0 ? '<h4>Note(s) :</h4>' : '' ?>
                    <p> 
                        <?php
                        $email_body = str_replace(PHP_EOL, ' ', $doc->observation);
                        $email_body = preg_replace('/[\r\n]+/', '\n', $email_body);
                        $email_body = preg_replace('/[ \t]+/', ' ', $email_body);
                        $email_body = preg_replace('/^\s+|\n|\r|\s+$/m', ' ', $email_body);
                        echo $email_body;
                        ?>
                    </p>                   
                </div>
                <?php
                if ($pt == 'on') {
                    ?>
                    <div style="float: right;width: <?php echo $colNbr == 3 ? 300 : 300 - $colSize ?>px;">
                        <table cellpadding="3" style="width: <?php echo $colNbr == 3 ? 300 : 300 - $colSize ?>px;">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th width="100px"></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <?php
                                if ($doc->remise > 0) {
                                    ?>
                                    <tr>
                                        <td  class="spanT" align="center" >Remise <?php echo $doc->remise ?>%</td>
                                        <td class="spanT" align="center"><?php echo $doc->totalRemise ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <td class="spanT totalTtc" align="center">Total HT</td>
                                    <td class="spanT totalTtc" align="center"><?php echo $doc->totalHT ?></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <?php
                }
                ?>
                <div style="clear: both;"></div>
            </div>
        </div>
    <htmlpagefooter name="myFooter1" style="display:none">
        <div id="footer" class="footer" >
            Société à responsabilité limité (SARL)
            - ICE: <?php echo $sce->iceN ?>
            - IF: <?php echo $sce->ifN ?>
            - Patente: <?php echo $sce->patenteN ?>
            - RC:<?php echo $sce->rcN ?>
            - CNSS:<?php echo $sce->cnssN ?><br>
            Compte Bancaire N°:<?php echo $sce->compteN ?>
            <div style="text-align: center;position: absolute;width: 100%;top:0;">
                Page {PAGENO} de {nb}
            </div>
        </div>
    </htmlpagefooter>
</body>

</html>
