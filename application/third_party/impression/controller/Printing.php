<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Printing extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['active'] = 'doc';
        $this->load->model('Client_m', 'clnt');
        $this->load->model('Echeance_m', 'ech');
        $this->load->model('Modepay_m', 'pay');
        $this->load->model('Doc_m', 'doc');
        $this->load->model('Sligne_m', 'lstk');

        if ($this->users->loggedin() == FALSE) {
            redirect('.', 'refresh');
        }
    }

    public function print_BS() {
        $pdf = true;
        $files = glob("pdf/*.pdf");
        $now = time();
        $this->data['colNbr'] = 0;
        $this->data['colSize'] = 100;
        $this->data['sce'] = $this->clnt->get_by("type='EN'")[0];

        foreach ($files as $file) {
            if (is_file($file)) {
                if ($now - filemtime($file) >= 5) {
                    unlink($file);
                }
            }
        }

        $param = $this->doc->array_from_post(array(
            'idPE',
            'TYPE',
            'ref',
            'qte',
            'pu',
            'pt'
        ));
        $this->data['doc'] = $this->doc->get($param['idPE']);
        $this->data['ref'] = $param['ref'];
        $this->data['qte'] = $param['qte'];
        $this->data['pu'] = $param['pu'];
        $this->data['pt'] = $param['pt'];

        $this->data['clnt'] = $this->clnt->get($this->data['doc']->idEntreprise);
        $this->data['dtls'] = $this->lstk->get_lignes($param['idPE']);

        if ($param['TYPE'] == 'fact') {
            $fact = $this->doc->array_from_post(array(
                'nFacture',
                'dateFacture'
            ));
            $fact['dateFacture'] = date('Y-m-d h:i:s', strtotime(str_replace('/', '-', $fact['dateFacture'])));
            $this->doc->save($fact, $param['idPE']);

            $this->data['echeance'] = $this->ech->get($this->data['doc']->idEcheance);
            $this->data['pay'] = $this->pay->get($this->data['doc']->idModePay);
        }

        if ($pdf == TRUE) {
            $TITLE = $param['TYPE'] . '_' . $this->data['doc']->nDoc;
            $this->data['TITLE'] = $TITLE;
            $PATH = "pdf/" . $TITLE . '.pdf';

            $this->load->library('M_pdf');
            $pdf = $this->m_pdf->load('c', 'A4', '', '', 10, 10, 48, 10, 10, 10); // $mgl,$mgr,$mgt,$mgb,$mgh,$mgf
            $pdf->SetTitle($TITLE);
            $pdf->SetProtection(array('print'));
            $pdf->SetDisplayMode('fullpage');
            $pdf->WriteHTML($this->load->view('view/print/' . $param['TYPE'], $this->data, true));
            $pdf->Output($PATH, "F");
            echo site_url($PATH);
        } else {
            echo site_url('printing/print_BSHTML?id=' . $param['idPE'] . '&typ=' . $param['TYPE'] . '&ref=' . $param['ref'] . '&qte=' . $param['qte'] . '&pu=' . $param['pu'] . '&pt=' . $param['pt']);
        }
    }

    public function print_DMP() {
        $pdf = true;
        $files = glob("pdf/*.pdf");
        $now = time();
        $this->data['colNbr'] = 0;
        $this->data['colSize'] = 100;
        $this->data['sce'] = $this->clnt->get_by("type='EN'")[0];
        $this->data['ref'] ='on' ;
        $this->data['qte'] ='on' ;
        $this->data['pu'] ='off' ;
        $this->data['pt'] ='off' ;

        foreach ($files as $file) {
            if (is_file($file)) {
                if ($now - filemtime($file) >= 5) {
                    unlink($file);
                }
            }
        }

        $param = $this->doc->array_from_post(array(
            'idDoc'
        ));
        $param['TYPE'] = 'DMP';
        $this->data['doc'] = $this->doc->get($param['idDoc']);

        $this->data['clnt'] = $this->clnt->get($this->data['doc']->idEntreprise);
        $this->data['dtls'] = $this->lstk->get_lignes($param['idDoc']);

        $this->data['echeance'] = $this->ech->get($this->data['doc']->idEcheance);
        $this->data['pay'] = $this->pay->get($this->data['doc']->idModePay);

        if ($pdf == TRUE) {
            $TITLE = $param['TYPE'] . '_' . $this->data['doc']->nDoc;
            $this->data['TITLE'] = $TITLE;
            $PATH = "pdf/" . $TITLE . '.pdf';

            $this->load->library('M_pdf');
            $pdf = $this->m_pdf->load('c', 'A4', '', '', 10, 10, 48, 10, 10, 10); // $mgl,$mgr,$mgt,$mgb,$mgh,$mgf
            $pdf->SetTitle($TITLE);
            $pdf->SetProtection(array('print'));
            $pdf->SetDisplayMode('fullpage');
            $pdf->WriteHTML($this->load->view('view/print/' . $param['TYPE'], $this->data, true));
            $pdf->Output($PATH, "F");
            echo site_url($PATH);
        } else {
            echo site_url('printing/print_BSHTML?id=' . $param['idPE'] . '&typ=' . $param['TYPE'] . '&ref=' . $param['ref'] . '&qte=' . $param['qte'] . '&pu=' . $param['pu'] . '&pt=' . $param['pt']);
        }
    }

    public function print_BSHTML() {
        $this->data['id'] = $this->input->get('id');
        $this->data['typ'] = $this->input->get('typ');
        $this->data['ref'] = $this->input->get('ref');
        $this->data['qte'] = $this->input->get('qte');
        $this->data['pu'] = $this->input->get('pu');
        $this->data['pt'] = $this->input->get('pt');

        //log_message('error', print_r($this->data,true));

        $this->data['colNbr'] = 0;
        $this->data['colSize'] = 107;
        $this->data['sce'] = $this->clnt->get_by("type='EN'")[0];
        $this->data['doc'] = $this->doc->get($this->data['id']);
        if ($this->data['doc']) {
            $this->data['clnt'] = $this->clnt->get($this->data['doc']->idEntreprise);
            $this->data['dtls'] = $this->lstk->get_lignes($this->data['id']);
            $this->data['TITLE'] = $this->data['typ'] . '_' . $this->data['doc']->nDoc;
            $this->data['subview'] = 'view/print/' . $this->data['typ'];
            $this->load->view('print_layout', $this->data);
        }
    }

}
