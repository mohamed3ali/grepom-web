<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('build_menu'))
{
	function build_menu ( $menu )
	{
		$out = "\n".'<ul class="tabs">
				<li><a href="'.base_url().'"><span>Accueil</span></a></li>' . "\n";
		//$out = '';
		for ( $i = 0; $i < count ( $menu ); $i++ )
		{
			//if ( is_array ( $menu [ $i ] ) ) {//must be by construction but let's keep the errors home
				if ( $menu [$i]->parent == 0 ) {//are we allowed to see this menu?
					$out .= '<li><a href="'. base_url().index_page().'page/afficher/'. $menu [$i]->id . '"><span>';
					$out .= $menu [$i]->titre;
					$out .= '</span></a>';
					$out .= get_childs ( $menu, $menu[$i]->id );//loop through childs
					$out .= '</li>' . "\n";
				}
			//}
			//else {
			//	die ( sprintf ( 'menu nr %s must be an array', $i ) );
			//}
		}
		$out .= '<li><a href="'.base_url().index_page().'contact"><span>Contact</span></a></li></ul>'."\n";
		return $out;
	}
}

if ( ! function_exists('get_childs'))
{	
	
	function get_childs ( $menu, $el_id )
	{
		$has_subcats = FALSE;
		$out = '';
		$out .= "\n".'<ul class="dropdown">' . "\n";
		for ( $i = 0; $i < count ( $menu ); $i++ )
		{
			if ( $menu [ $i ]->parent == $el_id ) {//are we allowed to see this menu?
				$has_subcats = TRUE;
				$out .= '<li><a href="'. base_url().index_page().'page/afficher/'. $menu [$i]->id . '"><span>';
				$out .= $menu [$i]->titre;
				$out .= '</span></a>';
				//$out .= $this->get_childs ( $menu, $i );
				$out .= '</li>' . "\n";
			}
		}
		$out .= '</ul>'."\n";
		return ( $has_subcats ) ? $out : FALSE;
	}

}