<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1"/>
    <meta name="msapplication-tap-highlight" content="no">
    
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="GREPOM">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="GREPOM">

    <meta name="theme-color" content="#4C7FF0">
    
    <title></title>

    <!-- page stylesheets -->
    <!-- end page stylesheets -->

    <!-- build:css({.tmp,app}) styles/app.min.css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/css/bootstrap.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/pace/themes/blue/pace-theme-minimal.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/animate.css/animate.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/app.css" id="load_styles_before"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/app.skins.css"/>
    <!-- endbuild -->
  </head>
  <body>

    <div class="app no-padding no-footer layout-static">
      <div class="session-panel">
        <div class="session">
          <div class="session-content">
            <div class="card card-block form-layout">
              <!--form role="form" action="index.html" id="validate"-->
               <?php				
      						if(validation_errors()){
      							echo '<div class="notification attention png_bg">
      								<div>'.
      									validation_errors().@$error_credentials.
      								'</div>
      							</div>';
      						}
      					?>
               <?php 
               
		       		if($this->input->get('location')){
		            	echo form_open(base_url().'auth/check?location='.$this->input->get('location'), array('name' => 'form_login', 'id' => 'form_login'));
		       		} else {
		       			  echo form_open(base_url().'auth/check', array('name' => 'form_login', 'id' => 'form_login'));
		       		}
                
               ?>
                <div class="text-xs-center m-b-3">
                  <img src="<?php echo base_url(); ?>assets/images/logo-icon.png" height="80" alt="" class="m-b-1"/>
                  <h5>
                    Authentification
                  </h5>
                  <p class="text-muted">
                    Merci d'utiliser votre email et mot de passe pour vous connecter
                  </p>
                  <?php echo validation_errors(); ?>
                </div>
                <fieldset class="form-group">
                  <label for="username">
                    Email
                  </label>
                  <input type="text" class="form-control form-control-lg" id="login" name="login" placeholder="Email" required/>
                </fieldset>
                <fieldset class="form-group">
                  <label for="password">
                    Mot de passe
                  </label>
                  <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Mot de passe" required/>
                </fieldset>
                <!--label class="custom-control custom-checkbox m-b-1">
                  <input type="checkbox" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">Garder ma session ouverte</span>
                </label-->
                <br/>
                <button class="btn btn-primary btn-block" type="submit">
                  Se connecter
                </button>
              </form>
            </div>
          </div>
          <footer class="text-xs-center p-y-1">
            <!--p>
              <a href="/auth/lostpass">
                Mot de passe oublié ?
              </a>
              &nbsp;&nbsp;·&nbsp;&nbsp;
              <a href="/auth/register">
                Inscription
              </a>
            </p-->
          </footer>
        </div>

      </div>
    </div>

    <!-- build:js({.tmp,app}) scripts/app.min.js -->
    <script src="<?php echo base_url(); ?>assets/vendor/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/pace/pace.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/tether/dist/js/tether.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/fastclick/lib/fastclick.js"></script>
    <script src="<?php echo base_url(); ?>assets/scripts/constants.js"></script>
    <script src="<?php echo base_url(); ?>assets/scripts/main.js"></script>
    <!-- endbuild -->

    <!-- page scripts -->
    <script src="<?php echo base_url(); ?>assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- end page scripts -->

    <!-- initialize page scripts -->
    <script type="text/javascript">
      $('#validate').validate();
    </script>
    <!-- end initialize page scripts -->
    
  </body>
</html>
