
            <div class="card">
              <div class="card-block"> 

                <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#one" role="tab">Matériel</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#two" role="tab">Documentation</a>
                  </li>
                </ul>

                <div class="tab-content">
                  <div class="tab-pane active" id="one" role="tabpanel">
                    <table id="mattable" class="dt table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Intitulé</th>
                                <th>Caractéristique</th>
                                <th>Catégorie</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
             
                        <!--tfoot>
                        </tfoot-->
                    </table>
                  </div>
                  <div class="tab-pane" id="two" role="tabpanel">
                    <table id="doctable" class="dt table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Référence</th>
                                <th>Titre</th>
                                <th>Catégorie</th>
                                <th>Année</th>
                                <th>Auteurs</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
             
                        <!--tfoot>
                        </tfoot-->
                    </table>
                  </div>
                </div>
                
              </div>
            </div>
        
        
<?php if (isset($zonemodals)) {$this->load->view($zonemodals);} ?>
