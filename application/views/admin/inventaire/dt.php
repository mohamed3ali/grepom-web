<script type="text/javascript">
var save_method; //for save method string
var batable;
var ctable;
var entity_id;
 
$(document).ready(function() {
    mattable = $('#mattable').DataTable({
        "language": {
              url: "<?php echo base_url(); ?>assets/vendor/datatables/media/js/french.json"
        },
        dom:'Blfrtip',
        buttons: [
            {
                text: '<i class="glyphicon glyphicon-plus"></i> Ajouter',
                className:'btn btn-success dataTables_btn',
                action: function ( e, dt, node, config ) {
                    add_entity();
                }
            }
        ],
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Tout']
        ],
        'ajax': "<?php echo base_url(uri_string()); ?>/jsonm",
      });
      
    doctable = $('#doctable').DataTable({
        "language": {
              url: "<?php echo base_url(); ?>assets/vendor/datatables/media/js/french.json"
        },
        dom:'Blfrtip',
        buttons: [
            {
                text: '<i class="glyphicon glyphicon-plus"></i> Ajouter',
                className:'btn btn-success dataTables_btn',
                action: function ( e, dt, node, config ) {
                    add_docentity();
                }
            }
        ],
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Tout']
        ],
        'ajax': "<?php echo base_url(uri_string()); ?>/jsond",
      });
});


function reload_table()
{
    mattable.ajax.reload(null,false); //reload datatable ajax 
    doctable.ajax.reload(null,false); //reload datatable ajax 
}

$('#maincat_id').on('change',function(){
    var maincat_id = $(this).val();
    if(maincat_id){
        $.ajax({
            type:'POST',
            url:'<?php echo base_url(uri_string()); ?>/souscatmat/'+maincat_id,
            //data:'pays_id='+countryID,
            success:function(html){
                $('#cat_id').remove();
                $('#cat_id_div').html(html);
            }
        }); 
    }else{
        //$('#ville_id').html('<option value="">Select category first</option>');
    }
});
$('#doc_maincat_id').on('change',function(){
    var maincat_id = $(this).val();
    if(maincat_id){
        $.ajax({
            type:'POST',
            url:'<?php echo base_url(uri_string()); ?>/souscatdoc/'+maincat_id,
            //data:'pays_id='+countryID,
            success:function(html){
                $('#modal_docform .cat_id').remove();
                $('#modal_docform .cat_id_div').html(html);
            }
        }); 
    }else{
        //$('#ville_id').html('<option value="">Select category first</option>');
    }
});


function add_entity()
{
    save_method = 'add';
    $('#form-mat')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('.demission-group').hide(); // clear error string
    $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#numrecu').focus();
                    //});  
    $('.modal-title').text('Nouveau matériel'); // Set Title to Bootstrap modal title
}

function add_docentity()
{
    save_method = 'add';
    $('#form-doc')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('.demission-group').hide(); // clear error string
    $('#modal_docform').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#numrecu').focus();
                    //});  
    $('.modal-title').text('Nouveau document'); // Set Title to Bootstrap modal title
}

function save()
{
    $('#btnSave').text('Enregistrement en cours ...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo base_url(uri_string()); ?>/ajax_add_mat";
    } else {
        url = "<?php echo base_url(uri_string()); ?>/ajax_update_mat/"+entity_id;
    }
 
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form-mat').serialize(),
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
 
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Erreur d\'enregistrement des données');
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}

function saveDoc()
{
    $('#btnSave').text('Enregistrement en cours ...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo base_url(uri_string()); ?>/ajax_add_doc";
    } else {
        url = "<?php echo base_url(uri_string()); ?>/ajax_update_doc/"+entity_id;
    }
 
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form-doc').serialize(),
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_docform').modal('hide');
                reload_table();
            }
 
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Erreur d\'enregistrement des données');
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}

function delete_entity(id)
{
    if(confirm('Voulez vous vraiment supprimer ?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo base_url(uri_string()); ?>/ajax_delete_mat/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression!');
            }
        });
 
    }
} 

function delete_doc_entity(id)
{
    if(confirm('Voulez vous vraiment supprimer ?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo base_url(uri_string()); ?>/ajax_delete_doc/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_docform').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression!');
            }
        });
 
    }
} 

function edit_entity(id)
{
    save_method = 'update';
    $('#form-mat')[0].reset(); // reset form on modals
    $('.demission-group').show(); // clear error string
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url(uri_string()); ?>/ajax_edit_mat/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            entity_id = data.id;
            $('#code').val(data.code);
            $('#intitule').val(data.intitule);
            $('#caracteristique').val(data.caracteristique);
            $('#maincat_id').val(data.matcat.cat_id);
            $('#cat_id').val(data.cat_id);

            $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#cin').focus();
                    //}); // show bootstrap modal when complete loaded
            $('.modal-title').text("Mise à jour du membre"); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from application');
        }
    });
}
 
function edit_doc_entity(id)
{
    save_method = 'update';
    $('#form-doc')[0].reset(); // reset form on modals
    $('.demission-group').show(); // clear error string
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url(uri_string()); ?>/ajax_edit_doc/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            entity_id = data.id;
            $('#ref').val(data.ref);
            $('#titre').val(data.titre);
            $('#annee').val(data.annee);
            $('#auteurs').val(data.auteurs);
            $('#doc_maincat_id').val(data.doccat.cat_id);
            $('#doc_cat_id').val(data.cat_id);

            $('#modal_docform').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#cin').focus();
                    //}); // show bootstrap modal when complete loaded
            $('.modal-title').text("Mise à jour du document"); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from application');
        }
    });
}
 
</script>