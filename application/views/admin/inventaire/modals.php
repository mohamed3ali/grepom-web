<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nouveau matériel</h4>
            </div>
            <div class="modal-body form">
              <form id="form-mat" action="#">
                <div class="row">
                  
                  <div class="col-xs-6">
                    <fieldset class="form-group">
                      <label for="">Catégorie</label>
                      <?php echo form_dropdown('maincat_id', $comboCat, '', 'class="form-control" id="maincat_id"'); ?>
                    </fieldset>
                  </div>
                  <div class="col-xs-6">
                    <fieldset class="form-group">
                      <label for="">Sous catégorie</label>
                      <div id="cat_id_div"><?php echo form_dropdown('cat_id', $comboSouscat, '', 'class="form-control" id="cat_id"'); ?></div>
                    </fieldset>
                  </div>
                  
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Code</label>
                      <input type="text" class="form-control" id="code" name="code">
                    </fieldset>
                  </div>
                  <div class="col-xs-9">
                    <fieldset class="form-group">
                      <label for="">Intitulé</label>
                      <input type="text" class="form-control" id="intitule" name="intitule">
                    </fieldset>
                  </div>
                  
                  <div class="col-xs-12">
                    <fieldset class="form-group">
                      <label for="">Caractéristiques</label>
                      <input type="text" class="form-control" id="caracteristique" name="caracteristique">
                    </fieldset>
                  </div>


                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modal_docform" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nouveau matériel</h4>
            </div>
            <div class="modal-body form">
              <form id="form-doc" action="#">
                <div class="row">
                  
                  <div class="col-xs-6">
                    <fieldset class="form-group">
                      <label for="">Catégorie</label>
                      <?php echo form_dropdown('maincat_id', $comboDocCat, '', 'class="form-control" id="doc_maincat_id"'); ?>
                    </fieldset>
                  </div>
                  <div class="col-xs-6">
                    <fieldset class="form-group">
                      <label for="">Sous catégorie</label>
                      <div id="cat_id_div" class="cat_id_div"><?php echo form_dropdown('cat_id', $comboDocSouscat, '', 'class="form-control doc_cat_id" id="doc_cat_id"'); ?></div>
                    </fieldset>
                  </div>
                  
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Référence</label>
                      <input type="text" class="form-control" id="ref" name="ref">
                    </fieldset>
                  </div>
                  <div class="col-xs-9">
                    <fieldset class="form-group">
                      <label for="">Titre</label>
                      <input type="text" class="form-control" id="titre" name="titre">
                    </fieldset>
                  </div>
                  
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Année</label>
                      <input type="text" class="form-control" id="annee" name="annee">
                    </fieldset>
                  </div>
                  <div class="col-xs-9">
                    <fieldset class="form-group">
                      <label for="">Auteurs</label>
                      <input type="text" class="form-control" id="auteurs" name="auteurs">
                    </fieldset>
                  </div>


                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="saveDoc()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->