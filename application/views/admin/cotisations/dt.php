<script type="text/javascript">
var save_method; //for save method string
var table;
var entity_id;
 
$(document).ready(function() {
    table = $('.dt').DataTable({
        "language": {
              url: "<?php echo base_url(); ?>assets/vendor/datatables/media/js/french.json"
        },
        dom:'<"dataTables_btns">Blfrtip',
        buttons: [
            {
                text: '<h4><?php if (isset($titre)) {echo $titre;} ?> :</h4>',
                className:'titre-tag'
            },
            {
                text: '<i class="glyphicon glyphicon-plus"></i> Ajouter',
                className:'btn btn-success dataTables_btn',
                action: function ( e, dt, node, config ) {
                    add_entity();
                }
            }
        ],
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Tout afficher']
        ],
        "order": [[ 2, "desc" ], [ 0, "desc" ]],
        'ajax': "<?php echo base_url(uri_string()); ?>/json",
    });
    table.sort(2);
});

<?php
    //var_dump($this->session->userdata('popup-id'));
    
    if (is_numeric($this->session->userdata('popup-id'))){
        echo 'add_entity('.$this->session->userdata('popup-id').');';
        $this->session->unset_userdata(array('popup-id'  => ''));
        $this->session->unset_userdata('popup-id');
        //var_dump($this->session->userdata('popup-id'));
    }
?>

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function nouvelle_annee()
{
    var n_annee = prompt("Nouvelle année:");
    if (/\d{4}/.test(n_annee)) {
        //alert(n_annee);
        $.ajax({
            //type:'POST',
            url:'<?php echo base_url(uri_string()); ?>/nouvelle_annee/'+n_annee,
            //data:'pays_id='+countryID,
            success:function(html){
                alert("l'année a été ajouté.");
                $('#annee_id').remove();
                $('#annee_id_div').html(html);
                return false;
            },
            error:function(html){
                alert("l'année n'a été ajouté.");
                return false;
            },
            complete:function(html){
                //alert("complete");
                return false;
            },
        }); 
    } else {
        alert("Veuillez saisir une année sur 4 chiffres.");
        //return null;
    }

    return false;
}

function add_entity(adh = null)
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('.demission-group').hide(); // clear error string
    $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#numrecu').focus();
                    //});
    $('.modal-title').text('Nouvelle cotisation'); // Set Title to Bootstrap modal title
}

function save()
{
    $('#btnSave').text('Enregistrement en cours ...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo base_url(uri_string()); ?>/ajax_add";
    } else {
        url = "<?php echo base_url(uri_string()); ?>/ajax_update/"+entity_id;
    }
 
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
 
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Erreur d\'enregistrement des données');
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}

function delete_entity(id)
{
    if(confirm('Voulez vous vraiment supprimer ?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo base_url(uri_string()); ?>/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression!');
            }
        });
 
    }
} 

function edit_entity(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.demission-group').show(); // clear error string
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url(uri_string()); ?>/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            entity_id = data.id;
            $('#numrecu').val(data.numrecu);
            $('#adherent_id').val(data.adherent_id);
            $('#annee_id').val(data.annee_id);
            $('#cotisationtype_id').val(data.cotisationtype_id);
            $('#modepaiement_id').val(data.modepaiement_id);
            $('[name="montant"]').val(data.montant);
            $('#receptionpar_id').val(data.receptionpar_id);
            $('[name="date_cotisation"]').val(data.date_cotisation);
            $('[name="remarques"]').val(data.remarques);

            $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#cin').focus();
                    //}); // show bootstrap modal when complete loaded
            $('.modal-title').text("Modifier la cotisation"); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from application');
        }
    });
}
 
</script>