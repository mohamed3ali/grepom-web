<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h6 class="modal-title">Nouvelle cotisation</h6>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                <div class="row">
                  <div class="col-xs-3">
                      <fieldset class="form-group">
                        <label for="">N° reçu</label>
                        <input type="text" class="form-control" id="numrecu" name="numrecu">
                      </fieldset>
                  </div>
      
                  <div class="col-xs-3">
                      <fieldset class="form-group">
                        <label for="">N° Adhérent</label>
                        <?php echo form_dropdown('adherent_id', $comboAdherents, '', 'class="form-control" id="adherent_id"'); ?>
                      </fieldset>
                  </div>
      
                  <div class="col-xs-2">
                      <fieldset class="form-group">
                        <label for="">Année</label>
                        <div class="row">
                          <div id="annee_id_div" class="col-xs-8 p-r-0" style="display: inline;">
                            <?php echo form_dropdown('annee_id', $comboAnneesCotisation, '', 'class="form-control" id="annee_id"'); ?>
                          </div>
                          <div class="col-xs-4 p-l-0"><button type="submit" class="btn btn-outline-primary btn-smaller" onclick="return nouvelle_annee()">+</button></div>
                        </div>
                      </fieldset>
                  </div>
                  
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">Type</label>
                      <?php echo form_dropdown('cotisationtype_id', $comboTypesCotisation, '', 'class="form-control" id="cotisationtype_id"'); ?>
                    </fieldset>
                  </div>
                  
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Montant</label>
                      <div class="input-group">
                          <input type="text" class="form-control" id="montant" name="montant">
                          <div class="input-group-addon p-t-0 p-b-0" >Dh</div>
                      </div>
                    </fieldset>
                  </div>

                  <div class="col-xs-5">
                    <fieldset class="form-group">
                      <label for="">Mode</label>
                      <?php echo form_dropdown('modepaiement_id', $comboModesPaiement, '', 'class="form-control" id="modepaiement_id"'); ?>
                    </fieldset>
                  </div>
            
                  <fieldset class="form-group col-xs-4">
                    <label for="">Date de paiement</label>
                    <input type="date" class="form-control datepicker" id="date_cotisation" name="date_cotisation">
                  </fieldset>
              
                  <fieldset class="form-group col-xs-8">
                    <label for="exampleTextarea">Remarques</label>
                    <textarea class="form-control" id="remarques" name="remarques" rows="5"></textarea>
                  </fieldset>

                  <fieldset class="form-group col-xs-4">
                    <label for="">Reçu par</label>
                    <?php echo form_dropdown('receptionpar_id', $comboPersonnes, '', 'class="form-control" id="receptionpar_id"'); ?>
                  </fieldset>
                  
                </div>
              </form>
            </div>
            <div class="modal-footer" style="padding: 5px 15px;">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->