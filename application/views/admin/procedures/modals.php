<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nouveau paragraphe</h4>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                      <div class="row">
                        <!--div class="col-xs-4">
                          <fieldset class="form-group">
                            <label for="">Type</label>
                            <?php //echo form_dropdown('procedure_id', $procedureFonctions, '', 'class="form-control" id="procedure_id"'); ?>
                          </fieldset>
                        </div-->
                        <input type="hidden" class="form-control" id="procedure_id" name="procedure_id" value="<?php echo $pid; ?>">

                        <div class="col-xs-12">
                          <fieldset class="form-group">
                            <label for="">Titre</label>
                                <input type="text" class="form-control" id="titre" name="titre">
                          </fieldset>
                        </div>
                        
                        <fieldset class="form-group col-xs-12">
                          <label for="exampleTextarea">Contenu</label>
                          <!--textarea class="form-control" id="contenu" name="contenu" rows="5"></textarea-->
                          <textarea class="summernote form-control" id="contenu" name="contenu" rows="5"></textarea>

                        </fieldset>
                      </div>  
              </form>
            </div>


            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->