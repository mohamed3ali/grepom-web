<div class="card">
    <div class="card-header no-bg b-a-0">
        <h3><?php if (isset($procedure)) {echo $procedure->nom_procedure;} ?></h3>
        <p class="m-b-0">
            <?php if (isset($procedure)) {echo $procedure->description_procedure;} ?>
            <a href="javascript:;" onclick="add_entity()" class="btn btn-outline-info m-r-xs" style="float:right;">Ajouter</a>        
        </p>
    </div>
    <div class="card-block">
        <div id="accordion" role="tablist" aria-multiselectable="true">
            
                <?php
                if(isset($procedure->chapitres) && $procedure->chapitres){
                    //var_dump($procedure); die();
                    $i = 1;
                    foreach ($procedure->chapitres as $chapitre) {
                ?>
            <div class="card panel panel-default m-b-xs paragraphe<?php echo $chapitre->id; ?>">
                <div class="card-header panel-heading" role="tab" id="heading<?php echo $chapitre->id; ?>">
                    <h6 class="panel-title m-a-0">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $chapitre->id; ?>" aria-expanded="<?php echo ($i)?'true':'false'; ?>" aria-controls="collapse<?php echo $chapitre->id; ?>" <?php echo ($i)?'class="collapsed"':'class=""'; ?>>
                            <?php echo $chapitre->titre; ?>
                        </a>
                    </h6>
                </div>
                <div id="collapse<?php echo $chapitre->id; ?>" class="card-block panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $chapitre->id; ?>" aria-expanded="false" style="height: 0px;">
                    <?php echo $chapitre->contenu; ?>
                    
                    <a href="javascript:;" onclick="edit_entity(<?php echo $chapitre->id; ?>)" class="btn btn-outline-warning btn-sm m-r-xs" style="float:right;">Modifier</a>
                    <a href="javascript:;" onclick="delete_entity(<?php echo $chapitre->id; ?>)" class="btn btn-outline-danger btn-sm m-r-xs" style="float:right;">Supprimer</a>
                </div>
            </div>
            
                <?php
                    $i = null;
                  }
                }
                ?>
        </div>
    </div>
</div>
        
<?php if (isset($zonemodals)) {$this->load->view($zonemodals);} ?>