<script type="text/javascript">
var save_method; //for save method string
var table;
var entity_id;
 
$.fn.dataTable.ext.buttons.reload = {
    text: 'Reload',
    action: function ( e, dt, node, config ) {
        dt.ajax.reload();
    }
};

$('#pays_id').on('change',function(){
    var countryID = $(this).val();
    if(countryID){
        $.ajax({
            type:'POST',
            url:'<?php echo base_url(uri_string()); ?>/ville/'+countryID,
            //data:'pays_id='+countryID,
            success:function(html){
                $('#ville_id').remove();
                $('#ville_id_div').html(html);
            }
        }); 
        $.ajax({
            type:'POST',
            url:'<?php echo base_url(uri_string()); ?>/indicatif/'+countryID,
            //data:'pays_id='+countryID,
            success:function(html){
                $('#indicatif').val(html);
            }
        }); 
    }else{
        //$('#ville_id').html('<option value="">Select country first</option>');
    }
});

$(document).ready(function() {
    table = $('.dt').DataTable({
        language: {
              url: "<?php echo base_url(); ?>assets/vendor/datatables/media/js/french.json"
        },
        dom:'<"dataTables_btns">Blfrtip',
        buttons: [
            {
                text: '<h4><?php if (isset($titre)) {echo $titre;} ?> :</h4>',
                className:'titre-tag'
            },
            {
                extend: 'print',
                text: '<i class="glyphicon glyphicon-plus"></i> Imprimer',
                className:'btn btn-success dataTables_btn hidden-print',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6]
                },
                //autoPrint: false
            },
            {
                extend: 'excel',
                text: '<i class="glyphicon glyphicon-plus"></i> Exporter',
                className:'btn btn-success dataTables_btn hidden-print',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6]
                }
            },
            /*{
                text: '<i class="glyphicon glyphicon-plus"></i> Imprimer',
                className:'btn btn-success dataTables_btn hidden-print',
                action: function ( e, dt, node, config ) {
                    window.print();
                }
            },*/
            {
                text: '<i class="glyphicon glyphicon-plus"></i> Ajouter',
                className:'btn btn-success dataTables_btn hidden-print',
                action: function ( e, dt, node, config ) {
                    add_entity();
                }
            }
        ],
        lengthMenu: [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Tout afficher']
        ],
        ajax: "<?php echo base_url(uri_string()); ?>/json",
      });
      
      //$(".dataTables_btns").html('<button class="btn btn-success" onclick="add_entity()"><i class="glyphicon glyphicon-plus"></i> Ajouter</button>');
});

/*$(".dataTables_btns").ready(function() {
    alert("test");
    $(".dataTables_btns").html('<button class="btn btn-success" onclick="add_entity()"><i class="glyphicon glyphicon-plus"></i> Ajouter</button>');
});*/

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function add_entity()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('#date_resiliation').attr('disabled',true); //Désactiver la date de résiliation 
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('.supp_photo_icon').hide();

    $('select[name^="pays_id"] option:selected').attr("selected",null);
    $('select[name^="pays_id"] option[value="MA"]').attr("selected","selected");
    $('select[name^="pays_id"] option[value="MA"]').prop("selected","selected");
    $('select[name^="pays_id"]').val("MA").change();
    
    $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#numrecu').focus();
                    //});  
    $('.modal-title').text('Adhérents: Ajouter'); // Set Title to Bootstrap modal title
}

function show_adh(id)
{
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url(uri_string()); ?>/fiche/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            entity_id = data.id;
            my_photo = data.personne.photo?data.personne.photo:"no-photo.png";
            $('#fiche_img').attr('src', '/uploads/photos/'+my_photo);
            $("#fiche_nom").html(data.personne.nom+" "+data.personne.prenom);
            $("#fiche_num").html(data.num_adh);
            dr = data.date_resiliation?" jusqu'au "+data.date_resiliation.substr(0, 10):'';
            $("#fiche_periode").html(data.date_adhesion.substr(0, 10) + dr);
            $("#fiche_type").html(data.adherenttype.type_adh);
            if (data.personne.profession) $("#fiche_profession").html(data.personne.profession.nom_profession);
            $("#fiche_pi").html(data.personne.identitetype.type_identite);
            $("#fiche_ni").html(data.personne.cin);
            $("#fiche_email").html(data.personne.email);
            $("#fiche_tel").html(data.personne.indicatif+data.personne.tel);
            $("#fiche_tel").html(data.personne.indicatif+data.personne.tel);
            $("#fiche_pays").html(data.personne.pays.name);
            $("#fiche_ville").html(data.personne.ville_id);
            $("#fiche_adresse").html(data.personne.adresse + "<br>" + data.personne.ville_id + ", " + data.personne.pays.Name);

            $('#modal_fiche').modal('show'); 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from application');
        }
    });
}

function carte_status(id,status)
{
    //alert(status);
    $('#carteid').val(id);
    entity_id = id;
    $('input[name="carte"]').prop('checked', false);
    $("#carte"+status).prop('checked', true);
    $('#modal_carte').modal('show');
}

function save_carte()
{
    $('#btnSave').text('Enregistrement en cours ...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url = "<?php echo base_url(uri_string()); ?>/carte_status/"+entity_id;
   
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#formcarte').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_carte').modal('hide');
                reload_table();
            }
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Erreur d\'enregistrement des données');
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
        }
    });
}


$uploadCrop = $('#image_preview0crop').croppie({
    enableExif: true,
    viewport: {
        width: 190,
        height: 170,
        //type: 'circle'
    },
    boundary: {
        width: 180,
        height: 160
    }
});

$('#photo0Crop').on('change', function () { 
    var reader = new FileReader();
    reader.onload = function (e) {
        $uploadCrop.croppie('bind', {
            url: e.target.result
        }).then(function(){
            console.log('jQuery bind complete');
        });
        
    }
    reader.readAsDataURL(this.files[0]);
});

$("#photo").change(function() {
    //$("#message").empty(); // To remove the previous error message
    var photo = this.files[0];
    var imagefile = photo.type;
    var match= ["image/jpeg","image/png","image/jpg"];
    if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
    {
        $('#previewing').attr('src','noimage.png');
        //$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
        alert("Erreur de type de fichier. Merci de choisir une photo !")
        return false;
    }
    else
    {
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
    }
});

function imageIsLoaded(e) {
    $("#file").css("color","green");
    $('#image_preview').css("display", "block");
    $('#previewing').attr('src', e.target.result);
    $('#previewing').attr('width', '180px');
    $('#previewing').attr('height', '180px');
};

function supp_photo() {
    if(confirm('Voulez vous vraiment supprimer la photo?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo base_url(uri_string()); ?>/ajax_supp_photo/"+entity_id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#previewing').attr('src', '/uploads/photos/no-photo.png');
                alert('La photo a été supprimée !')
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression!');
            }
        });
 
    }
};

function save()
{
    $('#btnSave').text('Enregistrement en cours ...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo base_url(uri_string()); ?>/ajax_add";
    } else {
        url = "<?php echo base_url(uri_string()); ?>/ajax_update/"+entity_id;
    }
 
    // ajax adding data to database

        //console.log($('#form')[0]);
        //$.ajax({
        //    url: "/ajaxpro.php",
        //    type: "POST",
        //    data: {"image":resp},
        //    success: function (data) {
        //        html = '<img src="' + resp + '" />';
        //        $("#upload-demo-i").html(html);
        //    }
        //});
        //$('#photo')[0].setAttribute("value", resp);
        profileData = new FormData($('#form')[0]);
        //profileData.set('photo', resp, 'photo.jpg');
        $.ajax({
            url : url,
            type: "POST",
            data: profileData,//$('#form').serialize(),
            //dataType: "JSON",
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,
            success: function(data)
            {
     
                //if(data.status){ //if success close modal and reload ajax table
                
                    entity_id = null;
                    $('#previewing').attr('src', '/uploads/photos/no-photo.png');
                    $('#modal_form').modal('hide');
                    reload_table();
                //}
     
                $('#btnSave').text('Enregistrer'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
     
     
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur d\'enregistrement des données');
                $('#btnSave').text('Enregistrer'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
     
            }
        });
}

function savecrop()
{
    $('#btnSave').text('Enregistrement en cours ...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo base_url(uri_string()); ?>/ajax_add";
    } else {
        url = "<?php echo base_url(uri_string()); ?>/ajax_update/"+entity_id;
    }
 
    // ajax adding data to database
    $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (resp) {

        //console.log($('#form')[0]);
        //$.ajax({
        //    url: "/ajaxpro.php",
        //    type: "POST",
        //    data: {"image":resp},
        //    success: function (data) {
        //        html = '<img src="' + resp + '" />';
        //        $("#upload-demo-i").html(html);
        //    }
        //});
        $('#photo')[0].setAttribute("value", resp);
        profileData = new FormData($('#form')[0]);
        //profileData.set('photo', resp, 'photo.jpg');
        $.ajax({
            url : url,
            type: "POST",
            data: profileData,//$('#form').serialize(),
            //dataType: "JSON",
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,
            success: function(data)
            {
     
                //if(data.status){ //if success close modal and reload ajax table
                
                    entity_id = null;
                    $('#previewing').attr('src', '/uploads/photos/no-photo.png');
                    $('#modal_form').modal('hide');
                    reload_table();
                //}
     
                $('#btnSave').text('Enregistrer'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
     
     
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur d\'enregistrement des données');
                $('#btnSave').text('Enregistrer'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
     
            }
        });
    });
}

function delete_entity(id)
{
    if(confirm('Voulez vous vraiment supprimer ?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo base_url(uri_string()); ?>/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                entity_id = null;
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression!');
            }
        });
 
    }
} 

function edit_entity(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    $('#date_resiliation').attr('disabled',false); //Désactiver la date de résiliation 
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url(uri_string()); ?>/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            entity_id = data.id;
            $('[name="identitetype_id"]').val(data.personne.identitetype_id);
            $('[name="cin"]').val(data.personne.cin);
            $('[name="nom"]').val(data.personne.nom);
            $('[name="prenom"]').val(data.personne.prenom);
            $('[name="email"]').val(data.personne.email);
            $('[name="indicatif"]').val(data.personne.indicatif);
            $('[name="tel"]').val(data.personne.tel);
            $('[name="pays_id"]').val(data.personne.pays_id);
            $('[name="ville_id"]').val(data.personne.ville_id);
            $('[name="adresse"]').val(data.personne.adresse);
            
            $('[name="num_adh"]').val(data.num_adh);
            $('[name="date_adhesion"]').val(data.date_adhesion?data.date_adhesion.substr(0, 10):'');
            dr = data.date_resiliation?data.date_resiliation.substr(0, 10):'';
            $('[name="date_resiliation"]').val(dr);
            $('#adherenttype_id').val(data.adherenttype_id);

            var my_photo = data.personne.photo ? data.personne.photo : "no-photo.png";
            $('#previewing').attr('src', '/uploads/photos/'+my_photo);
            
            $('.supp_photo_icon').show();
            $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#cin').focus();
                    //}); // show bootstrap modal when complete loaded
            $('.modal-title').text("Adhérents: Modifier"); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from application');
        }
    });
}

<?php
    //var_dump($this->session->userdata('popup-id'));
    
    if (is_numeric($this->session->userdata('popup-id'))){
        echo 'edit_entity('.$this->session->userdata('popup-id').');';
        $this->session->unset_userdata(array('popup-id'  => ''));
        $this->session->unset_userdata('popup-id');
    }
?>


jQuery.fn.extend({
	printElem: function() {
		var cloned = this.clone();
    var printSection = $('#printSection');
    if (printSection.length == 0) {
    	printSection = $('<div id="printSection"></div>')
    	$('body').append(printSection);
    }
    printSection.append(cloned);
    var toggleBody = $('body *:visible');
    toggleBody.hide();
    $('#printSection, #printSection *').show();
    window.print();
    printSection.remove();
    toggleBody.show();
	}
});

$(document).ready(function(){
  $(document).on('click', '.print_fiche_btn', function(){
  	$('.printMe').printElem();
  });
});
</script>