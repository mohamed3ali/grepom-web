<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Adhérents: Ajouter</h5>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
              <div class="row">
                <div class="col-xs-3">
                  <div>Photo <span class="pull-right supp_photo_icon"><a onclick="supp_photo()"><i class="material-icons text-danger">delete</i></a></span></div>
                  <div id="image_preview"><img id="previewing" src="/uploads/photos/no-photo.png"  width="180px" height="180px"/></div>
                  <?php //echo form_upload('uploadedphotos[]','','multiple'); ?>
                  <?php echo form_upload('photo','', 'id="photo"'); ?>

                </div>
                <div class="col-xs-9">
                  <div class="row">
                  
                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Pièce d'identité</label>
                        <?php echo form_dropdown('identitetype_id', $comboTypesIdentite, '', 'class="form-control" id="identitetype_id"'); ?>
                      </fieldset>
                    </div>
                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">N° de la pièce d'identité</label>
                            <input type="text" class="form-control" id="cin" name="cin">
                      </fieldset>
                    </div>

                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Nom</label>
                            <input type="text" class="form-control" id="nom" name="nom">
                      </fieldset>
                    </div>
                    
                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Prénom</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="prenom" name="prenom">
                        </div>
                      </fieldset>
                    </div>

                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Email</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="email" name="email">
                        </div>
                      </fieldset>
                    </div>

                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Tél</label>
                        <div class="input-group">
                          <div class="row">
                            <div class="col-xs-3 indicatif" id="">
                              <input type="text" class="form-control" name="indicatif" id="indicatif">
                            </div>
                            <div class="col-xs-9 tel" id="">
                              <input type="text" class="form-control" name="tel" id="tel">
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </div>
              
                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Pays</label>
                        <?php echo form_dropdown('pays_id', $comboPays, '', 'class="form-control" id="pays_id"'); ?>
                      </fieldset>
                    </div>

                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Ville</label>
                        <div id="ville_id_div"></div>
                        <input type="text" class="form-control" id="ville_id" name="ville_id">
                      </fieldset>
                    </div>

                    <div class="col-xs-12">
                      <fieldset class="form-group">
                        <label for="">Adresse</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="adresse" name="adresse">
                        </div>
                      </fieldset>
                    </div>



                  </div>
                </div>

                <div class="col-xs-3">
                  <fieldset class="form-group">
                    <label for="">N° d'adhérent</label>
                        <input type="text" class="form-control" id="num_adh" name="num_adh">
                  </fieldset>
                </div>
                
                <div class="col-xs-3">
                  <fieldset class="form-group">
                    <label for="">Type d'adhérent</label>
                    <?php echo form_dropdown('adherenttype_id', $comboTypesAdherent, '', 'class="form-control" id="adherenttype_id"'); ?>
                  </fieldset>
                </div>
                
                <div class="col-xs-3">
                  <fieldset class="form-group">
                    <label for="">Date d'adhésion</label>
                        <input type="date" class="form-control" id="date_adhesion" name="date_adhesion">
                  </fieldset>
                </div>

                <div class="col-xs-3">
                  <fieldset class="form-group">
                    <label for="">Date de résiliation</label>
                        <input type="date" class="form-control" id="date_resiliation" name="date_resiliation" disabled>
                  </fieldset>
                </div>
                
              </div>

              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->







<!-- Bootstrap modal -->
<div class="modal fade" id="modal_carte" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">État de la carte</h5>
            </div>
            <div class="modal-body form">
              <form id="formcarte" action="#">
                <input type="hidden" name="id" id="carteid" value="">
                <!--div class="row">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6"-->
                    <div class="row">
                      <label class="radio-inline col-xs-4">
                        <input type="radio" name="carte" id="carte0" value="0">
                        En attente
                      </label>
                      <label class="radio-inline col-xs-4">
                        <input type="radio" name="carte" id="carte1" value="1">
                        Délivrée
                      </label>
                      <label class="radio-inline col-xs-4">
                        <input type="radio" name="carte" id="carte2" value="2">
                        Reçue
                      </label>
                    </div> 
                  <!--/div>
                  <div class="col-xs-3"></div>
                </div-->     
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_carte()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->






<!-- Bootstrap modal -->
<div class="modal fade printMe" id="modal_fiche" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Fiche adérent</h5>
            </div-->
            <div class="modal-body">
              <button type="button" class="close_fiche_btn" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="material-icons">close</i></span>
              </button>
              <button type="button" class="print_fiche_btn">
                <span ><i class="material-icons">print</i></span>
              </button>
              <div class="row fiche_head">
                <div class="col-xs-5 fiche_logo">
                  <img class="expanding-hidden" src="/assets/images/logo.png" alt="Logo">
                </div>
                <!--div class="col-xs-3 fiche_annee"></div-->
                <div class="col-xs-7 fiche_head_txt">
                  Adhérent N° <span id="fiche_num">31986</span><br>
                  <span id="fiche_type">Membre</span><br>
                  Depuis: <span id="fiche_periode">13/01/2013</span>
                </div>
              </div>
              <div class="row fiche_content">
                <div class="col-xs-4">
                  <img id="fiche_img" src="/uploads/photos/no-photo.png" class="avatar avatar-lg img-circle" height="140px" alt="">
                </div>
                <div class="col-xs-8 fiche_details">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="fiche_lib">Nom</div>
                      <div id="fiche_nom" class="fiche_txt">Mohamed el Allali</div>
                    </div>
                    <div class="col-xs-12">
                      <div class="fiche_lib">N° <span id="fiche_pi">CIN</span></div>
                      <div class="fiche_txt" id="fiche_ni">ABC12345678</div>
                    </div>
                    <div class="col-xs-6">
                      <div class="fiche_lib">Ville</div>
                      <div class="fiche_txt" id="fiche_ville">Rabat</div>
                    </div>
                    <div class="col-xs-6">
                      <div class="fiche_lib">Pays</div>
                      <div class="fiche_txt" id="fiche_pays">Maroc</div>
                    </div>
                    <div class="col-xs-6">
                      <div class="fiche_lib">Tél</div>
                      <div class="fiche_txt" id="fiche_tel">0663535436</div>
                    </div>
                    <div class="col-xs-6">
                      <div class="fiche_lib">Email</div>
                      <div class="fiche_txt" id="fiche_email">test@mail.com</div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <!--div class="modal-footer">
            </div-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal_fiche0" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Fiche adérent</h5>
            </div-->
            <div class="modal-body form">

                <div class="flex-xs scroll-y">
                  <div class="column-equal">
                    <div class="col" style="width:128px;">
                      <img id="fiche_img" src="/uploads/photos/no-photo.png" class="avatar avatar-lg img-circle" height="140px" alt="">
                    </div>
                    <div class="col v-align-middle p-l-1">
                      <h4 id="fiche_nom">Mohamed El Allali</h4>
                      <h6>Adhérent N° <span id="fiche_num">31986</span><br/><span id="fiche_periode">depuis 13/01/2013</span></h6>
                    </div>
                  </div>
                  <div class="column-equal">
                    <div class="col text-xs-right" style="width:128px;">
                      <span class="text-muted">Type :</span>
                    </div>
                    <div class="col p-l-1">
                      <span id="fiche_type"></span>
                    </div>
                  </div>
                  <div class="column-equal">
                    <div class="col text-xs-right" style="width:128px;">
                      <span class="text-muted" id="fiche_pi"></span><span class="text-muted"> N° :</span>
                    </div>
                    <div class="col p-l-1">
                      <span id="fiche_ni"></span>
                    </div>
                  </div>
                  <div class="column-equal">
                    <div class="col text-xs-right" style="width:128px;">
                      <span class="text-muted">Email :</span>
                    </div>
                    <div class="col p-l-1">
                      <a id="fiche_email" href="">
                        email@example.com
                      </a>
                    </div>
                  </div>
                  <div class="column-equal">
                    <div class="col text-xs-right" style="width:128px;">
                      <span class="text-muted">Profession :</span>
                    </div>
                    <div class="col p-l-1">
                      <span id="fiche_profession"></span>
                    </div>
                  </div>
                  <div class="column-equal">
                    <div class="col text-xs-right" style="width:128px;">
                      <span class="text-muted">
                        Tél :
                      </span>
                    </div>
                    <div class="col p-l-1">
                      <span id="fiche_tel">
                        +1 123 456 789
                      </span>
                    </div>
                  </div>
                  <div class="column-equal">
                    <div class="col text-xs-right" style="width:128px;">
                      <span class="text-muted">
                        Adresse :
                      </span>
                    </div>
                    <div class="col p-l-1" id="fiche_adresse">
                    </div>
                  </div>
                </div>


            </div>
            <!--div class="modal-footer">
            </div-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->