<script type="text/javascript">
var save_method; //for save method string
var agtable;
var cntable;
var entity_id;
 
$(document).ready(function() {
    agtable = $('#agtable').DataTable({
        "language": {
              url: "<?php echo base_url(); ?>assets/vendor/datatables/media/js/french.json"
        },
        dom:'Blfrtip',
        buttons: [
            {
                //text: '<i class="glyphicon glyphicon-plus"></i> Ajouter',
                //className:'btn btn-success dataTables_btn',
                //action: function ( e, dt, node, config ) {
                //    add_entity();
                //}
            }
        ],
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            ['5', '10', '25', '50', 'Tout afficher']
        ],
        'ajax': "<?php echo base_url(uri_string()); ?>/json/1",
      });
    cntable = $('#cntable').DataTable({
        "language": {
              url: "<?php echo base_url(); ?>assets/vendor/datatables/media/js/french.json"
        },
        dom:'Blfrtip',
        buttons: [
            {
                //text: '<i class="glyphicon glyphicon-plus"></i> Ajouter',
                //className:'btn btn-success dataTables_btn',
                //action: function ( e, dt, node, config ) {
                //    add_entity();
                //}
            }
        ],
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            ['5', '10', '25', '50', 'Tout afficher']
        ],
        'ajax': "<?php echo base_url(uri_string()); ?>/json/2",
      });
    
    var next = 1;
    $(".nouvelle-ligne").click(function(e){
        e.preventDefault();
        var addto = ".ligne";
        var addRemove = ".ligne-btn" + (next);
        next = next + 1;
        var newIn = '<tr class="ligne" id="ligne' + next + '">';
        newIn += '     <td colspan="2"><input class="form-control theme-input" name="theme1" type="text"/></td>';
        newIn += '     <td><button id="remove' + next + '" class="btn btn-danger remove-me" >-</button></div><div id="field"></td>';
        newIn += '   </tr>';
        
        var newInput = $(newIn);
        $(addto).last().after(newInput);
        $("#ligne" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
        $("#ligne" + next + " .theme-input").val($("#ligne" + (next-1) + " .theme-input").val());
        $("#ligne" + next + " .sujet-input").focus();
        console.log("#ligne" + (next-1) + " .theme-input");
        $('.remove-me').click(function(e){
            e.preventDefault();
            var fieldNum = this.id.charAt(this.id.length-1);
            var fieldID = "#ligne" + fieldNum;
            $(this).remove();
            $(fieldID).remove();
        });
    });
    var next = 1;
    $(".nv-sujet00").click(function(e){
        e.preventDefault();
        var addto = ".ligne";
        var addRemove = ".ligne-btn" + (next);
        next = next + 1;
        var newIn = '<tr class="ligne" id="ligne' + next + '">';
        newIn += '     <td>L></td>';
        newIn += '     <td><input class="form-control sujet-input" name="sujet' + next + '" type="text"/></td>';
        newIn += '     <td class="ligne-btn"><button id="remove' + next + '" class="btn btn-danger remove-me" >-</button></div><div id="field"></td>';
        newIn += '   </tr>';

        //console.log(addto);
        //console.log(newIn);
        
        var newInput = $(newIn);
        $(addto).last().after(newInput);
        $("#ligne" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
        $("#ligne" + next + " .theme-input").val($("#ligne" + (next-1) + " .theme-input").val());
        $("#ligne" + next + " .sujet-input").focus();
        console.log("#ligne" + (next-1) + " .theme-input");
        $('.remove-me').click(function(e){
            e.preventDefault();
            var fieldNum = this.id.charAt(this.id.length-1);
            var fieldID = "#ligne" + fieldNum;
            $(this).remove();
            $(fieldID).remove();
        });
    });
    var iLine  = 0;
    var iTheme = 0;
    var iSujet = 0;
    var addto = ".ligne";
    function newTheme(l,t){
        var themeLine = '<tr class="ligne" id="ligne'+l+'"><td>';
        themeLine    += '     <div class="input-group"><input class="form-control theme-input" name="titre['+t+'][0]" type="text"/>';
        themeLine    += '     <div class="input-group-addon"><i class="material-icons" id="id="remove'+l+'" aria-hidden="true">delete</i></div></div>';
        themeLine    += '     </td></tr>';
        return themeLine;
    }
    function newLine(l,t,s=0){
        var themeLine = '<tr class="ligne" id="ligne'+l+'"><td><div class="input-group">';
        themeLine    += (s>0)?'<div class="input-group-addon"><i class="material-icons" aria-hidden="true">subdirectory_arrow_right</i></div>':'';
        themeLine    += '     <input class="form-control theme-input" name="titre['+t+']['+s+']" type="text"/>';
        themeLine    += '     <div class="input-group-addon"><i class="material-icons remove-me" id="remove'+l+'" aria-hidden="true">delete</i></div>';
        themeLine    += '     </div></td></tr>';
        return themeLine;
    }

    $(".nv-theme").click(function(e){
        e.preventDefault();
        iLine++;
        iTheme++;
        iSujet = 0;
        $(addto).last().after($(newLine(iLine, iTheme, iSujet)));
        $("#ligne" + iLine).attr('data-source',$(addto).attr('data-source'));
        $("#ligne" + iLine + " input").focus();
        $('.remove-me').click(function(e){
            e.preventDefault();
            var lineNum = this.id.charAt(this.id.length-1);
            var lineID = "#ligne" + lineNum;
            console.log("deleting "+lineID);
            $(this).remove();
            $(lineID).remove();
        });
    });
    $(".nv-sujet").click(function(e){
        e.preventDefault();
        iLine++;
        iSujet++;
        $(addto).last().after($(newLine(iLine, iTheme, iSujet)));
        $("#ligne" + iLine).attr('data-source',$(addto).attr('data-source'));
        $("#ligne" + iLine + " input").focus();
        $('.remove-me').click(function(e){
            e.preventDefault();
            var lineNum = this.id.charAt(this.id.length-1);
            var lineID = "#ligne" + lineNum;
            $(this).remove();
            $(lineID).remove();
        });
    });
    function moveItems(origin, dest) {
        $(origin).find(':selected').appendTo(dest);
    }
     
    function moveAllItems(origin, dest) {
        $(origin).children().appendTo(dest);
    }
     
    $('#left').click(function () {
        moveItems('#sbTwo', '#sbOne');
    });
     
    $('#right').on('click', function () {
        moveItems('#sbOne', '#sbTwo');
    });
     
    $('#leftall').on('click', function () {
        moveAllItems('#sbTwo', '#sbOne');
    });
     
    $('#rightall').on('click', function () {
        moveAllItems('#sbOne', '#sbTwo');
    });
});

$('.drp').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    locale: {
      format: 'DD/MM/YYYY H:mm'
    }
});


$('#listmembres').multiSelect();


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function add_entity()
{
    save_method = 'add';
    //$('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('.demission-group').hide(); // clear error string
    $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#numrecu').focus();
                    //});  
    $('.modal-title').text('Réunions: Ajouter'); // Set Title to Bootstrap modal title
}

function save()
{
    $('#btnSave').text('Enregistrement en cours ...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo base_url(uri_string()); ?>/ajax_add";
    } else {
        url = "<?php echo base_url(uri_string()); ?>/ajax_update/"+entity_id;
    }
 
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
 
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Erreur d\'enregistrement des données');
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}

function delete_entity(id)
{
    if(confirm('Voulez vous vraiment supprimer ?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo base_url(uri_string()); ?>/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression!');
            }
        });
 
    }
} 

function edit_entity(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.demission-group').show(); // clear error string
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url(uri_string()); ?>/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            //console.log(data);
            entity_id = data.id;
            $('[name="cin"]').val(data.personne.cin);
            $('[name="nom"]').val(data.personne.nom);
            $('[name="prenom"]').val(data.personne.prenom);
            $('[name="email"]').val(data.personne.email);
            $('[name="tel"]').val(data.personne.tel);
            $('[name="adresse"]').val(data.personne.adresse);
            
            $('[name="num_adh"]').val(data.num_adh);
            $('[name="date_adhesion"]').val(data.date_adhesion);
            $('#adherenttype_id').val(data.adherenttype_id);

            $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#cin').focus();
                    //}); // show bootstrap modal when complete loaded
            $('.modal-title').text("Réunions: Mise à jour"); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from application');
        }
    });
}

/*
    Global
*/
function add_reunion(typer)
{
    alert(typer);
    save_method = 'add';
    //$('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('.demission-group').hide(); // clear error string
    $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#numrecu').focus();
                    //});  
    $('.modal-title').text('Réunions: Ajouter'); // Set Title to Bootstrap modal title
}
/*
    CN
*/
function add_cn(typer)
{
    add_reunion(typer);
   
}

/*
    BA
*/
function add_ba(typer)
{
    add_reunion(typer);
}

/*
    BE
*/
function add_be(typer)
{
    add_reunion(typer);
}

/*
    CN + BE
*/
function add_cnbe(typer)
{
    add_reunion(typer);
}

/*
    CN + BE
*/
function add_cnbe(typer)
{
    add_reunion(typer);
}

/*
    BA + BE
*/
function add_babe(typer)
{
    add_reunion(typer);
}
 
</script>