
            <div class="card">
              <div class="card-header no-bg b-a-0">
                <h4 class="pull-left"><?php if (isset($titre)) {echo $titre;} ?>:</h4>

                <div class="dropdown btn-group pull-right m-r-xs m-b-xs">
                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    Nouvelle réunion
                    <span class="caret">
                    </span>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                  <?php
                    foreach ($comboTypesReunion as $typeReunion) {
                      //var_dump($typeReunion);die();
                      echo '<a class="dropdown-item" href="javascript:void(0)" onclick="add_'.$typeReunion["code"].'('.$typeReunion["id"].')">'.$typeReunion["type_reunion"].'</a>';
                    }
                  ?>
                  </div>
                </div>
                
              </div>
              <div class="card-block"> 
                
                <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#one" role="tab">Conseils nationaux</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#two" role="tab">Assemblées générales</a>
                  </li>
                </ul>

                <div class="tab-content">
                  <div class="tab-pane active" id="one" role="tabpanel">
                    <table id="cntable" class="dt table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>Date</th>
                              <th>Lieu</th>
                              <th>Objet</th>
                              <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                  </div>
                  <div class="tab-pane" id="two" role="tabpanel">
                    <table id="agtable" class="dt table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Lieu</th>
                                <th>Objet</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                  </div>
                </div>
                
              </div>
            </div>
        
        
<?php if (isset($zonemodals)) {$this->load->view($zonemodals);} ?>
