<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Réunions : Ajouter</h4>
            </div>
            <div class="modal-body form">
              <form id="form" class="step-form" action="#">

                <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#step1" role="tab">Détails de la réunion</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#step2" role="tab">Ordre du jour</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#step3" role="tab">Les participants</a>
                  </li>
                </ul>

                <div class="tab-content">
                  <div class="tab-pane active" id="step1" role="tabpanel">
                    
                    <fieldset class="form-group col-xs-12">
                      <label for="exampleTextarea">Objet</label>
                      <input type="text" class="form-control" id="objet" name="objet">
                    </fieldset>

                    <fieldset class="form-group col-xs-5">
                      <label for="">Date</label><div class="input-prepend input-group m-b-1">
                      <span class="add-on input-group-addon">
                        <i class="material-icons">
                          date_range
                        </i>
                      </span>
                      <input type="text" name="date" class="form-control drp" value=""/>
                    </div>
                    </fieldset>

                    <!--fieldset class="form-group col-xs-4">
                      <label for="">Début</label>
                      <div class="row">
                        <div class=" col-xs-8"><input type="date" class="form-control datepicker" id="ddebut" name="ddebut"></div>
                        <div class=" col-xs-4"><input type="time" class="form-control datepicker" id="hdebut" name="hdebut"></div>
                      </div>
                    </fieldset>
  
                    <fieldset class="form-group col-xs-4">
                      <label for="">Fin</label>
                      <div class="row">
                        <div class=" col-xs-8"><input type="date" class="form-control datepicker" id="ddebut" name="ddebut"></div>
                        <div class=" col-xs-4"><input type="time" class="form-control datepicker" id="hdebut" name="hdebut"></div>
                      </div>
                    </fieldset-->
                
                    <fieldset class="form-group col-xs-5">
                      <label for="exampleTextarea">Lieu</label>
                      <input type="text" class="form-control" id="lieu" name="lieu">
                    </fieldset>
                
                    <fieldset class="form-group col-xs-2">
                      <label for="exampleTextarea">Chorum</label>
                      <input type="text" class="form-control" id="chorum" name="chorum">
                    </fieldset>




                  </div>
                  <div class="tab-pane" id="step2" role="tabpanel">
                    
                    <input type="hidden" name="count" value="1" />
                    <table class="table m-b-0">
                      <thead>
                        <tr>
                          <th style="text-align: right;">
                            <button id="T1" class="btn nv-theme" type="button">Nouveau thème</button> 
                            <button id="S1" class="btn nv-sujet" type="button">Nouveau sujet</button>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="ligne"></tr>
                        <!--tr class="ligne" id="ligne1"><td>
                          <div class="input-group"><input class="form-control theme-input" name="titre[1][0]" type="text"/>
                          <div class="input-group-addon"><i class="material-icons" id="id="remove1" aria-hidden="true">delete</i></div></div>
                        </td></tr>

                        <tr class="ligne" id="ligne2">
                          <td>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="material-icons" aria-hidden="true">subdirectory_arrow_right</i></div>
                              <input class="form-control theme-input" name="titre[1][1]" type="text"/>
                              <div class="input-group-addon"><i class="material-icons" id="id="remove2" aria-hidden="true">delete</i></div>
                            </div>
                          </td>
                        </tr-->
                      </tbody>
                    </table>

                  </div>
                  <div class="tab-pane" id="step3" role="tabpanel">
                    <div class="col-xs-6">Tous les membres:</div><div class="col-xs-6 p-l-2">les participants:</div>
                    <div class="col-xs-12">
                      <?php echo form_dropdown('membresCN[]', $comboMembresCN, '', 'multiple id="listmembres" class="multiselect" name="listmembres[]"'); ?>
                    </div>
                    <!--select multiple id="listmembres" class="multiselect" name="listmembres[]">
                      <option value="elem_1" selected>
                        elem 1
                      </option>
                      <option value="elem_2">
                        elem 2
                      </option>
                      <option value="elem_3">
                        elem 3
                      </option>
                      <option value="elem_4" selected>
                        elem 4
                      </option>
                      <option value="elem_5">
                        elem 5
                      </option>
                      <option value="elem_6">
                        elem 6
                      </option>
                      <option value="elem_7">
                        elem 7
                      </option>
                      <option value="elem_8">
                        elem 8
                      </option>
                    </select>

                      <!--fieldset class="form-group col-xs-5">
                        <label for="">Tous les membres</label>
                        <?php echo form_dropdown('membresCN', $comboMembresCN, '', 'id="sbOne" class="form-control select-members-6" multiple="multiple"'); ?>
                      </fieldset>
                      
                      <fieldset class="form-group col-xs-1 move-member-btns">
                        <input type="button" id="right" value=">" />
                        <input type="button" id="left" value="<" />
                        <input type="button" id="leftall" value="<<" />
                        <input type="button" id="rightall" value=">>" />
                      </fieldset>

                      <fieldset class="form-group col-xs-5">
                        <label for="">Les participants</label>
                        <select id="sbTwo" class="form-control select-members-6" multiple="multiple">
                        </select>
                      </fieldset-->
                   
                  </div>
                </div>

              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->