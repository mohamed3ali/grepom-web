<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nouvelle réunion</h4>
            </div>
            <div class="modal-body form">
              <form id="reunion-form" class="step-form" action="#">
              </form>
              
                  <!-- BEGIN STEP FORM WIZARD -->
                  <div class="tsf-wizard tsf-wizard-1">
                    <!-- BEGIN NAV STEP-->
                    <div class="tsf-nav-step">
                      <!-- BEGIN STEP INDICATOR-->
                      <ul class="gsi-step-indicator triangle gsi-style-1 gsi-transition ">
                        <li class="current" data-target="step-1">
                          <a href="javascript:;">
                            <div class="number">
                              1
                            </div>
                            <div class="desc">
                              <label>
                                Détails de la réunion
                              </label>
                            </div>
                          </a>
                        </li>
                        <li data-target="step-2">
                          <a href="javascript:;">
                            <div class="number">2</div>
                            <div class="desc">
                              <label>
                                Ordre du jour
                              </label>
                            </div>
                          </a>
                        </li>
                        <li data-target="step-3">
                          <a href="javascript:;">
                            <div class="number">3</div>
                            <div class="desc">
                              <label>
                                les participants
                              </label>
                            </div>
                          </a>
                        </li>
                      </ul>
                      <!-- END STEP INDICATOR -->
                    </div>
                    <!-- END NAV STEP-->
                    <!-- BEGIN STEP CONTAINER -->
                    <div class="tsf-container">
                      <!-- BEGIN CONTENT-->
                      <form id="form" class="tsf-content">
                        <!-- BEGIN STEP 1-->
                        <div class="tsf-step step-1  ">
                          <fieldset>
                            <div class="row">
                              <!-- BEGIN STEP CONTENT-->
                              <div class="tsf-step-content">
                                
                                
                                                
                                <fieldset class="form-group col-xs-12">
                                  <label for="">Type</label>
                                  <?php echo form_dropdown('reuniontype_id', $comboTypesReunion, '', 'class="form-control" id="reuniontype_id"'); ?>
                                </fieldset>
                                
                                <fieldset class="form-group col-xs-6">
                                  <label for="">Début</label>
                                  <div class="row">
                                    <div class=" col-xs-8"><input type="date" class="form-control datepicker" id="ddebut" name="ddebut"></div>
                                    <div class=" col-xs-4"><input type="time" class="form-control datepicker" id="hdebut" name="hdebut"></div>
                                  </div>
                                </fieldset>
              
                                <fieldset class="form-group col-xs-6">
                                  <label for="">Fin</label>
                                  <div class="row">
                                    <div class=" col-xs-8"><input type="date" class="form-control datepicker" id="ddebut" name="ddebut"></div>
                                    <div class=" col-xs-4"><input type="time" class="form-control datepicker" id="hdebut" name="hdebut"></div>
                                  </div>
                                </fieldset>
                                
                                <fieldset class="form-group col-xs-12">
                                  <label for="exampleTextarea">Objet</label>
                                  <input type="text" class="form-control" id="tel" name="tel">
                                </fieldset>
                            
                                <fieldset class="form-group col-xs-6">
                                  <label for="exampleTextarea">Lieu</label>
                                  <input type="text" class="form-control" id="tel" name="tel">
                                </fieldset>
                            
                                <fieldset class="form-group col-xs-6">
                                  <label for="exampleTextarea">Chorum</label>
                                  <input type="text" class="form-control" id="tel" name="tel">
                                </fieldset>
                                
                                
                                
                                
                              </div>
                              <!-- END STEP CONTENT-->
                            </div>
                          </fieldset>
                        </div>
                        <!-- END STEP 1-->
                        <!-- BEGIN STEP 2-->
                        <div class=" tsf-step step-2 ">
                          <fieldset>
                            
                            <!-- BEGIN STEP CONTENT-->
                            <div class="tsf-step-content">
                              <input type="hidden" name="count" value="1" />
                              <table class="table m-b-0">
                                <thead>
                                  <tr>
                                    <th>Thème</th>
                                    <th>Sujet</th>
                                    <th><button id="b1" class="btn nouvelle-ligne" type="button">+</button></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr class="ligne" id="ligne1">
                                    <td>
                                      <input class="form-control" name="theme1" type="text"/>
                                    </td>
                                    <td>
                                      <input class="form-control" name="sujet1" type="text"/>
                                    </td>
                                    <td>
                                      <!--button id="remove1" class="btn btn-danger remove-me" >-</button-->
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                            <!-- END STEP CONTENT-->
                          </fieldset>
                        </div>
                        <!-- END STEP 2-->
                        <!-- BEGIN STEP 3-->
                        <div class=" tsf-step step-3 active">
                          <fieldset>
                            <!-- BEGIN STEP CONTENT-->
                            <div class="tsf-step-content">
                              Liste des participants :
                              <div class="row">
                                            
                                <fieldset class="form-group col-xs-6">
                                  <label for="">Tous les membres</label>
                                  <select id="sbOne" class="form-control" multiple="multiple">
                                    <option value="1">Ahmed Sobhi</option>
                                    <option value="2">Fatima Alaam</option>
                                    <option value="3">Hamid Benmoussa</option>
                                    <option value="4">Reda Mrabet</option>
                                    <option value="5">Khaled Hilali</option>
                                  </select>
                                </fieldset>
                                
                                <fieldset class="form-group col-xs-6">
                                  <label for="">Les participants</label>
                                  <select id="sbTwo" class="form-control" multiple="multiple">
                                  </select>
                                </fieldset>
                             
                                <fieldset class="form-group col-xs-12">
                                  <input type="button" id="left" value="<" />
                                  <input type="button" id="right" value=">" />
                                  <input type="button" id="leftall" value="<<" />
                                  <input type="button" id="rightall" value=">>" />
                                </fieldset>
                              </div>
                            </div>
                            <!-- END STEP CONTENT-->
                          </fieldset>
                        </div>
                        <!-- END STEP 3-->
                      </form>
                      <!-- END CONTENT-->
                      <!-- BEGIN CONTROLS-->
                      <div class="tsf-controls ">
                        <!-- BEGIN PREV BUTTTON-->
                        <button class="btn btn-primary btn-icon btn-left tsf-wizard-btn" data-type="prev" type="button">
                          <i class="material-icons">arrow_back</i>
                          <span>Précédent</span>
                        </button>
                        <!-- END PREV BUTTTON-->
                        <!-- BEGIN NEXT BUTTTON-->
                        <button class="btn btn-primary btn-icon btn-right tsf-wizard-btn" data-type="next" type="button">
                          <i class="material-icons">arrow_forward</i>
                          <span>Suivant</span>
                        </button>
                        <!-- END NEXT BUTTTON-->
                        <!-- BEGIN FINISH BUTTTON-->
                        <button class="btn btn-right tsf-wizard-btn" data-type="finish" type="button">
                          Terminer
                        </button>
                        <!-- END FINISH BUTTTON-->
                      </div>
                      <!-- END CONTROLS-->
                    </div>
                    <!-- END STEP CONTAINER -->
                  </div>
                  <!-- END STEP FORM WIZARD -->
                  
                  
                  
                  
              
              
              
            </div>
            <!--div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->