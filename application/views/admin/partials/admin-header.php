<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1"/>
    <meta name="msapplication-tap-highlight" content="no">
    
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="GREPOM">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="GREPOM">

    <meta name="theme-color" content="#4C7FF0">
    
    <title>GREPOM<?php if (isset($titre) && $titre) echo " - ".$titre; ?></title>

    <!-- page stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/datatables/media/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/summernote/dist/summernote.css"/>
    
    <?php if (isset($css)) {echo $css;} ?>
    <!-- end page stylesheets -->

    <!-- build:css({.tmp,app}) styles/app.min.css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/css/bootstrap.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/pace/themes/blue/pace-theme-minimal.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/animate.css/animate.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/select2/select2.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/multiselect/css/multi-select.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/croppie.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/app.css" id="load_styles_before"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/app.skins.css"/>
    <!-- endbuild -->
    
    <?php 
    if(isset($css_files)) {
      foreach($css_files as $file): ?>
    	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; }?>
    
  </head>
  <body>

    <div class="app">

      <?php require_once("sidebar.php"); ?>
      
      <!-- content panel -->
      <div class="main-panel">

        <?php require_once("header.php"); ?>
        
        <!-- main area -->
        <div class="main-content">
          <div class="content-view">
<!--    START CONTENT     -->