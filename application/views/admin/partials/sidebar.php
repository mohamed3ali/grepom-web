<!--sidebar panel-->
      <div class="off-canvas-overlay" data-toggle="sidebar"></div>
      <div class="sidebar-panel">
        <div class="brand">
          <!-- toggle offscreen menu -->
          <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen hidden-lg-up">
            <i class="material-icons">menu</i>
          </a>
          <!-- /toggle offscreen menu -->
          <!-- logo -->
          <a class="brand-logo">
            <img class="expanding-hidden" src="<?php echo base_url(); ?>assets/images/logo.png" alt=""/>
          </a>
          <!-- /logo -->
        </div>
        <!--div class="nav-profile dropdown">
          <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
            <div class="user-image">
              <img src="<?php echo base_url(); ?>assets/images/avatar.png" class="avatar img-circle" alt="user" title="user"/>
            </div>
            <div class="user-info expanding-hidden">
              <small class="bold"><?php echo "Admin"; //ucwords($mod['data']->login); ?></small>
            </div>
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="javascript:;">Profile</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/auth/logout">Se déconnecter</a>
          </div>
        </div-->
        <!-- main navigation -->
        <nav>
          <p class="nav-title"></p>
          <ul class="nav">
            <!-- dashboard -->
            <li>
              <a href="/">
                <i class="material-icons">home</i>
                <span>Accueil</span>
              </a>
            </li>
            <!-- /dashboard -->
            <!-- apps -->
            <li class="<?php echo ($this->uri->segment(1)=='rh')?'open': '';?>">
              <a href="javascript:;">
                <span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span>
                <i class="material-icons">assignment_ind</i>
                <span>Ressources humaines</span>
              </a>
              <ul class="sub-menu">
                <li>
                  <a href="/rh/adherents"><span>Adhérents</span></a>
                </li>
                <li>
                  <a href="/rh/salaries"><span>Bureau Exécutif</span></a>
                </li>
                <li>
                  <a href="/rh/cnmembres"><span>Conseil National</span></a>
                </li>
              </ul>
            </li>
            <!--li>
              <a href="/cotisations">
                <i class="material-icons">attach_money</i>
                <span>Cotisations</span>
              </a>
            </li>
            <!--// Compta -->
            <li class="<?php echo ($this->uri->segment(1)=='compta')?'open': '';?>">
              <a href="javascript:;">
                <span class="menu-caret">
                  <i class="material-icons">arrow_drop_down</i>
                </span>
                <i class="material-icons">attach_money</i>
                <span>Comptabilité</span>
              </a>
              <ul class="sub-menu">
                <li>
                  <a href="javascript:;">
                    <span class="menu-caret">
                      <i class="material-icons">arrow_drop_down</i>
                    </span>
                    <i class="material-icons">chevron_right</i>
                    <span>Recettes</span>
                  </a>
                  <ul class="sub-menu">
                    <li>
                      <a href="/compta/projrecettes">
                        <span>Projets</span>
                      </a>
                    </li>
                    <li>
                      <a href="/compta/donrecettes">
                        <span>Dons</span>
                      </a>
                    </li>
                    <li>
                      <a href="/compta/cotisations">
                        <span>Cotisations</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="javascript:;">
                    <span class="menu-caret">
                      <i class="material-icons">arrow_drop_down</i>
                    </span>
                    <i class="material-icons">chevron_right</i>
                    <span>Dépenses</span>
                  </a>
                  <ul class="sub-menu">
                    <li>
                      <a href="/compta/depenses">
                        <span>Liste des dépenses</span>
                      </a>
                    </li>
                    <li>
                      <a href="/compta/depensesnomenclatures">
                        <span>Nomenclatures</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="/compta/projets">
                    <i class="material-icons">chevron_right</i>
                    <span>Projets</span>
                  </a>
                </li>
                <li>
                  <a href="/compta/donateurs">
                    <i class="material-icons">chevron_right</i>
                    <span>Donateurs</span>
                  </a>
                </li>
                <li>
                  <a href="javascript:;">
                    <span class="menu-caret">
                      <i class="material-icons">arrow_drop_down</i>
                    </span>
                    <i class="material-icons">chevron_right</i>
                    <span>Commandes</span>
                  </a>
                  <ul class="sub-menu">
                    <li>
                      <a href="/compta/commandes">
                        <span>Liste des commandes</span>
                      </a>
                    </li>
                    <li>
                      <a href="/compta/commandebons">
                        <span>Bons de commandes</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <!-- /Compta -->

            <li>
              <a href="/inventaire">
                <i class="material-icons">assignment</i>
                <span>Inventaire</span>
              </a>
            </li>
            <li>
              <a href="/compta/fondsbailleurs">
                <i class="material-icons">business_center</i>
                <span>Bailleurs de fonds</span>
              </a>
            </li>
            <li>
              <a href="/evenements/reunions">
                <span class="menu-caret"></span>
                <i class="material-icons">question_answer</i>
                <span>Réunions et évènements</span>
              </a>
            </li>
            <!--li>
              <a href="javascript:;">
                <span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span>
                <i class="material-icons">question_answer</i>
                <span>Réunions et évènements</span>
              </a>
              <ul class="sub-menu">
                <li>
                  <a href="/evenements/reunions"><span>Réunions</span></a>
                </li>
                <li>
                  <a href="#"><span>Sujets</span></a>
                </li>
                <li>
                  <a href="#"><span>Thèmes</span></a>
                </li>
                <li>
                  <a href="#"><span>Présence</span></a>
                </li>
              </ul>
            </li-->
          </ul>
          <ul class="nav">
            <li>
              <a href="javascript:;">
                <span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span>
                <i class="material-icons">library_books</i><span>Procédures</span>
              </a>
              <ul class="sub-menu">
                <?php
                if(isset($menu_procedures) && $menu_procedures){
                  foreach ($menu_procedures as $value) {
                    echo '<li><a href="/procedures/details/'.$value["id"].'"><span></span>'.$value["nom_procedure"].'</a></li>';
                  }
                }
                ?>
              </ul>
            </li>
            <!-- /static -->
            <li><hr/></li>
            <!-- documentation -->
            <li>
              <a href="javascript:;" target="_blank">
                <span class="menu-caret"><i class="material-icons">arrow_drop_down</i></span>
                <i class="material-icons">settings</i><span>Paramétrage</span>
              </a>
              <ul class="sub-menu">
                <li>
                  <a href="/parametrage/personnes"><span>Personnes</span></a>
                </li>
                <li>
                  <a href="/parametrage/types_personne"><span>Types personnes</span></a>
                </li>
                <li>
                  <a href="/parametrage/types_adherent"><span>Types adhérents</span></a>
                </li>
                <li>
                  <a href="/parametrage/fonctions_salarie"><span>Fonctions des salariés</span></a>
                </li>
                <li>
                  <a href="/parametrage/fonctions_cn"><span>Fonctions CN</span></a>
                </li>
                <li>
                  <a href="/parametrage/modes_cotisation"><span>Modes de paiement</span></a>
                </li>
                <li>
                  <a href="/parametrage/types_cotisation"><span>Types de cotisations</span></a>
                </li>
                <li>
                  <a href="/parametrage/types_reunion"><span>Types de réunions</span></a>
                </li>
                <li>
                  <a href="/parametrage/procedures"><span>Procédures</span></a>
                </li>
              </ul>
            </li>
            <!-- /documentation -->
          </ul>
        </nav>
        <!-- /main navigation -->
      </div>
      <!-- /sidebar panel -->
      