<!--    END OF CONTENT     -->
          </div>
          
          <?php require_once("footer.php"); ?>
          
        </div>
        <!-- /main area -->
      </div>
      <!-- /content panel -->

      

    </div>
    <!-- build:js({.tmp,app}) scripts/app.min.js -->
    <script src="<?php echo base_url(); ?>assets/vendor/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/pace/pace.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/tether/dist/js/tether.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/fastclick/lib/fastclick.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/multiselect/js/jquery.multi-select.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/select2/select2.js"></script>
    <script src="<?php echo base_url(); ?>assets/scripts/constants.js"></script>
    <!--script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js"></script-->
    <script src="<?php echo base_url(); ?>assets/scripts/main.js"></script>
    <!-- endbuild -->

    <!-- page scripts -->
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/media/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/media/js/dataTables.bootstrap4.js"></script>
    <!--script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script-->
    <script src="<?php echo base_url(); ?>assets/scripts/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/summernote/dist/summernote.js"></script>
    <!-- end page scripts -->

    <?php if(isset($js_files)) {
      foreach($js_files as $file): ?>
    	<script src="<?php echo $file; ?>"></script>
    <?php endforeach; }?>

    <!-- initialize page scripts -->
    <script type="text/javascript">
      //$('#cin').mask('aa999999');
      $('.summernote').summernote({
        airMode: false
      });
    </script>
    
    <?php if (isset($scripts)) {echo $scripts;} ?>
    
    <?php if (isset($zonescripts)) {$this->load->view($zonescripts);} ?>
    <!-- end initialize page scripts -->       

  </body>
</html>
