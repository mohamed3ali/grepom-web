<script type="text/javascript">
var save_method; //for save method string
var batable;
var ctable;
var entity_id;
 
$(document).ready(function() {
    batable = $('#batable').DataTable({
        "language": {
              url: "<?php echo base_url(); ?>assets/vendor/datatables/media/js/french.json"
        },
        dom:'Blfrtip',
        buttons: [
            {
                text: 'Mandat : <?php echo str_replace("\r", "", str_replace("\n", "", form_dropdown('mandat_filtre', $comboMandats, '', ' onChange="filterByMandat(0, this.value);" class="form-control" id="mandat_filtre"'))); ?>',
                className:'mandat-tag'
            },
            {
                text: '<i class="glyphicon glyphicon-plus"></i> Ajouter',
                className:'btn btn-success dataTables_btn',
                action: function ( e, dt, node, config ) {
                    add_entity();
                }
            }
        ],
        "lengthMenu": [
            [11, 25, 50, -1],
            ['11', '25', '50', 'Tout']
        ],
        'ajax': "<?php echo base_url(uri_string()); ?>/jsonba",
      });

    ctable = $('#ctable').DataTable({
        "language": {
              url: "<?php echo base_url(); ?>assets/vendor/datatables/media/js/french.json"
        },
        dom:'Blfrtip',
        buttons: [
            {
                text: 'Mandat : <?php echo str_replace("\r", "", str_replace("\n", "", form_dropdown('mandat_filtre', $comboMandats, '', ' onChange="filterByMandat(0, this.value);" class="form-control" id="mandat_filtre"'))); ?>',
                className:'mandat-tag'
            },
            {
                text: '<i class="glyphicon glyphicon-plus"></i> Ajouter',
                className:'btn btn-success dataTables_btn',
                action: function ( e, dt, node, config ) {
                    add_entity();
                }
            }
        ],
        "lengthMenu": [
            [11, 25, 50, -1],
            ['11', '25', '50', 'Tout']
        ],
        'ajax': "<?php echo base_url(uri_string()); ?>/jsonc",
      });

});

var filterByMandat = function(column, dateMandat) {

    $.fn.dataTableExt.afnFiltering.length = 0;
    batable.ajax.reload(null,false);
    ctable.ajax.reload(null,false);

// Custom filter syntax requires pushing the new filter to the global filter array
    $.fn.dataTableExt.afnFiltering.push(
        function( oSettings, aData, iDataIndex ) {
          var rowDate = aData[column];
          
          // If our date from the row is between the start and end
          if (dateMandat == "") {
            return true;
          } else {
            if (dateMandat == rowDate) {
                return true;
            } else {
                return false;
            }
          }
        }
    );
    batable.ajax.reload(null,false);
    ctable.ajax.reload(null,false);
};

function add_entity()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('.demission-group').hide(); // clear error string
    $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#numrecu').focus();
                    //});  
    $('.modal-title').text('Nouveau membre'); // Set Title to Bootstrap modal title
}

function save()
{
    $('#btnSave').text('Enregistrement en cours ...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo base_url(uri_string()); ?>/ajax_add";
    } else {
        url = "<?php echo base_url(uri_string()); ?>/ajax_update/"+entity_id;
    }
 
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
 
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Erreur d\'enregistrement des données');
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}

function delete_entity(id)
{
    if(confirm('Voulez vous vraiment supprimer ?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo base_url(uri_string()); ?>/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression!');
            }
        });
 
    }
} 

function edit_entity(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.demission-group').show(); // clear error string
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url(uri_string()); ?>/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            entity_id = data.id;
            $('#mandat').val(data.mandat);
            $('#adherent_id').val(data.adherent_id);
            $('#cnfonction_id').val(data.cnfonction_id);
            $('#reunion_id').val(data.reunion_id);

            $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#cin').focus();
                    //}); // show bootstrap modal when complete loaded
            $('.modal-title').text("Mise à jour du membre"); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from application');
        }
    });
}
 
</script>