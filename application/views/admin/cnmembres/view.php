
            <div class="card">
              <div class="card-block"> 

                <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#one" role="tab">Bureau Administratif</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#two" role="tab">Conseil National</a>
                  </li>
                </ul>

                <div class="tab-content">
                  <div class="tab-pane active" id="one" role="tabpanel">
                    <table id="batable" class="dt table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Mandat</th>
                                <!--th>N° adhérent</th-->
                                <th>N° d'identité</th>
                                <th>Nom complet</th>
                                <th>Fonction</th>
                                <!--th>Réunion de nomination</th-->
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
             
                        <!--tfoot>
                        </tfoot-->
                    </table>
                  </div>
                  <div class="tab-pane" id="two" role="tabpanel">
                    <table id="ctable" class="dt table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Mandat</th>
                                <!--th>N° adhérent</th-->
                                <th>N° d'identité</th>
                                <th>Nom complet</th>
                                <th>Fonction</th>
                                <!--th>Réunion de nomination</th-->
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
             
                        <!--tfoot>
                        </tfoot-->
                    </table>
                  </div>
                </div>
                
              </div>
            </div>
        
        
<?php if (isset($zonemodals)) {$this->load->view($zonemodals);} ?>
