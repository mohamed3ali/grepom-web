<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nouveau membre</h4>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                <div class="row">
                  <div class="col-xs-2">
                    <fieldset class="form-group">
                      <label for="">Mandat</label>
                      <input type="text" class="form-control" id="mandat" name="mandat">
                    </fieldset>
                  </div>

                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">N° adhérent</label>
                      <?php echo form_dropdown('adherent_id', $comboAdherents, '', 'class="form-control" id="adherent_id"'); ?>
                    </fieldset>
                  </div>

                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Fonction</label>
                      <?php echo form_dropdown('cnfonction_id', $comboCnFonctions, '', 'class="form-control" id="cnfonction_id"'); ?>
                    </fieldset>
                  </div>

                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">Réunion de nomination</label>
                      <?php echo form_dropdown('reunionion_id', $comboReunions, '', 'class="form-control" id="reunionion_id"'); ?>
                    </fieldset>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->