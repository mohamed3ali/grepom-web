<script type="text/javascript">
(function($) {
  'use strict';

  //Global defaults
  Chart.defaults.global.responsive = true;
  Chart.defaults.global.defaultFontFamily = $.constants.font;
  Chart.defaults.global.defaultFontSize = 12;
  //Title
  Chart.defaults.global.title.fontFamily = $.constants.font;
  Chart.defaults.global.title.fontStyle = 'normal';
  //Tooltip
  Chart.defaults.global.tooltips.FontFamily = $.constants.font;
  Chart.defaults.global.tooltips.FontSize = 12;
  Chart.defaults.global.elements.arc.borderWidth = 1;
  Chart.defaults.global.elements.line.borderWidth = 1;
 
  Chart.defaults.bar.scales.xAxes[0].categorySpacing = 0

  var cotisationsChartData = {
    labels: [<?php foreach ($cotisationStats as $key => $value) { echo "'$value->annee_id',"; } ?>],
    datasets: [{
      label: 'Cotisations',
      backgroundColor: $.constants.primary,
      data: [<?php foreach ($cotisationStats as $key => $value) { echo "'$value->montant',"; } ?>]
    }]
  };

  var cotisationsBarElement = document.getElementById('cotisations-bar').getContext('2d');
  window.myBar = new Chart(cotisationsBarElement, {
    type: 'bar',
    data: cotisationsChartData,
    options: {
      title: {
        display: true,
        text: 'Totaux des cotisations en Dh par année'
      },
      tooltips: {
        mode: 'label'
      },
      scales: {
        xAxes: [{
          //stacked: true,
          barPercentage: 0.5
        }],
        yAxes: [{
          stacked: true
        }]
      }
    }
  });



  var adhChartData = {
    labels: [<?php foreach ($adherantCountStats as $key => $value) { echo "'$value->annee_id',"; } ?>],
    datasets: [{
      label: 'Adhérents actifs',
      backgroundColor: $.constants.primary,
      data: [<?php foreach ($adherantCountStats as $key => $value) { echo "$value->total,"; } ?>]
    }]
  };

  var cotisationsBarElement = document.getElementById('adh-count-bar').getContext('2d');
  window.myBar = new Chart(cotisationsBarElement, {
    type: 'bar',
    data: adhChartData,
    options: {
      title: {
        display: true,
        text: "Nombre d'adhérents actifs par année"
      },
      tooltips: {
        mode: 'label'
      },
      scales: {
        xAxes: [{
          //stacked: true,
          barPercentage: 0.5
        }],
        yAxes: [{
          stacked: true
        }]
      }
    }
  });



function getRandomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

function getRandomColorEachPro(count) {
        var data =[];
        for (var i = 0; i < count; i++) {
            data.push(getRandomColor());
        }
        return data;
    }

  var adhProData = {
    type: 'doughnut',
    data: {
      datasets: [{
        data: [
          <?php foreach ($adherantsProStats as $key => $value) { echo "$value->count,"; } ?>
        ],
        backgroundColor: getRandomColorEachPro(<?php echo count($adherantsProStats); ?>),
        label: 'Dataset 1'
      }],
      labels: [
        <?php foreach ($adherantsProStats as $key => $value) { if ($value->profession) echo '"'.$value->profession.'",'; else echo '"Autre",'; } ?>
      ]
    },
    options: {
      legend: {
        display: false,
        position: 'top'
      },
      title: {
        display: true,
        text: 'Répartition des membres par profession'
      },
      animation: {
        animateScale: true,
        animateRotate: true
      }
    }
  };

  var adhProChart = document.getElementById('adh-pro-chart').getContext('2d');
  window.myDoughnut = new Chart(adhProChart, adhProData);
})(jQuery);
</script>