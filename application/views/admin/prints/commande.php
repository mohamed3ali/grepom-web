<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Commande N° <?php echo $commande->num_commande;?></title>
        <style>
        .clearfix:after {
          content: "";
          display: table;
          clear: both;
        }

        a {
          color: #0087C3;
          text-decoration: none;
        }

        body {
          position: relative;
          width: 21cm;  
          height: 29.7cm; 
          margin: 0 auto; 
          color: #555555;
          background: #FFFFFF; 
          font-family: Arial, sans-serif; 
          font-size: 14px; 
        }

        header {
          padding: 10px 0;
          margin-bottom: 20px;
          /*border-bottom: 1px solid #AAAAAA;*/
        }

        #logo {
          float: left;
          margin-top: 8px;
        }

        #logo .img {
          background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABaCAYAAACR8EvTAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAABBdEVYdENvbW1lbnQAQ1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2NjIpLCBxdWFsaXR5ID0gOTAKsEVYkwAAQ6ZJREFUeF7tnQfcXVWV9ncapNBSCIEAAcQEBOmiCDJ0gYAIzCBFVBBHVAaRj4CDoI6OZX6DgqgQunR0pEqRUMUCGAkQQAgkJISQDiQhvd1v/9e5z73r7pxz7n3fvCnM+CT73W3ttdZeZ7ezzz7ndlq2bFmlU6dOoVKpBO/jhDSvI7F8+fLQuXPnaqw1UEb6tLVse+vQTCbp8tuqW6v1gQ5AU1RG8lclkF2kZ7O8VmzYqv5lslYGzfRclZDsjr6GsR6djan3PWR80NHCwargCdDbOyFPHsaFBt/Dp2MX8RFPXwa+xD1/T5dCvPPyhLLyHimdj3vXUSi7Zs2uZzM9vL7p9UjRTNbKQHq2qktHgTr5eqmdtE0+ustldejUr1+/yqGHHhqGDh0a9t5777DFFltYpmBEUbCE+QYPUsVWFdKKSqcUGqhSetJ9muqBU57KthXiK51kD69fGW/Kq6ynS/VVXOE8OUKefVId4OH5giL5IC+vTI50xCksqK6pDNF7QON5ej1AyiPN90hp81BWXiiT6fOKdCWdcF59PbBTUX5+XaBdloViuUol499p4403rixcuDAsWrTIhG+11VZhhx12CAcddFDYZ599wo477miFUkiIlPDK6MIrTcp6pcsqkAdfqTxZggwrIwJoZNjU8EA82qKPh7dFXr0kvwjSNaVL072evn6C6Hw5j1QH6EQvpPI9fB68vTwP0ZGf6igoT3wE0tIy0jMNC2V6poA2zzYeZeWFMpk+L+UluV6HtL4e0BTlS454ycd17hzDyyP/mEZ6pwEDBtg9Oi7er4clS5YYg8WLF4elS5eGLl26hF133TXst99+Ya+99gq777572HTTTU1AHuqCVjSWlEnDII2n8MZLy3lIbt6FKLoASs/TuRWksvJQxruZXsDr78NFwEZl9gTwSHkVyQc+D7vrmnk54lkWBoqnMoCnA75sXjtJeaTlPfLkpSgrL5TJ9Hl5dQFKJ97KtcqDl0N5bxvykEF86dJloVP//v0rZRWjMJ2fGZ/Ov+6664ZYJgwePDh85CMfCR/72MdsICjr/AA+ghTCx0l+WWXT8kJaRnFPAyRP8OVkMNLSdHSjnE9PkcrKQ6vlPV2arrgPF6FMngCPlFeRfKC8onQgnmVhoHjKC3g6kJZNkfJoC20K8sv6g1Am0+fl1QUoPY23BXU5mR1hAZ9lMblLld1zz78YbrvtttBpyy23rEybNi306NEjrLPOOjaDg6xgY+NSh2DmZ7bXCoC8nj17hu222846PQPAhz/84fDBD34wrLfeelYmD76DAcn0kA44dTrR+TTF0/JCWR56eH7q4IA00B6+QkojnkrPy2vGc03C2ydFK/b4BzoGlQrtNgsvZ5ke0blz1n/HT3jTOvj48ROsb3aKF61yxx13hCuvvDL88Y9/tM67/vrrW4fngvkGrwuYXkzKENfMT0MgzOwfbw2sw7MCoPMPGTLE4qS3Ct/48zpCqptQRJNCHb0IZWUBeWU0efopLS2j/CJeguhAM9qOhq9PirK8Ivi6AF++I+tZxKs9OncE2lI30aZ647gfjzmWNm7c2PC7ex8ML7/8sk3cOKtf/FOTtmDBgnDLLbeEG264ITz33HPWaZnpu3btGrp161alyqDOkQmqz7TQMttrZsQR9ysAhMOXW4BBgwaF7bffPmy77bbm2Axk57979+5VSe9fYA/ZyNtJafjYp2gwLYPPV3lQVk68BS9P8OXTvFUFbODh5Rbp06puqmOZXcjzNvRojz2KypTpUEZHXPmErW9V01Tq7y+/Gu67794wduxrcYLtWevg9DmbiKORjWtaiffeey888MAD4fbbb7eZfsqUKdb5evXqZYK0xBco7xutwqmSoiOMj9PGH8CHpm/fvqFfv3428zMgsAeAr/hGG20UNtxwQ7s1YGmCXgxGqV5FQI6eNlDXefPmhblz59bC+LNmzTI9Z8+eXbtFQSaDGYbcYIMNTD4rIBx5ONJZzbQC6g9f5AjeXnkQvejkA/gVIW3M0OalCWneqoJsIOTZAhSl+3ARvL1Aaqeiunq6VmXm2RD5Xn+QJ9PTSYbi+KSp3KLYb5566unw6KOPh6lTp1gHzybkSmzfy8L8+fNs0jz22GMaZ3QpmKfASy+9FH73u9+Ze+WVV6wzIJTOT8Ong+EYPSjvlU3DikOni5ymE4YX+XRKfNII40ODU8WpID46KC5+ADpAhyWdwQWekiHnQRmVU1h6KSxdlI98ZGMXOjsD0uabb24GHzhwYG3Vwq0MeUUQbwBfQFxy5QPkC2kdPDwdgDYvTbzTvFWFMp1Vd6D6pijTU7ZKkcos4lGkW5lMX0Z0Pq3IvtIV34dTOu67HxrxUHj+xZcoFNvbOkYDPe17yZLFNlEec8wxYbddd7IytRkdeObyQZ5CzzzzTHjiiSfCk08+aeHx48fbLKdlPg4elM0r730gmlS24kA+kOFEm9KT7+UqP+WV8vfllC6kPBQXxIuBQHH4eac9DGj69OljA8CWW25pexfsYXzoQx+yPQxWBnnIk+ltV4a0PnmAh/ik160VpDp4mT6vKD0FedDKbwWeDlvn1aNMT48i3cp08WWku0AcnUDDdevkOnf8F3tNzKnLeO21cWHUqFHhmejefWdW6B5vfZlIKINjAmQGZxI54ojDwy47Zx18eSXKgm9scDUtEFwTVmUAFJefZzhmeDr9008/bY7NgLfffjsKn29lNOvj4+ABP5wuhniro3h9gNdBUB4oSvdI+QkpHy9HYZ8GytKpE75HSg8NjovESKx6s3/BrQiHlTi0xFOMPfbYwwaGFPDxPPOujUC+kOrmId3LaIpAWQ+vj88r09NDupTVzdOAVmzQnroJ8Gi1PLSpPVUeR16evnPemx1GvzA6vDD67+HVV1+LE8TiSN8lTqbZyjUrW6lOHMviSnGbcPTRnw7bbL2Vlc/qSduoTohRkNWcDC8wI6xDSgKUE6RwHujkEyZMsE7P0n/MmDEWnzRpUpg5c6Y1bqCNA1YBWnrDE9/rRZr0kszUUOSTJ+MKeWU9lAfSfPGUL+Slez55KOItm/owF5FBAB+w9N95553t7ALHlTm8xIDg4fnAWzb0SHVIIds1o8uDt7vkC8pL08ug+oCiMr7OgtcB+Hgqn7gv2wx59F6O8vF9uvc9lsRr/NbkKeH1cePizD02THjjjfB27B/ZpJg98qacdKbL0re6du1kk8DBBx8UNts0e4oVb3DjOoB/GaBFZGFHL4M3vgflVUloyvih6JtvvmnutddeCxMnTrRBAMdK4N1337VVArMc/LQakA9v0nHesN4X8uLAp6VIyzSDp5d9fP19fsobeuJeL08LH+wAHXsLONKZ+Vnm77///uGwww4L++67bwNfyfF8m10X4OnbCtUFSHfB17OZDgJlhNSewNcPn7jXW2ny81CWJxTRtMLfY+HCRWHa9OnWzl9//fUwefLkMGPG9DjpZbdzLMfhQ11x7K537pRdf9yiRQttZXfAAQeGT3zi46H7uusYXzo4QI8ukV6QzQs7eo2gBeVBWmF8D6WDVnmy6/1GHN0wCoMBjtUBg8P0aCwaPKsBOj+bX/joTT0kz+skSDf5efUGregp3vg4uzhVHopLjnzlCcQlK01P40B15MJnmy9L7NEoTx8Y4Y866qhw+OGH28af4PkQhpf4iC8gTfB2aRXefpJJXPLwSS/jXaSD9AaiEV9de/FXXnaopM5D5VcVli5dHubOfy+8997cOGG9E2bOmBE781TrzNOmTY+T1/x4vRY3rF47d+5qehHmmgLyli5bGpYvjR08zvhM0Tt+eMew3z/tG7bbbrDRAMwQbw6q9Yr2sXv7Fa91u2b0MviL4eEvguTk0ZIG8nh4cJpv7Nixth/w17/+1RwDAAai82tjEFmS4/0USse1agfPT+G8xianPJ8PiAtpuo+DlNbLoZGw1GclhB222WYbeznp85//vJ1Y9FBZzw94foTb2ibEF3j94ZO2AXzipCtuiF4Wqts39XHUsS3gkdOCBTxSXRBtxdHuxTbDIof2kj0S7WTtpmvXLg3ylizJbqGwLZtedGQeu+LmzJlTeyyLW+z2W0CXLrTD+i0pLgPhLKQ6QZPN3PHeO8YH9Osf9tv/n8LH9tozTmZ6ZMuTH2wW6889ODzMYFHX+A8eQq3NRaZV6+aP4AgHdeXaBwQKnpcPy7DANwpBhijShVnt0UcfDY8//nh45JFH7EkAz8ZZ4mrGp2yt8o6PZKvRtQrK0bmQowGGNBwzLbcoyMVlDShz0gU6QTpJP3zopJv4EgbkA/RN+ZBHg+GsAHZhuXf00UeHk046yZb4QspTIK0tdhCkUwp4+TzJs3plrTRrr1V9slj8y/W20IqA3dx5c8N7saMxe7IkptNx2zeHjjeXTjk/dr65ZgvmNHaheasLv3OUnc32mfxszlMbw6ZRQFXP5bFjEURfOhj5CqN/F0vL9KXzZafVMhrqndE22iCrZ4ZKTOdenVXqJptsEnbbbVfbh+m/cX0DljqIh/gJnm9e+irt6KlwXzGhiG+DEVwYeuJpuTw+LP15EnDvvfeaYxOQDq/n3L4MPOlUMqLSiuQJdHIO8YwYMcIGFNFxUWhk77zzThg3bpwNOpw/YEMSn30IaLGLOr4eSeLLdrIbTtfH60MY+Dhh1QNHGvpoRtpss83sbcQzzjgj7LnnnlZOEC2yxBOQLvlAcoHPQ650BpIvPw+VTvV2AjqHFWfq2XPm2oqNvZxJk960jv3OO+/aIMY1oLN1jTMnclgK22uaUZzqobpkPva0aC0OSKuSRWQBr7MvKzpfN1xad9lD1zPjEdtHjDN4kL548cKw/no97QkLG6xbbum/CVHftccyxAXxAnl6KB00HJhZ25AaB/hKeKga+ND7MsKMeL901113hXvuucee/XOfz641SzY6G/LU2YEMJ14Kpz6zJvfDf/rTn2xGbxXsOTz//PPW8Z999lnbj+AEInqhBzrR6eGJHOkhP08fIaWhTmpwOHV6TvSdcMIJ4eSTT7bXkAXKqYx4eBlK83k+HVA+zUuhPIEl8vSZ06MtJobXXx8fJr4xKXbwidaZmT2xCWXka2bN2HgdLBjDyMj8+NfSQD2fNHiktJ6mnp6lUe/8tkkcOgaarP5ugotlli5bEpbGOkK36Wabhg99aPvqzN0vozFAL9vWB8HIrtr26zKFuuzGji4UdnRd5DUJdMhDkV4pvaomA9B5BJbVzPa/+c1vwv/8z//YDMssj6MReRneRLqwnjeg01CWN4bYBQfSR7TiSbyoDgwaDEgvvPBCGD16tO09/O1vf7MBAH7Sj86PDy/p4KE6Ky+9ntKJ2RuZOE7wHXHEEeGrX/2qvYnoIV4qJx+kfCUzC+NbNGue1TyBpfbr4yeEv//9ZRvoeOyKLQEnvri/5bpJNmF0zvjCS53cyYmdg7BPz3Sq3zc3gjzqkM7Wme/jqp+vI5B+8uGJzXnGzSCF6xI7/5Ah2evdHIzaaKMNrCygDPSUlz25lejcmTaELHST3KpCBZAeHmt1Ry9QraESniavgkA01EmNJgVL6htvvDHcf//9sdH93erOLrZmjjKQjwwGDwaMz33uc+GXv/ylnYNHpmg8VMbr3EwOLxr9+c9/Dn/5y18szO0AjZ4VCQOAH8jESzJ0LQnLh1750KM/94h0Ppb3X/ziF22256UjQY1RfIDXX+0m42vJMV6v19RpM8Orr46xcxV0bgYY9Fh33ewlpvr1wdVtk8rzeldJGgB5VjbTI6PJIcyB+KZQunRJ6Qhjw+XLseMSuya8o7HddkPCzjvvErbeassqZR3iBcSvUTbtRzRcwyxP5Rppi7FWL91bge9IVKVocJIRvYF82IMOxMx8zTXX2P21lvdalnt6zxcffdiYo8w3v/nN8I1vfKNG5+WpjIf4pulAeR7M/HT63//+9/YCEjMiemqAYtmfLXkzm0hmKsfHoaUODCDMrIS5LTn++OPDqaee2vCBEejVsUHGB56Nuk6YMDE8HVcmL7zwku2bsGPcrRszNa6+uZTqJzRLyw/DK+Nbhzp9PQ16gXSVr6dDW6cB5GMfSJYtY7bmGfdSs/lWWw0KQwZ/MOyww4fsXju9lcvkryi3GFn7zvRo1BtX1N5TNJx1Lxe4dkKNRIZrteIC5YF44Hs7TJ06NVx22WXhzjvvjDPRq3bh6EiNjbsO8WBkZ2bk5YIf//jHNssL2dKzUY7Cqgfw+aSLd56egI7PtwW4FeFcNB2VN+nSVYznASTTp+MrncGCjUU2vvie4GmnnRY+85nPNHR6HeRZGuvGPfbr4yaEV14eYzbj2TJHN+kIXhc/SHgU6eN9QXHxqhY1rFiOzBXrLChNyMplL1NxzXDate/eo3vo26evvauw7bYfsEeZmw7oXy1Zh3hm8le8Zs1R19kDfrhm7V3y12hHV+VXBqqI0FZ+NUPEcoQ1cCjN8+Oe+frrrw833XST3UuykcVOO+XSZbB8GgezGG+rnX/++bYUFjy95GjgAVnDrddPYc9fvtIFZLLx+Ktf/cpePmIZicueFddBeTUWLyuVoTx1euq7yy672CYej+xYwbCf8MCDvw9jX3s98uxiA2L37j1q/OFV75D1ugiSV41FV8/Lg9ev7jc+0vLQBlomul4nlVu8ZDHJRkc96Rrd1+1qgyW3MpsNHBi22Hxg2Dx27r59+tjg5QEfWCJWvKVDJqO1jq6ywNOLRxpOobwGPjFSj61m6KKvaaSGSY0EUqPyjv4ll1xiz+25z6RRs1SmPqqXOi1lde/LM9ILL7wwfOlLX6rVXY1S5SRbYa8XIO7p8L1+qa4c5mD/4brrrrP9B8AAhb5A/BiUpBMQH+nn5QNt4rF6YeeYTbzjPnNc7J6dwugXXgx//vNTNqPzQgb36czo8JcsBgvg9c/ChPKvgU9THF+2y/h2jT7fNyCdzTCW2dgxux5aVVD/7KlGNxu0e/feKLo+duZg4MDN4rUaEDbasPhTaIDn3zw7z/g3Xhevn/xWoHYD/PXwaAs/8I+OHiHDYrg84/lGpIumRsr9+M033xyGDx9uG2RswNGJ1JiATIyP49k6H8/gnvdb3/qWzbRA/IHkpL6QZzfRCaL3aZwlYJZnec8KhQauBo/OlPFygPh6XbCJ4jhmQG4VWDF84hOfCKeffrrt4IM333wrDox/Cs89Pzq8++6sKEtPDTgvkHV4Zs9MTfjJBuYZpJOvi0+r68HnzBaaLemw3F4wG/fu3Tv03ij7MEjPnr2q14kVTtnjUPhnvFVfyQLpNeio9qz2CDw/5Pr6FyGPrt0dvVWhZWhmmDwZHSHXw1efcGpYgDwZn3yFSfe6sHF30UUX2f08p7NoTDRolYFf1qizpSGDBOU5l/6jH/3IXlARaKiUk05legqpPYlLP8qk+nL45IYbbrDPh/E+AXkMUtpAUhkNQHJeJ4WlLzJ5Rs/ynuX7YYcNjZ3+y+GQQw4ynhx8odPzyHBGvP1hRmSQ4chptG50yJDt67oiN6bG9CwOLCmCjs258O7r9ghDhnww7Lb7rvaKb8/qANoMmayMMWHVEVAHb0f5ZRCPVpHSSx7p6fX0cQ/pC3w5pbe7o5cJbRXNeOTld4TcImCK1OBCeuFkTNF4nWj0v/71r8Oll15qZ/F1L69ODi+cytIx6PQf/ehHw7Bhw+ylFMHr0Kzeqf4pyBdNSscS+9ZbbzW9eXEIfXU7AkSf1pt08fTyCUPDDvusWWxK9g4HHXxQOPWUU8KBBx5oNAvisn/UM6PC3555xh61gWymZ9Oufv+r2V4yQKdK57Bo8YIwf9788OGddwz/tO8+YedddrFjrSmsDMlNWnpRXdoK6t2WNtqqrDI6q6OD6JS+xjs6yONTlLeycpsZyxtIcXxk4gNf3pcBqX48qvvBD35gszxLW83ynrfCmgnZ2DrvvPPCl7/85dqXZiQbpDLbCsn1Yc+P2Z2ZnsNErFKojx7ZAehVR88H+DhOqwEu5+LYMdm55yDMsccea/f0/gjuS7Gz8w20MWNesZdGkLHOOhwrzu6rsS3heWwGxtmfgZFBcYMNe1Y5IB959fMEzaC6p/7KYGXbaHsguwOvf+16xECdog3oCIM045GX3xa5vmqUIY4rugieHvh42YXzdH6AIkwDBXRiNsN++tOfWuehA3M/6+si/SjHBhqzKY+x6PR65VQ0HdmQ6IxAfD1vjulypoCZnqO62sTTLQllhLy6yA7KI404m5PUkcdThx52aDjttC+FPXbf3WjAxDcn2S7+6NEv2CNOlvZx6LAz4uwBHHboofEePPvmHvwyOdQDmdmAtKZAHZu1UekseDt6NOMj+PLpdQBrdDOuvcBIgjdWirTyxPMMl/JTvIy3R5E+Remcbvvud79rPgMBnYfZUvpRDnriLOmZBf/lX/7FltYCD0WpCs+olzOLufvZPGAJXxvkZPfFjXZSmPzUVmzkMdP/9re/tU4P9MgOWsripDtQegqls1exaOECe7WTzbPjjz8hnBKX93w/T5g9e074a7wFmj1nTjjggAMiXW9LlzzpipjoNaDoGqT6KQ483aqC9BZSPb1+KwP4mn0iwxWvwlqOoovXXqT8asZp0ciix5TShzBOPGRmz5fNMDbh2AxjhmPJ7jfCBPjzoguP9Gjkd7DZN2seRNZRPK1X2SXbMJAdGMnSoWMnev/9Dwj77rt3Nd0VqKLIBrxJxqfAWd7zQg5ldU+vZbZsqYab8vJp3IfjFiyYH1c/i8M222wdjj76mHDCCceFnXba2WgEv/EnexfpCY0gWoBsyS8q+34AdQDUQfVRmsc/OnpEyk/xVnkX6aN0jE9YjYowdIQBnfXqq68OV1xxRXjrrbeso+iC4dOweVbNwECnuv+BB8KcORxPjTM5fIxLhkqlLr/KvorIi1Nd1Zj485PZRx45tJpaDGh9HXACJ/J4XMfynt106DTTyx6qS+qjV/ayRn2ABdR3YZzpeR7Oj3ywouHsPV85FeCBy7tOpHsfFF0byXw/om7HOlRn0mvhGKhb4n0Cr3JHXKSUX1G8SFaRPj69iKdvfF//+tft5B0zIxANS3deIeU8u7Bk6bK4pJ1l3xNrBXCKUrNIFSyVAXKK6uYhfcpsohN53GbwdiB7E9ya0PGpK+W8vHq4/uiRmV12Ic5Ahw3gxVt1nMSj43/gAx8wGkE6CZT19vW6FunfEVCdUvkdCV/Xug0zpPUk/r7s6KsL3oCpMVtFal4uPnzUAPgkFs/e6eCcMmNXPpXLZhSHW7hvFb/26JKH9jRG6ZfqmepEfVjeM9s/9NBD1mHp9LqnR66W4ZTHl33yZJDH6ocnE4BOz2Ylpww1aAHxgJZbiTzdPH9QlL8yaMYjle3p07KKqwwgjMNuZXLAPzp6C5CJmhkzD5SlHI2PsHbhR44cGb7//e/bl2+4N6cDkCd6wCzGZhw79WeeeeYKnWJ1Q3YARbbwHdXrSDqvALOZx9l73hWgztTd81JYdvA+0GBAnOU9nR5/t912C5/97GftsR2fxfaAFpdnM+Uhw8sWlNYWiF+KNF3x1Pd5HrItSPMAkwEfVWGTl81SVleAU5j/Jzt6WuU8o3lg4FY7VspbFwxfPE488USb5Viia5lOHh2Zzk7j5Zk732/nvXbtQMPD82kPpE9ZnUSTgnShyGaeRnzwvSxk/+EPf7CjuDTM7Nl6t9pgJ4iXlyVe+D5fnZ7yfNSBnXs6PiukPKgsIAwPyUE/oT22btYhvd4KSwegdMV92IMNWl5RxoZPPfWUfQsBOlZM2LOhfPxTr/H/EfgLCVam46QQb4ysC46JaYDcY/KaJ1+L4bQceboYmq1ZyvOCyFVXXVX7ygt50ElPXXj4t1X3Vsq0h28K6ZjXvFRngRmINwJptGw4MvhxT88jR8/DlxN/4G0O6PQ8o8dufP2WM/d895637fRUwwNecuJB/WUHny7kpQmel6fx6YoTVrof5FIwiPFCEh1aP4XG407qwwCppx1eX3xgesTIPzr6SjZqD3j7i0dY/HfaaSf7OAQNmXQg89M4+XQwsxybTYCGCuxCVZ1HezpkK2Xaw7etkJ3SOrFrz5t2vFfPoEcDxl5FnV5x2ToFgyu25X6dwZX3xhlI6x9iXPGrL0WQDHwvz+sFCGM/b0dP3wy0A5bh3N7xohTfN+TJBm8/0pmZsWWPlL9keH0sPSbUU9YAUvFFiqphkG6KOzqlN0MRnXiBIn6eRroA6HynSPMI42hUfK6KxgY8P5bp3Ec99thj9t02ysFHEH9fZnVANkCu6tIMZbYRD3zxBrplIc/L4FEitzhs6HGUmNmLTg+teIkfUFmvg/LlkEXnx9H52SOgs/NCEYMAn83itB6P9DbeeGM7wchg01Fgg5J7ZzouKzvOI+inynjfgMerLMGpI/VFNp3az9aAevm46id7ezrzY0Ld4msAqXh/sQTSlA7I8xUir5VqiM7zSuH5eZ6+wZbB81f4U5/6lH1nnkYjnvBjpuGgDJ+L5kUSaNVIRSdewIdXB7wOvl7thXjgp/XzMvJk8Y05nkzwrJ6fMWJW0/IeejV6+UV80jTsTeen0+NzTcQDGSyLcezq8zPXDNTc9zNAIB868ijDgE1Hhidhbh/UsXmbkZma600aAw1lAXVQZ1aHBr4O8hX26aIVlAZq5WKgnrqGIaU8SKspW83zdHllytAqvZeLL+M3gy+Dzzn1iy++2D4pBSSbRsVFZ/ZgmQZobJqtgGjlr0mkOglpfX2aoLJAtMDTeR7ex6W2x150eGZ7ZkQ6CTN9SudlAclSmo/7sC9DmGsF8JVHZ1Y5wgD5hNEHWt9pFSad/FS24kAySFM+UDogDVnwzKMhTnvCMbCsVR1digu+Ijjlebq0TDNAD5qVgc4bry0yoKcsR1b5oQSWgOrA0pdOzkXnGTkzB+k439HbInNVAn1kA9Utprpwvc65jUmJMT+jLgZ8gPGKYcUBYXSQTPDiiy/aTM8BHWZ6/6gSeNqajk5GCtEAtRVdE1/e0/n0lCdx8VGe0uBLGyDuyxXxIw6Uhi/e+KxGtErEsfpgP+KYY45Z+2d0Ia/SSisqk4cyGR6ev+KNDT0fGskxOEtyaLnXUrr4srHC21l8IEFp0HreSgcd3elpGMiCfxFvyfaQTtV+FPmgN42OhOZ2FWie8LEfM6jwYk6lthT2cqVbmp7aCrBxxWbmgw8+aL9UyszJIIr9xUczatr5ZA+AHOhlH6V5eFrRlaUB8fBpgHRfBhBGJw0G6TVSZ+YWgPqwx7P11lvbCUr2g9hs5LNlwlrV0dsCXaiiRtoRyLv4pJXJVD6fUmL3mPs6dXL5bLbw3Tg+I+X56WILujR5MtPGAvLK59EVQfIElc94kFfMC1rqxb3oO/GedNas2WFuvEflt9HmvjfXPjKxZDG3K+yCs6Tk++fZSzlRQlgW41ttPSicddaZNT3wfb1T/VQ3XSdfVx5FsXPPSzf8GAYdno0tHcXlOgDVMY+30hQu8gWfDtI8kFc+zROoF05LcHVuaOjMH//4x+139HiESCfPg3iudTO6h690CgwA0g7QkZAMYMaKLr0YHsrjV17ZxeV7ZfDwozL3S+ysc5/Z3jqkOiie+soT8soIaTzFokU8m54TZsyYGaZOmxqmTpkeZkyfYWG+gbdsaXXWif9hY2ECVfnwprrSh7hk0sljjm1q/fjHP7R00RXZBtspT3xYRWFrwjgBWs7fcwqRL/6www0tnR+nMvDL009pIE3HB74c8HmgqIzScLQROrSW3+Sxt8PTAB7NMlvTudMz/oLaU8rfXIzUa+HgDbm6kKcKSubpIlpVamVQxKvANIV0+OjJc3AaFRtEgDR1dE4z0dgYkamXyqRAxluTp4Yrr7oq9I6rgsGDPxgv8LY2SPTq2dq30NqKxXGmpdNOnTbNOjGPgCZPnmK3Gfwi6fz5vE2WLX2jhlWX6UpdrKHxbnzn2LBiOveIvao71HwymQ8x6h5aBz3Aer3Ws6O+PNLaYouBDXYnDH/ZSW1B6aKRLxohz7bcy3PbxG/lcViHHX12yeHHrK+lPs5vnMkH0kmQbK+LfGjRCdsBOjNh0ujUoqX+HKjaeeed7XAPx3o5zos+HqIH4p+nT4O+MaGe6yCD/m8H1ZcJZLA8lDUe5ZHOSMwoTIMWHfwJ02GGDh1qX2xJ5RL2Fwv6a6/jm+x/tMc5yCAPf9111g0bb9wvzoB9Q79+/ew3vHrFzkLH6tqtq3Ug+3E/u4Y0skqYv2BBbFSLw8Lo85mmubFh06l59MNjH77OSqOj+rIDPKIm0WnGYXXSOXbY7lFWT3tc2L//xlGX/lbnvuiy4QZhQ/vRiFXTdvLapWynsGwIREs51SsPHM6hw7PS4nk2X9bhNoTBTufpWY3RSeGZ6gDg7eVDw60CKwauIe//cyvHSo/XbVlu8+WgIUOG2Jdqy6A6yhckq6heQmFHTxmubrRagY4AslTfInlqKNDlNTRAPr/NzjvezGDZzJeBEZwORSPigIbKePhGDK/Xxo6Ls85fwsSJb9juPDx4/3yd6maS9JEDbGop7m8ZhKzzVutAVWMxwioDPT5pPXr0tIGETcXN48yymX3rfJNYt/VD99LPJGeQjgCeQDrjK6x6i97rm0JlPXya5Ag+XXRpWpk8QEdnkOZMvp6FM0DmyeRWgIGWQRefVR2dXKu7IoiHdPRhxVcGa+1mHBcdqJK+sjKAN8rKAD4yg+fnw75jFckk/b//+7/tM1H+cAxgNmC5/vDDD1dT6qBzAcpLBvANcMnS5WFKXHJOfHNieGvSWzYb41jyZps12e9/xe5tPp8pVifPfuQw5nTtErrEOAMAP3LQs2f2DfoePdYJfeJs04dZOboBAzYxv0f3xl918YgLUVhmcOboVEH/RtsJsnNRPRVXWdEWwdPnhYEv7wcUfOi8DB8GPtwRSGUBtSuAn+oAlJ4ipRNyecTEFTmsJUA1qecbfUfDd2IPL9NfkJROZaHngAy/1cYoTroaFTMBX4M966yzrAzP2Dk5xbKNzRXo6JisAvDpjAC+8JHsPEDD55dsR9s6PLqiF65LvO/kPjOErrFzrxNnYjp7a4j1jLxVW3hQ9UyV+n2yHrEBpeGnwA7UE3iaovqJVxHS6yZaL7uVa6i0MlmAMr6ch+fh6Yh7vqSnNIJ0VV5aVrYTfN2aoXXKNQBVNkVqoI6AN2yeTKALkMKX0ezsLwphZl02V7gPzL7Vtr8djaWjf/rTn7aLxkYQfNTZAXHJhY/8KAnONde9eze7Xejbp3fov3HfsHG/eP/ODN1no7DB+vH+PQ48/DJJ2sl9neq8hdiJbYamfpTj2XLmW27NThkPzycPpMtWCoM6n8ayPr0IKU+VLyrraTxalZUHyRcPT5fyJZ7HJ6+TK9wRaLzqDh0loKOgSqeVL0pvK9ILAjw/8oucl80mHGHSgWjoQHRwPiBBp+e0HI4l8t133227wGz+cJKJVzU1o3uIT+bj0BE56IDLdr6lD2Ef9+kKS0+lKQ6Ix7/msnDdJvhyHpRv1Ym+vcjjpTB6KSwoP3W+Dml9FBc/X0bhFJ7OgzJF/PJAuuiBp/d5niYFebj3RUdXpWpKu1nJp+M6Ct6QwBs4dZ6OHyRghxYdSRcNHZf3hzngwH01J5rY1CHMMp8NOk434Ze9Ogkv7yM66+RZWPrg0CG1FZBOAmHo0nSvv8p6X040zZxHWXqr8OXTsPcF0aTOQ/XLQ15ZOY+i9DKbCT4Mnb9+RWVaQWFHTwWuKcgwZWhrpfMgOd61BdLhk5/8pO28svQmTbxYlvPBgO985zvh2muvtQMQPGJhhucRDgMES3vObEOv2Rh4Pr6uWZg0YlleSgOIK018UkhWiiJ6IJ7ir7iH6lHEv6ORp8PKoKP5CUU2FZrle7SiY9PfR9eF9iPLqgJyvA5FjcPrAo3KtKpnW+Wk9CnSfDbk+N01npkqnRmcjs1HBFJwSIJHN5zPBiztKZfK9z5I01I/tYWvJ3nERaM8Xya1C3nin/IqQhFdqu/KAj5APBUWyuT4PG8TjzS9PXrn2TMPrdhWNEX5Qs0WsUBpR1+daK8hiDersEd75TSDv/gchmCnndkdwItnsbxtpSOMdGxmcp7TQsusrwsDxA/fy69dvJzrhRyVSZFXB8loKzz/9pRvq22bQfzg5Xl7tCKHsh1ppzIUyWoVbdGpM4RyqwtqJL6xCF4X8nF5F87rSziPhrSy8p5HEVqh8fXg/hzw/TM6sGRzQdmoY2nP9+DOOecce3ONmfz555+3Tg6t9CqT2ywffXy+6FN7+rhPB8pLoXTxFF9B4bKygHLyla681PfISwPSxSNNkww5nyZ4fTyUDtI8D5X1tLKv4vJXppODtL5lWCPP0REpw3llfUMDPt8bxdMpPeUF8uiA0lWmyODQSc8iGvKAZGvZzeYb3zLnlBw769pF596dAYByOL7yesopp9Q28PxueyvyoUnrpjL4PiwQT5HSCGlaWrYsX7JTKL1IZhlURjzS8nl8ZRNvJ2iEPFqA7+lSvj4MfH5aDtm6VspLy69K5LeeCJTxbmVBJT3yeFJxOQzifQ9Pl+opJ6OKzoM4eXIelBOPtkBlJJPwwQcfbO9GcyqOI5MMAuTzvJuTc7ysMGrUKCvPs/O2ygTw87ZVXaUHyKt/6qDXBqJ3KfLykU9Z9Cfs8zxtmi4d5VR/n5a6vHpRTg4oHRBm8KScB+ly6Cynst6X8/p6mTjKeqTlAD5tQLJWB6Rf6Vl3DxReGVAxVdjzLuILDS69QClkMPikOrdSNqUhTbykWzM98nQgTDqNjI24888/337AgK/Akq43txgA+CzScccdZ52laEaXLoKP+3waks7Y86klXs/k2C1vzQHefjv66KPtFoJn9mwacj7/0UcftXzo+ea6XpNU3Rms0IfbDzYZeQTI9+b5jXIgXXE///nP7WuuOh2YwtsIx+0LB4j09Vt+eJJv6DEgki/e+AB9sN/hhx9u5QC2Ix0a+MsGnEngYxQjRoywQRVZDLLcNh144IH2Ky/YhHI4eOC+/e1v24cadZ0AfNlz4Zd1fL1Ix3HtuL4//OEPa3Kgw3acgjz55JNNZ9EjJw/kCV5OEaAvoqvxioFcxEo3uPagiEeaLueRl5aHeIELXRHEN49GZSUfl8ZTqExKF5fjldjxqlQZJk2aVLniiisqsaNV4rK+cu+991o69LFzWVjwfFMnEKasl/Od73ynElcLXOGaix2yEhtZpV+/fg3puI997GPVkpXKxRdfXFl//fVXoBk0aFBlp512WiE9dsbKhAkTrKz0ircjlR122GEF2jIXO4GVBZdddlll8ODBuXSp23rrra3+APtJh9i5KnEQaKD9yle+UrnqqqsqJ554YkP62WefbWWA7Pi9732vEgeqBjq5J554wmh0nSVzzpw5ufRx0KkMHTq08uSTTxod9LpueU754luGZnTiWdjROwJe+SK0QiPk0fjyeU40echLx2iky5eTQfOMmtJ536cXQXlpGe98nudFXI3z3XffrWyyySYNjSzOkpbn8eabb1b69+9fozn33HMtHV6Ajkt6nCVqNB5nnnlmLR0HLyAdhThjNdCdddZZ1ZwMv/nNb2p5t9xyi6X5uvmyONWTQYG4+MeVhaUrP65gGsptv/32lu5x0003NdAwCAB4qA4Myp5G9jjjjDMs318TsO+++65Ai5s3b57lA9FT1julp64Z8uh8efFfpR29o4HC7UFqiDKI1hsJ5w1XBtHKB94XP4G0vHxPA4iLNs0D8fbAZg4aljoAMxeAXg1YZd95551aQ7z77rstTR3lyiuvrOXh4jLd0qUnskj3jXnu3LmWJ5q49K7lyd13332WN3/+/Mro0aMtfMwxx1jea6+9ZnFWQeC5555bobwQbwtWyGM2BSNHjqylST9mdyD7CfE2pEaLQ2cgG918880N+XKsbgB0omVlRp63CS4u9S0fOsnPcx2JPP7lN7Hvc8Q6mx+Nb34rUBl8yskRV14zpHLxo7EbeAo+7tPbCl6OiR3VeCCLDx7Ehmp5xHU/qHy9WAP4kKAH9+we3MsDygG+DQe8PdhY9DbiFCDwdeLkH+CTSIcccoiF2SsAfIAhdoaanvyYhQf308K4ceOqoQzI4H1v6v+Rj3zE0rhfRhd+U537aumep4/SeLsQKC4d/C+1Au7DkYWu0peffUIOexge/H4eEE/8PNeRyOPb0NF1kfwFXJsgo7YKX49W6ybjeGOlrgxFdD49zfMooiGO7nn14HPHfAfN2+fyyy83n86jsuIpn404Pkethqz0v/zlL+Yr7jsP+MlPfmK+8jnxxzkAD34jDEhPPsTAByx4eYdzA/wWGuDRIt9mF8ST13g9GBwEziEA0XLoCN04XgwIU29w2mmnme/rLyguHfnEF9CgwGemsOnPfvYziwPZmA9PCl/4whfM5/XktI3yjTcgnmsMsZL/JxANXQ2tGXj57dWFcirrw5ttthkttWHZKLBkZEmO82WJC6IRxEP8Xn311WpOpXLJJZc05OFefPFFy2PZDS/Acp+82OnMHzhwYOW2226r3V7ceOONRidINyGtU+x0ln7NNdc0pOOmTZtmebGT1dJwLM0F6eUhHeU+8IEPVHMyIGOLLbawsKfDHXXUUZYeBzSLs/GHHsqXfvfff7/RefuvCfyv6uitGHJVGntVX0j4S4Ya7syZM2uNSw1du+jQ4FrRSx399ddfr/GT4wkBLt2xZ8ede2kgWeIjGhq875RycfltdNLP67lgwYIaneq0yy67rNAx2WR76aWXrAwbkT4Px/0/EP8U4i39Dj744GpOfUMyrjwsTocXX9yGG25o6diAODv+N9xwg4V9fWfMmGF0efJXJxrWGdEg1dDqQ9Sh0BUhpVE4GtjqIJcH6MhTGcXlfHqr8PS+vHdlKKJL032cugK+Xw5Ud8AXRD04dceR22HDhoV///d/r7kLLrggfOMb3wjf+973astyLbnhJxkc+sHFQcXigOfPnM/ni6WyG4AP97BAy1jyDjjggNr9KuATxkByoBUPvsMuKA3ZLIN5xs4zf04XUnd+Cx3w7F+Q3kceeaT56EeaeAF+2FD2Ej0f7hT4MQigcwJangPoqTv3+Ly/wJKdWxd+X8+DZ/B8cw9IPjLlVhW8DGRaveOfGmJGNbT6gMwiVwRGRzkAreLNyvv8PLqUdyuAhy+X58pQROfTcdJXYRAbOK23YdnKc3Sg2XXWrFk2Iys/daIH6aMzZvF479qQhtOMJng7+sdmsYGbz863njNvt912RufLoCv1AuntgXauU8guQE8KVAYXBwzLA7KZbHLRRRfV6OQmT55seeC8886ztAceeMDizz777Ar0OP/obscdd2zIiwOBpeua+bDiqxqS1TCjM+qsbiCzyJUhL59ZoVl5n19El5dWhmhP81XO826VVxGd54EcHHHJ1C90xAZUo2MGB5olOaHFSzbnnnuupWv2BrHh28csKQ+0ISVezMJsdp144okWVzoz2r/9279ZWGWlk2Y2ryc70mzIAc3C5J999tm136YTHzbBPPgpYwF+0Imv9OGUnqCVBE8eBOgop7w4mJiv8nzOSz+4AfTUQDM6q6S8L7n6lQRvJnpgN4AMySUsmasLJi8a7H0HRqh4QWqjosKKrwlIPrqkrr16pfwkQ4404A+/4I4//nhLJ58ZTHScjvN0cfls6Z6XTsXFxmH++eefb+k8+/Zl5d5++23LV3mw++67W15s3Ob7Ta7rr7++tnn3pz/9yfK//OUvW1w8OO1GunQ45ZRTLB1AIzofBtDiJPfWW2+1dNFpNud5vuePU558VhGkexxyyCGWpg3G73//+9WcSmX8+PE1XnLXXnttNXfNQW1ljXZ0FCiDlEzpiHPh8srrwuviypXByxHPPN55gLfKpTLllN8e53kApckHI0aMaGhgNGAdEvHwNDjtHKtxc+glpbnrrrssD1x66aWWRkNXZ9KpNI84k1qeOtJpp51Wzalj+PDhlofThp6gdJXXqTnVVzbxceB54nRgyCNvwBo7dqzleRuTzjFeD3+QiCO6QLJ5okA6dpHer7zyiuVhX5xoBeTIefh071Lk0eA8iCN3jXf0tPIeUhwa75SWl+fRSjpOcsRPNK1A5cWryHkZeS6PJi0PlO7zweWXX15rhLjevXtXrrvuuspjjz1Weeihh+yYq8/HqQMBTqYde+yxK9Bw3FSyQZ8+fVag0W4197inn376CvkDBgywe17OlB9xxBENZ+k5Ty5w4u1zn/tcQ1ncOeecU4m3Ckbj7eFtoMHq1FNPbSjLrAodp/kYtHr06FHL4+w++xdAduRk3a677mr58ZaitqsP/M4+x4gBcp955hlbtShPjtWLbAed1xenOohG8OnepcijweVhjXZ0GbcIeZXAFRkLJygsOo+0TMpPNK0i5dFWJz1SPj4dJ1neiQ6MGTOmss8++6zQ4OR4FMTm0XHHHWfHOwWe9ebRy22++eZVykpl1KhRDUtezewXXHBBLa0t7rvf/a7xZdDJy5fTgEB9PVR//wyfJXu8387lg+PW4pFHHjFaoEHiBz/4QS79gw8+aPlgr732spld4DYpr4zc3nvvbXQ8fkuvKXF//QRP410K0jyPPDrlr/U/4CA/Ni4LA8XTdKB4Wq20vIfn0x7ePp6WaYZUXuoLkpGXJyiNVySnTJlij3x4DMWxTB7z+M0pEBtFbQOTHxmMDaK2aSSQpk006LWRBz18vS580RZQRptr8FM50ZIOyPM8eJUTqKxAmFdWgewASKec50lYdeCjmxyXZZOMjTR+xJBNNR1ThZfnR1k2GZEvntiQx2SiIx1IZ8AjNtlRNgWEU71VxsPzagtS3ctQ6+jtFbY6gMFlPKALjM4+vb3w/FLfQ7LSdG838QKeztN4QANf1bGobinfvDzS8YtkAc/HN0rPLw/wboV/WyG+zeQL6A+g93UB8AGkNeMpPoL45NVN9RYUT9OLAF0eLWl5KNI71RlID1/G0ym9NeuuYbRizI6AjAZ0YeQ8fHqaByiblhffIng+RbTiWwQuap4+ghqA+PBxBl+GfOWlclSPFHHZm9sA88DAIlBG8vIattdFYXx0qDVepzv5WTj/uogXEB/vlE+eaBROeSmepou/eMkBL0OONLlVjU7R+LUrmmfw1QlVXuEiiA6/I3TWhffygZeD7/M8yM+Dp5cMwZfxdSji1QyU42WUHj16WYeiA+KzDGXJvs02W4eNNtoQyqhLZrfhw4fb22M8i0/1S+FtwNdrn3zySfuJ4f/6r/+yfD5jjTzyuXXgGTLPzlOe11xzjb0Nt91225lM9Bg9+oVwxx132o9G7rXXR8MJJ5xQ1adaqPYrjtimc1zizw2jRj0bl8XrWz333HMPy12+nLf36PydTQeW7MjnazmcqqMOqkcZyJc9cGkZH/d0gvLzygmEi657UZk8SEZRGaWvka/AFiHVAYXz3KrUtcgeZTJT3eR8ukdK59FqesqbdO6l+WzS3XffY5+owr3zztuxUzwTzjrr7HDrrb+G0hoYnfLZZ58LkydPtfLiQ8NNeePDX3E618KFS6zDCRtssGF4+OFHw7XXXmeDCy4FvOmgkya9ZXH04D764ot/Fi688MLwz//8z+GttyZbHvL4VVg6L8j0of7cw3cOPbr3CsMvv9LeLHv55VeMxuv4hz/8MfziF5eFm2++Ld6f118d9TTeJx0H0FO+7AHwfRq+yhD3dGkcSIbKeBTleV6extPKF9J80OW7EWnimoIqA/B95YrQEXojR/D8itJTtKc8jZy0Mr7KL6MB5COLk11jxrxqX5094YTPhCFDBofBgwfbmeyBAzcP119/Q9h9993sm2/IP/LII8KWW25hnZ6fVmYm1Cwj/VQHxQH8F8xfEGfi58MRRwy1Bt+vX98wZ87cMH36NHv1VHqLh8JDhx5u59xhG6NxJfBsmDljZjjwoP3DoEFbVmdeOhC68FHHbGMs48FJvxC6rdMtbLHFpuGpp/4a+m/SLw5Wk2sn2DL9O4Xf/va3YZP+A+xnro466kgbnKSD6iIH/LVSvqejjsrz6egKYjDSZPXMKweIC7JJM3i98uhb4QNNZzHyDPNAvlwRPE0ZXRGKypBOZfJcq/B6pc7zayU9hadLkZYXyvgVQfRennjjV6ms41qo+qOLYOutB8XGvsyWtMILL7wYpk2ZWd1l7hzefXd2eO65543XCy+8ZMtz2NJwAR9X5COLdKzu3XtG/pZck7Gs+pPNQGVSvPzyGFtFwHfq1GnGa1mctceOfT3MnTvfaNBl/Pg3woMPjrCXbOAvflbNquDOXTqF/fbb336hdn4ceCgXKez99h49eoYBAzYNixfVjwPLXuPHT4i8HwqPPfZ4mDnznVq6HLckPFWYN2+urQzeemuKlcfNnPl2LPeH8Mgjj5n+2UCUydVuPR/G5Mgxtw4ql15r0lqB10s8vFM6kJ+C9NakrSagdAop7yvsXauAT5Hz/FpJT+HpUqRlCDfjVwboafheplyWXz/LTgOkQS1dushm85132tnOjc+cOcPeXONrphPeGG+0d955t324Af/SS38RZ8Tbo//zeD8+3s7Oc07+nnvuscdN9913n82Y/Awz8Mv0qhoNQDeW+f/xH9+3e/qxY8daOrz50ir8R44cGRYtWmjpw4dfFW688UZbodApzz57WOzIC403jtUHgH7vvT8eyy+NK4Pss9mAt9sOOGC/sHgJX7LN0mQjlvPYgo9gMMt/61vfsrLkMQjyc1p8xZb6XXTRT+xLuldccYXxoHNfeOF3ov2zT1tTn/vv/73JoDyDxtfP/IZ1cL4se+eddxp/6kd+eyC9cb7NpA7IT2GDSsw0xMZTDa09QCe5lUWc5WrO8015ezqPovQy5MmgPPG28lOZlF+KS372i8o5w86rXHLJzyvx3rfywx/+uPKlL32p8v/O/n+VOGNWqTKcccaZlZEjR1VjlcrIv46qfPGL/1qZPHmKxfUdOL7q6t9wAzfddHPla1/TRxKz77zdffc9lXPPPc/CUeOo55LoGus4bNiw2kckAF9U/fa3L6zGKpXHH3+i8q//errpumRJVi52qMo111xnYY9zzsk+annZZcMrF1zwbQuDYcPOMf/yy6+oXHTRTy0Mpk2bXjniiE9VZs/Ovi8Hbr3119FGP7Kw7MopPn8oJg4IlalTp1ZOOOGkShx4qqm8sz6xctJJJ1fijG/xYcO+WfnV9TdYWOCkHh+nMJRct1bg20zqlF+E2owew9XQ2gM/mrUHRXXyfNvLuxUUyVhZW+fxE09mG3bRv/71M8JZZ50ZvvnNc+3XYPbeZ5+Y9vU4Q2YzuBD7YzUUwrz5c+M97cZxJh1gy//sMEv2bvZBBx1kNKSDQYO2spkUZEtX9LCYhTOf9PrjMMDSPrb3aoyNvWUN8b/97Rk73MOvysbOFnW/PPTt2yesu+46VQpoM/rYMa0sP5Tx6qtjwqKFS8KrY1612TTLZ/VkQbMPdRs+/HLbrZ86dXp4/vnRtsyv65/x5Tv8/t15VixPPz3S9ja22mpQrAP2xgZbWBr2mT37vbg6mRQO2P8AK7OsejvDZuEee+wRYhc3typR1pZrV6CM6P0IGoFAmMYmlyJrMJkroitKbyvU8Vvh5/VSGaV730P0gIZLOZbbRx/9aTulddddd1teDe6yd+3KibSsI+uUK6fVaNg9e65XTTcv3o9jqyyijhI1NAeywSerK6fNRo7MfkW2Uyfd02ZI2/6CBfNtA/G0004NX/3q6eFrX/tKdF8NJ510ovHM6qfn8dkvxGy77TZhi80H2a7/H554ovbBScwVp1ELS++HH34k3rZ8235Mg7rxIxOiUUcHetVXsrhn12uq1FvpvXr1tCU/pwIZNNfvlZ0irET70dnhz0Cx3DY86/VuD3ybSR3A1kWoSS4j6mh4g64qUHnVSYYow+qqP3LaIiulJZ53YWVTX2/fiQGzZ7du2S+Y1OCuRVZO8WzHmNmvW7d1bNPMUmNHBQwePOYCtQEnFpdsPA0EV111TZzN/OBjQQP0vuOzSz5uXPbz0cAPWujGACQZ2Zdns/zddt/dvtr6xhsTw5Ah21kaYLDJ0Cne998SXnnllfCf//m9cOqpXwj77LO3vQsvHtpr8O1Fum222Wa1r98C0c6YMT3e7/c3vbt1WzeMrereKS6WxYdjyBxOWh3tvgj1Gq1G1BpiO4HB5FIojQbSFsO2hbYV1Bp/Cdqqn6f39cOXTZmlFi1abOHFi3nenf3u2/DhV8RZ5704sx9jeYAlKp1foOzixZrR6wMGj9DuvP3uMHXK9JgWwoQJr4cHfv9AbMjZl19rDTrOeovjTMjyfGmc8ekYv/jFL61zfXTP7EuyPBtHrsDMOX/+vGos2MrjjTcmhEceyX4iCt4jRjwUrrvuOtOJJiu9eErAbjs4fOihNhjtumv2CWcwd+4823ATZs+eZWW5NNx2PPDACPvIhbcBy/0FCxbWdIx3vuZzyIcZfcSIh80G4Pbbb7ezC3vt9fHYybuETx56cLjuV1eH9+bOD13iIIf9f3rJxQ0fpPDXUNfUpwG1nbI25MvimrV3OxnnRzAK+HiKsvxW8wgD4q3KU4V0kVUpxQXSxZe8NB+Ij2hAGhefFF4H6EFKB43XA6gM8HkqK74+Tfp4mYp7KH711VeHWbPn2WzDMjxOK6ESOx3LW3aw6bDM0HTA2277dRwEFoXOnbrG+/e9rMM9+eRTcfm5PGy40Ybhs5890V7mAMi86457w2OPPxrz1g877vihsPnArcIdd94RZ8S+Ydiwc2KjvzNMnDgpKh3Cut27haWxQ7OkRTe+Kzf08MPClVddbct4Zjc+IU0ej6JYBq+3Xq8o82RbCnOg5qabbrbyrBx22mmn8KlPHVGzAx36jjvuisvp+XFW72Zfh+Hx4cWX/Czy+GzYuF/fcP2vboyz7Uyj792nd5zBP28d+Iorroz351Ptu/Y8s+eMAffRyOHb8U8//VcbELHhnnFwYtaX3Fmz5ti+AZeHwYE9jFNOOcV0F8199z8QnvjjH0Ovnr3Cer3WC0OPPCwM+eDg2nVNr7lAWaBrjhPPPIguBWmpjE6dOoX/DwnAjhdHd0j9AAAAAElFTkSuQmCC');
          width: 200px;
          height: 70px;
          background-size: contain;
          background-repeat: no-repeat;
        }

        #company {
          float: left;
          text-align: left;
        }


        #details {
          padding: 10px;
          margin-bottom: 30px;
          border: 1px #dce2ef dashed;
          background-color: #f9fbfd;
        }

        #client {
          /*padding-right: 6px;
          border-left: 6px solid #0087C3;*/
          float: right;
          text-align: right;
        }

        #client .to {
          color: #777777;
        }

        h2.name {
          font-size: 1.4em;
          font-weight: normal;
          margin: 0;
        }

        #invoice {
          float: right;
          text-align: right;
        }

        #invoice h1 {
          color: #0087C3;
          font-size: 2.4em;
          line-height: 1em;
          font-weight: normal;
          margin: 0  0 10px 0;
        }

        #invoice .date {
          font-size: 1.1em;
          color: #777777;
        }

        table {
          width: 100%;
          border-collapse: collapse;
          border-spacing: 0;
          margin-bottom: 20px;
        }

        table th,
        table td {
          padding: 12px;
          background: #EEEEEE;
          text-align: center;
          border-bottom: 1px solid #FFFFFF;
        }

        table th {
          white-space: nowrap;        
          font-weight: normal;
          background-color: #4c7ff0!important;
          color:#fff!important;
        }

        table td {
          text-align: right;
        }

        table td h3{
          color: #57B223;
          font-size: 1.2em;
          font-weight: normal;
          margin: 0 0 0.2em 0;
        }

        table .no {
          font-size: 1.6em;
          /*color: #FFFFFF;*/
          /*background: #57B223;*/
        }

        table .desc {
          text-align: left;
        }

        table .unit {
          background: #DDDDDD;
        }

        table .qty {
        }

        table .total {
          background: #DDDDDD;
          /*background: #57B223;
          color: #FFFFFF;*/
        }

        table td.unit,
        table td.qty,
        table td.total {
          font-size: 1.2em;
        }

        table tbody tr:last-child td {
          border: none;
        }

        table tfoot td {
          padding: 10px 20px;
          background: #FFFFFF;
          border-bottom: none;
          font-size: 1.2em;
          white-space: nowrap; 
          border-top: 1px solid #AAAAAA; 
        }

        table tfoot tr:first-child td {
          border-top: none; 
        }

        table tfoot tr:last-child td {
          font-size: 1.4em;
          /*color: #57B223;
          border-top: 1px solid #57B223; */
        }

        table tfoot tr td:first-child {
          border: none;
        }

        #thanks{
          font-size: 2em;
          margin-bottom: 50px;
        }

        #notices{
          padding-left: 6px;
          border-left: 6px solid #0087C3;  
        }

        #notices .notice {
          font-size: 1.2em;
        }

        footer {
          color: #777777;
          width: 100%;
          height: 30px;
          position: absolute;
          bottom: 0;
          border-top: 1px solid #AAAAAA;
          padding: 8px 0;
          text-align: center;
        }

        </style>
  </head>
  <body>
<?php 
//print_r($commande);
$sous_total=0;
$tva=0;
$grd_total=0;
?>
    <header class="clearfix">
        <div id="logo">
            <div class="img"></div>
        </div>
        <div id="invoice">
          <h3>Commande N° <?php echo $commande->num_commande;?></h3>
          <div class="date">Date : <?php echo $commande->date_commande;?></div>
        </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="company">
          <h2 class="name">GREPOM</h2>
          <div>Tél: +212 5 37 774549 <br>Fax: +212 5 37 774540</div>
          <div>contact@grepom.org</div>
          <div>www.grepom.org</div>
        </div>
        <div id="client">
          <!--div class="to">:</div-->
          <h2 class="name"><?php echo $commande->fournisseur;?></h2>
          <div class="address"><?php echo $commande->fournisseur_adr;?></div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="desc"><b>Désignation</b></th>
            <th width="15%" class="unit"><b>P.U.</b></th>
            <th width="8%" class="qty"><b>Qte</b></th>
            <th width="20%" class="total"><b>Total HT</b></th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($commande->lignescommande as $ligne) {
            $sous_total += $ligne->pu * $ligne->qte;
            echo "
                  <tr>
                    <td class='desc'>$ligne->designation</td>
                    <td class='unit'>".number_format($ligne->pu, 2, ',', ' ')."</td>
                    <td class='qty'>$ligne->qte</td>
                    <td class='total'>".number_format($ligne->pu * $ligne->qte, 2, ',', ' ')."</td>
                  </tr>";
          }
          ?>
          <!--tr>
            <td class="desc">Developing a Content Management System-based Website</td>
            <td class="unit">$40.00</td>
            <td class="qty">80</td>
            <td class="total">$3,200.00</td>
          </tr-->
        </tbody>
        <tfoot>
          <tr>
            <td></td>
            <td colspan="2">Sous-total</td>
            <td><?php echo number_format($sous_total, 2, ',', ' ');?></td>
          </tr>
          <tr>
            <td></td>
            <td colspan="2">TVA <?php echo $commande->tva;?>%</td>
            <td><?php echo number_format($sous_total * $commande->tva / 100, 2, ',', ' ');?></td>
          </tr>
          <tr>
            <td></td>
            <td colspan="2">Grand total</td>
            <td><?php echo number_format($sous_total * (1+ ($commande->tva / 100)), 2, ',', ' ');?></td>
          </tr>
        </tfoot>
      </table>
      <div id="notices">
        <div class="notice">Arrêté la présente commande à la somme de <?php 
        $f = new NumberFormatter("fr_MA", NumberFormatter::SPELLOUT);
        echo $f->format(number_format($sous_total * (1+ ($commande->tva / 100)), 2, '.', ''));
        ?> dirhams.</div>
      </div>
      <!--div id="thanks">Thank you!</div>
      <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
      </div-->
    </main>
    <footer>
      Tél: +212 5 37 774549 Fax: +212 5 37 774540 contact@grepom.org www.grepom.org
    </footer>
  </body>
</html>