<?php require_once(dirname(__FILE__) . '/../before-content.php'); ?>
    <div class="row">
        <div class="col-lg-6">
        <div class="card">
                  <div class="card-header no-bg b-a-0">
                    Gestion des personnes : Nouvelle personne
                  </div>
                  <div class="card-block">

                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">CIN :</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="text" name="cin" id="cin" placeholder="AA999999">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">Nom :</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="text" name="nom">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">Prénom :</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="text" name="prenom">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">Adresse :</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="text" name="adresse">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">Email :</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="email" name="email">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">Tél :</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="tel" name="tel">
                      </div>
                    </div>
                  </div>
                </div>
            
        </div>
    </div>        
<?php require_once(dirname(__FILE__) . '/../after-content.php'); ?>
