
            <div class="card">
              <div class="card-header no-bg b-a-0">
                <h4>Gestion des personnes <a href="/admin/personne/add" class="btn btn-round btn-primary btn-sm m-r-xs" style="float:right;">Ajouter</a></h4>
                
              </div>
              <div class="card-block"> 
              <?php
              //flash messages
              if($this->session->flashdata('flash_message')){
                if($this->session->flashdata('flash_message') == 'updated')
                {
                  echo '<div class="alert alert-success">';
                    echo '<a class="close" data-dismiss="alert">×</a>';
                    echo 'La personne a été mis à jour avec succès.';
                  echo '</div>';       
                }else{
                  echo '<div class="alert alert-error">';
                    echo '<a class="close" data-dismiss="alert">×</a>';
                    echo 'Erreur de mise à jour.';
                  echo '</div>';          
                }
              }
              ?>
                <table class="table table-bordered personnesdatatable">
                  <thead>
                    <tr>
                        <th>CIN</th>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Adresse</th>
                        <th class="no-sort"></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
