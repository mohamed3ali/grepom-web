    <div class="row">
      <div class="col-lg-6">
        <div class="card">
                  <div class="card-header no-bg b-a-0">
                    <h4>Gestion des personnes : Modifier une personne</h4>
                  </div>
                  <div class="card-block">
          <?php
          //flash messages
          if($this->session->flashdata('flash_message')){
            if($this->session->flashdata('flash_message') == 'updated')
            {
              echo '<div class="alert alert-success">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo 'La personne a été modifiée avec succès.';
              echo '</div>';       
            }else{
              echo '<div class="alert alert-error">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo 'Erreur de mise à jour.';
              echo '</div>';          
            }
          }
          ?>
                  <?php
                  //form data
                  $attributes = array('class' => '', 'id' => '');
            
                  echo validation_errors();
            
                  echo form_open('/personnes/update/'.$this->uri->segment(3), $attributes);
                  ?>
      
                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">CIN</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="text" name="cin" id="cin" value="<?php echo $formData->cin; ?>">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">Nom</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="text" name="nom" value="<?php echo $formData->nom; ?>">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">Prénom</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="text" name="prenom" value="<?php echo $formData->prenom; ?>">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">adresse</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="text" name="adresse" value="<?php echo $formData->adresse; ?>">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">Email</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="email" name="email" value="<?php echo $formData->email; ?>">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-xs-2 col-form-label">Tél</label>
                      <div class="col-xs-10">
                        <input class="form-control" type="tel" name="tel" value="<?php echo $formData->tel; ?>">
                      </div>
                    </div>
                    <button class="btn btn-primary" type="submit" style="float: right;">Enregistrer</button>
                    <a class="btn btn-info" style="float: left;" href="javascript:history.back()">Annuler</a>
                  </div>
                </div>
        
        <?php echo form_close(); ?>
        </div>
    </div>  
