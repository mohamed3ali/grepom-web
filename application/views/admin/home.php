<!-- admin home -->
<?php
    echo $hello;
    
    //var_dump($cotisationStats);
?>
<div class="row">
	<div class="col-md-4">
		<div class="card">
	        <div class="card-header no-bg b-a-0">
	          <h4>Cotisations annuelles</h4>
	        </div>
	        <div class="card-block">
	          <div class="canvas-holder">
	            <canvas class="cotisations-bar" id="cotisations-bar" height="200"></canvas>
	          </div>
	        </div>
	    </div>
	</div>
	<div class="col-md-4">
		<div class="card">
	        <div class="card-header no-bg b-a-0">
	          <h4>Adhérents actifs</h4>
	        </div>
	        <div class="card-block">
	          <div class="canvas-holder">
	            <canvas class="adh-count-bar" id="adh-count-bar" height="200"></canvas>
	          </div>
	        </div>
	    </div>
	</div>
	<div class="col-md-4">
		
		<div class="card">
			<div class="card-header no-bg b-a-0">
			  <h4>Professions des adhérents</h4>
			</div>
			<div class="card-block">
			  <div class="canvas-holder">
			    <canvas class="adh-pro-chart" id="adh-pro-chart" height="200"></canvas>
			  </div>
			</div>
		</div>
	</div>
</div>