<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nouvelle nomenclature</h4>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                <div class="row">
                  <div class="col-xs-2" style="padding-right: 0;">
                    <fieldset class="form-group">
                      <label for="">Montant</label>
                      <input type="text" class="form-control" id="montant" name="montant">
                    </fieldset>
                  </div>
                  <div class="col-xs-1" style="padding-left: 0;">
                    <fieldset class="form-group">
                      <label for="">Devise</label>
                      <?php echo form_dropdown('devise_id', $comboDevises, '', 'class="form-control" id="devise_id"'); ?>
                    </fieldset>
                  </div>
                  <div class="col-xs-2">
                    <fieldset class="form-group">
                      <label for="">Mode</label>
                      <?php echo form_dropdown('modepaiement_id', $comboModesPaiement, '', 'class="form-control" id="modepaiement_id"'); ?>
                    </fieldset>
                  </div>
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Rubrique</label>
                      <?php echo form_dropdown('depensesnomenclature_id', $comboRubriques, '', 'class="form-control" id="depensesnomenclature_id"'); ?>
                    </fieldset>
                  </div>
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">Nature</label>
                      <input type="text" class="form-control" id="nature" name="nature">
                    </fieldset>
                  </div>

                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">Source</label>
                      <?php echo form_dropdown('source', $comboProjets, '', 'class="form-control" id="source"'); ?>
                    </fieldset>
                  </div>
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">Bénéficiaire</label>
                      <input type="text" class="form-control" id="beneficiaire" name="beneficiaire">
                    </fieldset>
                  </div>
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">Date</label>
                      <input type="date" class="form-control" id="date" name="date">
                    </fieldset>
                  </div>

                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">Commande</label>
                      <?php echo form_dropdown('commande_id', $comboCommandes, '', 'class="form-control" id="commande_id"'); ?>
                    </fieldset>
                  </div>
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">Justificatif</label>
                      <?php echo form_dropdown('justificatif_id', $comboJustificatifs, '', 'class="form-control" id="justificatif_id"'); ?>
                    </fieldset>
                  </div>
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">N° du justificatif</label>
                      <input type="text" class="form-control" id="num_facture" name="num_facture">
                    </fieldset>
                  </div>

                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->