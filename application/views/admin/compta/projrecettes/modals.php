<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h6 class="modal-title">Nouvelle recette</h6>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                <div class="row">
                  <div class="col-xs-4">
                      <fieldset class="form-group">
                        <label for="">Projet</label>
                        <?php echo form_dropdown('type_id', $comboProjets, '', 'class="form-control" id="type_id"'); ?>
                      </fieldset>
                  </div>
                  <div class="col-xs-3" style="padding-right: 0;">
                      <fieldset class="form-group">
                        <label for="">Montant</label>
                        <input type="text" class="form-control" id="montant" name="montant">
                      </fieldset>
                  </div>
                  <div class="col-xs-1" style="padding-left: 0;">
                      <fieldset class="form-group">
                        <label for="">Devise</label>
                        <?php echo form_dropdown('devise_id', $comboDevises, '', 'class="form-control" id="devise_id"'); ?>
                      </fieldset>
                  </div>
                  <div class="col-xs-4">
                      <fieldset class="form-group">
                        <label for="">Mode d'acquisition</label>
                        <?php echo form_dropdown('modepaiement_id', $comboModesPaiement, '', 'class="form-control" id="modepaiement_id"'); ?>
                      </fieldset>
                  </div>

                  <fieldset class="form-group col-xs-4">
                    <label for="">Date de virement</label>
                    <input type="date" class="form-control datepicker" id="date_virement" name="date_virement">
                  </fieldset>
                  <fieldset class="form-group col-xs-4">
                    <label for="">Date de reception</label>
                    <input type="date" class="form-control datepicker" id="date_reception" name="date_reception">
                  </fieldset>
                  <div class="col-xs-4">
                      <fieldset class="form-group">
                        <label for="">Source</label>
                        <?php echo form_dropdown('source', $comboBailleurFonds, '', 'class="form-control" id="source"'); ?>
                      </fieldset>
                  </div>
      
      
                </div>
              </form>
            </div>
            <div class="modal-footer" style="padding: 5px 15px;">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->