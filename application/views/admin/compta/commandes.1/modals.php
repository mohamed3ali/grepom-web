<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nouvelle commande</h4>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                <div class="row">
                  <div class="col-xs-6">
                    <fieldset class="form-group">
                      <label for="">N° commande</label>
                      <input type="text" class="form-control" id="numCommande" name="numCommande">
                    </fieldset>
                  </div>
                  <div class="col-xs-6">
                    <fieldset class="form-group">
                      <label for="">Date d'arrivée</label>
                      <input type="date" class="form-control" id="date_arrivee" name="date_arrivee">
                    </fieldset>
                  </div>
                  <div class="col-xs-6">
                    <fieldset class="form-group">
                      <label for="">Fournisseur</label>
                      <input type="text" class="form-control" id="fournisseur" name="fournisseur">
                    </fieldset>
                  </div>
                  <div class="col-xs-6">
                    <fieldset class="form-group">
                      <label for="">Client</label>
                      <input type="text" class="form-control" id="client" name="client">
                    </fieldset>
                  </div>

                  <div class="col-xs-12">
                    <fieldset class="form-group">
                      <label for="">Description</label>
                      <textarea rows="4" class="form-control" id="description" name="description"></textarea>
                    </fieldset>
                  </div>

                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->