<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nouveau membre</h4>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                <div class="row">
                  <div class="col-xs-12">
                    <fieldset class="form-group">
                      <label for="">Titre</label>
                      <input type="text" class="form-control" id="titre" name="titre">
                    </fieldset>
                  </div>
                  <div class="col-xs-2">
                    <fieldset class="form-group">
                      <label for="">code</label>
                      <input type="text" class="form-control" id="code" name="code">
                    </fieldset>
                  </div>
                  <div class="col-xs-4">
                      <fieldset class="form-group">
                        <label for="">Bailleur de fonds</label>
                        <?php echo form_dropdown('bf_id', $comboFondbailleurs, '', 'class="form-control" id="bf_id"'); ?>
                      </fieldset>
                  </div>
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Proposé le</label>
                      <input type="date" class="form-control" id="date_proposition" name="date_proposition">
                    </fieldset>
                  </div>
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Accepté le</label>
                      <input type="date" class="form-control" id="date_acceptation" name="date_acceptation">
                    </fieldset>
                  </div>

                  <div class="col-xs-12">
                    <fieldset class="form-group">
                      <label for="">Description</label>
                      <textarea rows="4" class="form-control" id="description" name="description"></textarea>
                    </fieldset>
                  </div>

                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->