<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nouvelle commandebon</h4>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                <div class="row">
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Désignation</label>
                      <input type="text" class="form-control" id="designation" name="designation">
                    </fieldset>
                  </div>
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Commande</label>
                      <?php echo form_dropdown('commande_id', $comboCommandes, '', 'class="form-control" id="commande_id"'); ?>
                    </fieldset>
                  </div>
                  <div class="col-xs-2">
                    <fieldset class="form-group">
                      <label for="">Prix unitaire</label>
                      <input type="text" class="form-control" id="prixU" name="prixU">
                    </fieldset>
                  </div>
                  <div class="col-xs-2">
                    <fieldset class="form-group">
                      <label for="">Quantité</label>
                      <input type="text" class="form-control" id="quantite" name="quantite">
                    </fieldset>
                  </div>
                  <div class="col-xs-2">
                    <fieldset class="form-group">
                      <label for="">Prix total</label>
                      <input type="text" class="form-control" id="prixT" name="prixT">
                    </fieldset>
                  </div>

                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->