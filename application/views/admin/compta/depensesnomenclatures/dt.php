<script type="text/javascript">
var save_method; //for save method string
var dtable;
var entity_id;
 
$(document).ready(function() {
    dtable = $('#dtable').DataTable({
        "language": {
              url: "<?php echo base_url(); ?>assets/vendor/datatables/media/js/french.json"
        },
        dom:'Blfrtip',
        buttons: [
            {
                text: '<h4><?php if (isset($titre)) {echo $titre;} ?> :</h4>',
                className:'titre-tag'
            },
            {
                text: '<i class="glyphicon glyphicon-plus"></i> Ajouter',
                className:'btn btn-success dataTables_btn',
                action: function ( e, dt, node, config ) {
                    add_entity();
                }
            }
        ],
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Tout']
        ],
        'ajax': "<?php echo base_url(uri_string()); ?>/json",
      });

});

function reload_table()
{
    dtable.ajax.reload(null,false); //reload datatable ajax 
}

function add_entity()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('.demission-group').hide(); // clear error string
    $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#numrecu').focus();
                    //});  
    $('.modal-title').text('Nouvelle nomenclature'); // Set Title to Bootstrap modal title
}

function save()
{
    $('#btnSave').text('Enregistrement en cours ...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo base_url(uri_string()); ?>/ajax_add";
    } else {
        url = "<?php echo base_url(uri_string()); ?>/ajax_update/"+entity_id;
    }
 
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
 
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Erreur d\'enregistrement des depensesnomenclaturenées');
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}

function delete_entity(id)
{
    if(confirm('Voulez vous vraiment supprimer ?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo base_url(uri_string()); ?>/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression!');
            }
        });
 
    }
} 

function edit_entity(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.demission-group').show(); // clear error string
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url(uri_string()); ?>/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            entity_id = data.id;
            $('#code').val(data.code);
            $('#designation').val(data.designation);
            $('#signification').val(data.signification);

            $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#cin').focus();
                    //}); // show bootstrap modal when complete loaded
            $('.modal-title').text("Modification de la nomencature"); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from application');
        }
    });
}
 
</script>