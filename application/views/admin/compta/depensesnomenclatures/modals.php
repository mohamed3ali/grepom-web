<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nouvelle nomenclature</h4>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                <div class="row">
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">Code</label>
                      <input type="text" class="form-control" id="code" name="code">
                    </fieldset>
                  </div>
                  <div class="col-xs-8">
                    <fieldset class="form-group">
                      <label for="">Désignation</label>
                      <input type="text" class="form-control" id="designation" name="designation">
                    </fieldset>
                  </div>
                  <div class="col-xs-12">
                    <fieldset class="form-group">
                      <label for="">Signification</label>
                      <input type="text" class="form-control" id="signification" name="signification">
                    </fieldset>
                  </div>

                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->