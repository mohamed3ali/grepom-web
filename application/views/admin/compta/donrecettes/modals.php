<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-sm" style="width:400px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h6 class="modal-title">Nouveau don</h6>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                <div class="row">
                  <div class="col-xs-12">
                      <fieldset class="form-group">
                        <center>
                        <label class="radio-inline">
                          <input type="radio" name="source_type" id="donateurs" value="donateurs" checked="checked">
                          Donateur
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="source_type" id="fondsbailleurs" value="fondsbailleurs">
                          Bailleur de fond
                        </label>
                        </center>
                      </fieldset>
                  </div>
                  <div class="col-xs-12 ligne-donateurs">
                      <fieldset class="form-group">
                        <label for="">Donateur</label>
                        <?php echo form_dropdown('type_id_donateurs', $comboDonateurs, '', 'class="form-control" id="type_id_donateurs"'); ?>
                      </fieldset>
                  </div>
                  <div class="col-xs-12 ligne-bf" style="display:none;">
                      <fieldset class="form-group">
                        <label for="">Bailleur de fonds</label>
                        <?php echo form_dropdown('type_id_fondsbailleurs', $comboFondbailleurs, '', 'class="form-control" id="type_id_fondsbailleurs"'); ?>
                      </fieldset>
                  </div>
                  <div class="col-xs-4" style="padding-right: 0;">
                      <fieldset class="form-group">
                        <label for="">Montant</label>
                        <input type="text" class="form-control" id="montant" name="montant">
                      </fieldset>
                  </div>
                  <div class="col-xs-2" style="padding-left: 0;">
                      <fieldset class="form-group">
                        <label for="">Devise</label>
                        <?php echo form_dropdown('devise_id', $comboDevises, '', 'class="form-control" id="devise_id"'); ?>
                      </fieldset>
                  </div>
                  <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Mode d'acquisition</label>
                        <?php echo form_dropdown('modepaiement_id', $comboModesPaiement, '', 'class="form-control" id="modepaiement_id"'); ?>
                      </fieldset>
                  </div>

                  <fieldset class="form-group col-xs-6">
                    <label for="">Date de virement</label>
                    <input type="date" class="form-control datepicker" id="date_virement" name="date_virement">
                  </fieldset>
                  <fieldset class="form-group col-xs-6">
                    <label for="">Date de reception</label>
                    <input type="date" class="form-control datepicker" id="date_reception" name="date_reception">
                  </fieldset>
      
      
                </div>
              </form>
            </div>
            <div class="modal-footer" style="padding: 5px 15px;">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->