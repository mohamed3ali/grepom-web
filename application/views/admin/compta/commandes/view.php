
            <div class="card">
                <div class="card-block">
                    <table id="dtable" class="dt table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="width:18%; text-align:center">Fournisseur</th>
                                <th style="width:15%; text-align:center">N° Commande</th>
                                <th style="width:20%; text-align:center">Objet</th>
                                <th style="width:8%; text-align:center">Date</th>
                                <th style="width:8%; text-align:center">Montant</th>
                                <th style="width:auto; text-align:center">N° Facture</th>
                                <th style="width:auto; text-align:center">N° BL</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        
        
<?php if (isset($zonemodals)) {$this->load->view($zonemodals);} ?>
