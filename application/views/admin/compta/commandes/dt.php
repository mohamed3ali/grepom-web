<script type="text/javascript">
var save_method; //for save method string
var dtable;
var entity_id;
var ligne_i=0;

$(document).ready(function() {
    dtable = $('#dtable').DataTable({
        "language": {
              url: "<?php echo base_url(); ?>assets/vendor/datatables/media/js/french.json"
        },
        dom:'Blfrtip',
        buttons: [
            {
                text: '<h4><?php if (isset($titre)) {echo $titre;} ?> :</h4>',
                className:'titre-tag'
            },
            {
                text: '<i class="glyphicon glyphicon-plus"></i> Ajouter',
                className:'btn btn-success dataTables_btn',
                action: function ( e, dt, node, config ) {
                    add_entity();
                }
            }
        ],
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Tout']
        ],
        'ajax': "<?php echo base_url(uri_string()); ?>/json",
      });

	var indx=1;
    $("#add_row").click(function(e){b=indx-1;
        e.preventDefault();
      	$('#addr'+indx).html($('#addr'+b).html());//.find('td:first-child').html(indx+1);
      	$('#tab_logic').append('<tr id="addr'+(indx+1)+'"></tr>');
      	$("#addr"+indx+" input[name='l_id[]']").val(0);
      	indx++; 
  	});
    $("#delete_row").click(function(e){
        e.preventDefault();
    	if(indx>1){
		$("#addr"+(indx-1)).html('');
		indx--;
		}
		calc();
	});
	
	$('#tab_logic tbody').on('keyup change',function(){
		calc();
	});
	$('#tva').on('keyup change',function(){
		calc_total();
	});
});
	
function calc()
{
	$('#tab_logic tbody tr').each(function(indx, element) {
		var html = $(this).html();
		if(html!='')
		{
			var qte = $(this).find('.qte').val();
			var prix = $(this).find('.prix').val();
			$(this).find('.total').val(qte*prix);
			
			calc_total();
		}
    });
}

function calc_total()
{
	total=0;
	$('.total').each(function() {
        total += parseInt($(this).val());
    });
	$('#sous_total').val(total.toFixed(2));
	tva_sum=total/100*$('#tva').val();
	$('#montant_tva').val(tva_sum.toFixed(2));
	$('#montant_total').val((tva_sum+total).toFixed(2));
}

function reload_table()
{
    dtable.ajax.reload(null,false); //reload datatable ajax 
}

function clean_lignes_cmd()
{
    if(ligne_i>0){
        for (i = 1; i < ligne_i+1; i++) { 
            $("#delete_row").click();
        }
    }
    ligne_i = 0;
    $("#addr"+ligne_i+" input[name='l_designation[]']").val("");
    $("#addr"+ligne_i+" input[name='l_qte[]']").val("");
    $("#addr"+ligne_i+" input[name='l_pu[]']").val("");
}

function add_entity()
{
    clean_lignes_cmd();
    
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('.demission-group').hide(); // clear error string
    $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#numrecu').focus();
                    //});  
    $('.modal-title').text('Nouvelle commande'); // Set Title to Bootstrap modal title
}

function save()
{
    var empty = true;
    $(".in_obligatoire").each(function(){
       if($(this).val()!=""){
          empty = false;
          return false;
        }else{
            console.log($(this));
            $(this).addClass("error");
        }
    });
    //return;
    if(!empty){
        $('#btnSave').text('Enregistrement en cours ...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        var url;
        if(save_method == 'add') {
            url = "<?php echo base_url(uri_string()); ?>/ajax_add";
        } else {
            url = "<?php echo base_url(uri_string()); ?>/ajax_update/"+entity_id;
        }
        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                }
                $('#btnSave').text('Enregistrer'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur d\'enregistrement');
                $('#btnSave').text('Enregistrer'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
            }
        });
    }
}

function delete_entity(id)
{
    if(confirm('Voulez vous vraiment supprimer ?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo base_url(uri_string()); ?>/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression!');
            }
        });
 
    }
} 

function edit_entity(id)
{
    clean_lignes_cmd();
    
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.demission-group').show(); // clear error string
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url(uri_string()); ?>/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            entity_id = data.id;
            $('#fournisseur_adr').val(data.fournisseur_adr);
            $('#fournisseur').val(data.fournisseur);
            $('#num_commande').val(data.num_commande);
            $('#objet').val(data.objet);
            $('#date_commande').val(data.date_commande);
            $('#id_facture').val(data.id_facture);
            $('#id_bl').val(data.id_bl);
            $('#tva').val(data.tva);

            // insersion des lignes
            data.lignescommande.forEach(function(ligneCmd){
                console.log(ligneCmd);
                $("#addr"+ligne_i+" input[name='l_id[]']").val(ligneCmd.id);
                $("#addr"+ligne_i+" input[name='l_designation[]']").val(ligneCmd.designation);
                $("#addr"+ligne_i+" input[name='l_qte[]']").val(ligneCmd.qte);
                $("#addr"+ligne_i+" input[name='l_pu[]']").val(ligneCmd.pu);
                
                ligne_i++;
                $("#add_row").click();
            });
            $("#delete_row").click();


            $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#cin').focus();
                    //}); // show bootstrap modal when complete loaded
            $('.modal-title').text("Modifier la commande"); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from application');
        }
    });
}
 
</script>