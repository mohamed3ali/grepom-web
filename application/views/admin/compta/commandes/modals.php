<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nouvelle commande</h4>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                <div class="row">
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">Fournisseur</label>
                      <input type="text" class="form-control in_obligatoire" required id="fournisseur" name="fournisseur">
                    </fieldset>
                  </div>
                  <div class="col-xs-8">
                    <fieldset class="form-group">
                      <label for="">Adresse</label>
                      <input type="text" class="form-control in_obligatoire" id="fournisseur_adr" name="fournisseur_adr">
                    </fieldset>
                  </div>
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">N° commande</label>
                      <input type="text" class="form-control in_obligatoire" id="num_commande" name="num_commande">
                    </fieldset>
                  </div>
                  <div class="col-xs-8">
                    <fieldset class="form-group">
                      <label for="">Objet</label>
                      <textarea rows="2" class="form-control in_obligatoire" id="objet" name="objet"></textarea>
                    </fieldset>
                  </div>
                  
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">Date</label>
                      <input type="date" class="form-control in_obligatoire" id="date_commande" name="date_commande">
                    </fieldset>
                  </div>
                  <!--div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Montant total</label>
                      <input type="text" class="form-control" id="montant_facture" name="montant_facture">
                    </fieldset>
                  </div-->
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">N° facture</label>
                      <input type="text" class="form-control in_obligatoire" id="id_facture" name="id_facture">
                    </fieldset>
                  </div>
                  <div class="col-xs-4">
                    <fieldset class="form-group">
                      <label for="">N° BL</label>
                      <input type="text" class="form-control in_obligatoire" id="id_bl" name="id_bl">
                    </fieldset>
                  </div>
                </div>
                <hr>
                              
                <div class="row clearfix">
                  <div class="col-md-12">
                    <table class="table table-bordered table-hover" id="tab_logic">
                      <thead>
                        <tr>
                          <!--th class="text-center"> # </th-->
                          <th class="text-center"> Produit </th>
                          <th class="text-center"> Qte </th>
                          <th class="text-center"> P.U. </th>
                          <th class="text-center"> Total </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr id='addr0'>
                          <!--td>1</td-->
                          <td width="55%">
                            <input type="hidden" name='l_id[]'  placeholder='' class="form-control" value="0"/>
                            <input type="text" name='l_designation[]'  placeholder='' class="form-control in_obligatoire"/>
                          </td>
                          <td width="15%"><input type="number" name='l_qte[]' placeholder='' class="form-control qte in_obligatoire" step="0" min="0"/></td>
                          <td width="15%"><input type="number" name='l_pu[]' placeholder='' class="form-control prix in_obligatoire" step="0.00" min="0"/></td>
                          <td width="15%"><input type="number" name='l_total[]' placeholder='0.00' class="form-control total" readonly/></td>
                        </tr>
                        <tr id='addr1'></tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row clearfix">
                  <div class="col-md-12">
                    <button id="add_row" class="btn btn-default pull-left">Nouvelle ligne</button>
                    <button id='delete_row' class="pull-right btn btn-default">Retirer la ligne</button>
                  </div>
                </div>
                <div class="row clearfix" style="margin-top:20px">
                  <div class="pull-right col-md-4">
                    <table class="table table-bordered table-hover" id="tab_logic_total">
                      <tbody>
                        <tr>
                          <th class="text-center">Sous total</th>
                          <td class="text-center"><input type="number" name='sous_total' placeholder='0.00' class="form-control" id="sous_total" readonly/></td>
                        </tr>
                        <tr>
                          <th class="text-center">TVA</th>
                          <td class="text-center"><div class="input-group mb-2 mb-sm-0">
                              <input type="number" class="form-control" id="tva" name="tva" placeholder="0">
                              <div class="input-group-addon">%</div>
                            </div></td>
                        </tr>
                        <tr>
                          <th class="text-center">Montant TVA</th>
                          <td class="text-center"><input type="number" name='montant_tva' id="montant_tva" placeholder='0.00' class="form-control" readonly/></td>
                        </tr>
                        <tr>
                          <th class="text-center">Grand Total</th>
                          <td class="text-center"><input type="number" name='montant_total' id="montant_total" placeholder='0.00' class="form-control" readonly/></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->