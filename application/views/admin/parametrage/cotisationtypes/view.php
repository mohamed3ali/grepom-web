<div class="container">

	<div style="text-align:center">				
		<h2 data-ng-show="cotisationtypes.length == 0">Aucun type n'est trouvé !</h2>
	</div>
	
	<div class="col-md-12" data-ng-show="cotisationtypes.length > 0">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Type</th>
					<th>Valeur</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr data-ng-repeat="cotisationtype in cotisationtypes track by $index">
					<td>
						<input class="todo" type="text" ng-model-options="{ updateOn: 'blur' }" ng-change="updateCotisationtype(cotisationtypes[$index])" ng-model="cotisationtypes[$index].cotisation_type">
					</td>
					<td>
						<input class="todo" type="text" ng-model-options="{ updateOn: 'blur' }" ng-change="updateCotisationtype(cotisationtypes[$index])" ng-model="cotisationtypes[$index].valeur">
					</td>
					<td style="text-align:center">
						<a class="btn btn-xs btn-default" ng-click="deleteCotisationtype(cotisationtypes[$index])"><span class="glyphicon glyphicon-trash"></span></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<form style="form-inline" role="form" ng-submit="addCotisationtype()">
		<div class="form-group col-md-6">
			<input type="text" class="form-control" name="cotisation_type" ng-model="cotisation_type" placeholder="" required>
		</div>
		<div class="form-group col-md-4">
			<input type="number" class="form-control" name="valeur" ng-model="valeur" placeholder="" required>
		</div>
		<button type="submit" class="btn btn-default">Ajouter</button>
	</form>
	
</div>