<div class="card">
	<div class="card-block">
		<div class="titre-tag" style="float: none;">
			<h4>Projets :</h4>
		</div>
		
		<div style="text-align:center">				
			<h2 data-ng-show="projets.length == 0">Aucun type n'est trouvé !</h2>
		</div>
		
		<div class="col-md-12" data-ng-show="projets.length > 0">
			<table id="projtable" class="dt table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
						<th style="width:120px; text-align:center">Code</th>
						<th>Titre</th>
						<th>Description</th>
						<th style="width:120px; text-align:center">Date d'entrée</th>
						<th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
			<table class="table table-bordered table-sm table-stripped">
				<thead>
					<tr>
						<th style="width:120px; text-align:center">Code</th>
						<th>Titre</th>
						<th>Description</th>
						<th style="width:120px; text-align:center">Date d'entrée</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr data-ng-repeat="projet in projets track by $index">
						<td>
							<input class="ng-modifiable" type="text" ng-model-options="{ updateOn: 'blur' }" ng-change="updateProjet(projets[$index])" ng-model="projets[$index].code">
						</td>
						<td>
							<input class="ng-modifiable" type="text" ng-model-options="{ updateOn: 'blur' }" ng-change="updateProjet(projets[$index])" ng-model="projets[$index].titre">
						</td>
						<td>
							<input class="ng-modifiable" type="text" ng-model-options="{ updateOn: 'blur' }" ng-change="updateProjet(projets[$index])" ng-model="projets[$index].description">
						</td>
						<td>
							<input class="ng-modifiable" type="text" ng-model-options="{ updateOn: 'blur' }" ng-change="updateProjet(projets[$index])" ng-model="projets[$index].date_entree">
						</td>
						<td style="text-align:center" ng-click="deleteProjet(projets[$index])">
							<button class="btn btn-icon-icon btn btn-danger btn-sm m-r-xs">
		                      <i class="material-icons">
		                        close
		                      </i>
		                    </button>
							<!--a class="btn btn-xs btn-default" ng-click="deleteProjet(projets[$index])"><span class="glyphicon glyphicon-trash"></span></a-->
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<form style="form-inline" role="form" ng-submit="addProjet()">
			<div class="form-group col-md-6">
				<input type="text" class="form-control" name="cotisation_type" ng-model="cotisation_type" placeholder="" required>
			</div>
			<div class="form-group col-md-4">
				<input type="number" class="form-control" name="valeur" ng-model="valeur" placeholder="" required>
			</div>
			<button type="submit" class="btn btn-default">Ajouter</button>
		</form>
		
	</div>
</div>
	