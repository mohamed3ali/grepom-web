<script type="text/javascript">
var save_method; //for save method string
var table;
var entity_id;
 
$(document).ready(function() {
    table = init_dt();
});



function init_dt()
{
    if ( $.fn.dataTable.isDataTable( '.dt' ) ) {
        
    }
    return $('.dt').DataTable({
        "language": {
              url: "<?php echo base_url(); ?>assets/vendor/datatables/media/js/french.json"
        },
        dom:'<"dataTables_btns">Blfrtip',
        buttons: [
            {
                text: '<h4 style="display:inline-block;"><?php if (isset($titre)) {echo $titre;} ?> :</h4> <div class="btn-group" style="vertical-align: sub;" data-toggle="buttons"> <label class="btn btn-default btn-sm active" onclick="salaries_tous()"> <input type="radio" name="options" id="tous" autocomplete="off" checked="checked"> Tous </label> <label class="btn btn-default btn-sm" onclick="salaries_actuels()"> <input type="radio" name="options" id="actuels" autocomplete="off"> Actuels </label> </div>',
                className:'titre-tag'
            },
            {
                text: '<i class="glyphicon glyphicon-plus"></i> Ajouter',
                className:'btn btn-success dataTables_btn',
                action: function ( e, dt, node, config ) {
                    add_entity();
                }
            }
        ],
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Tout afficher']
        ],
        'ajax': "<?php echo base_url(uri_string()); ?>/json",
      }); 
}

var allSalarie = true;
function salaries_tous()
{
    allSalarie = true;
    salaries_search(); 
}
function salaries_actuels()
{
    allSalarie = false;
    salaries_search();
}
function salaries_search()
{
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var fin = data[5];
     
            if (allSalarie)
            {
                return true;
            } else {
                if ( !isNaN(fin) )
                {
                    return true;
                }
                return false;
            }
        }
    );
    table.ajax.reload(null,false); //reload datatable ajax 
}


$('#pays_id').on('change',function(){
    var countryID = $(this).val();
    if(countryID){
        $.ajax({
            type:'POST',
            url:'/rh/personnes/ville/'+countryID,
            //data:'pays_id='+countryID,
            success:function(html){
                $('#ville_id').remove();
                $('#ville_id_div').html(html);
            }
        }); 
        $.ajax({
            type:'POST',
            url:'/rh/personnes/indicatif/'+countryID,
            //data:'pays_id='+countryID,
            success:function(html){
                $('#indicatif').val(html);
            }
        }); 
    }else{
        //$('#ville_id').html('<option value="">Select country first</option>');
    }
});

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function add_entity()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.supp_photo_icon').hide();
    $('select[name^="pays_id"] option:selected').attr("selected",null);
    $('select[name^="pays_id"] option[value="MA"]').attr("selected","selected");
    $('select[name^="pays_id"] option[value="MA"]').prop("selected","selected");
    $('select[name^="pays_id"]').val("MA").change();
    
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('.demission-group').hide(); // clear error string
    $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#numrecu').focus();
                    //});  
    $('.modal-title').text("Salariés: Ajouter"); // Set Title to Bootstrap modal title
}


$("#photo").change(function() {
    //$("#message").empty(); // To remove the previous error message
    var photo = this.files[0];
    var imagefile = photo.type;
    var match= ["image/jpeg","image/png","image/jpg"];
    if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
    {
        $('#previewing').attr('src','noimage.png');
        //$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
        alert("Erreur de type de fichier. Merci de choisir une photo !")
        return false;
    }
    else
    {
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
    }
});

function imageIsLoaded(e) {
    $("#file").css("color","green");
    $('#image_preview').css("display", "block");
    $('#previewing').attr('src', e.target.result);
    $('#previewing').attr('width', '180px');
    $('#previewing').attr('height', '180px');
};

function supp_photo() {
    if(confirm('Voulez vous vraiment supprimer la photo?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo base_url(uri_string()); ?>/ajax_supp_photo/"+entity_id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#previewing').attr('src', '/uploads/photos/no-photo.png');
                alert('La photo a été supprimée !')
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression!');
            }
        });
 
    }
};

function save()
{
    $('#btnSave').text('Enregistrement en cours ...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo base_url(uri_string()); ?>/ajax_add";
    } else {
        url = "<?php echo base_url(uri_string()); ?>/ajax_update/"+entity_id;
    }
 
    // ajax adding data to database
    profileData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: profileData,//$('#form').serialize(),
        //dataType: "JSON",
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,
        success: function(data)
        {
 
            //if(data.status) //if success close modal and reload ajax table
            //{
                entity_id = null;
                $('#previewing').attr('src', '/uploads/photos/no-photo.png');
                $('#modal_form').modal('hide');
                reload_table();
            //}
 
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Erreur d\'enregistrement des données');
            $('#btnSave').text('Enregistrer'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}

function delete_entity(id)
{
    if(confirm('Voulez vous vraiment supprimer ?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo base_url(uri_string()); ?>/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur de suppression!');
            }
        });
 
    }
} 

function edit_entity(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.demission-group').show(); // clear error string
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo base_url(uri_string()); ?>/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            entity_id = data.id;
            $('#identitetype_id').val(data.personne.identitetype_id);
            $('#cin').val(data.personne.cin);
            $('#nom').val(data.personne.nom);
            $('#prenom').val(data.personne.prenom);
            $('#email').val(data.personne.email);
            $('#indicatif').val(data.personne.indicatif);
            $('#tel').val(data.personne.tel);
            $('#pays_id').val(data.personne.pays_id);
            $('#ville_id').val(data.personne.ville_id);
            $('#adresse').val(data.personne.adresse);
            
            $('#salariefonction_id').val(data.salariefonction_id);
            $('#salariestatut_id').val(data.salariestatut_id);
            $('#date_recrutement').val(data.date_recrutement);
            $('#date_demission').val(data.date_demission);

            var my_photo = data.personne.photo ? data.personne.photo : "no-photo.png";
            $('#previewing').attr('src', '/uploads/photos/'+my_photo);
            $('.supp_photo_icon').show();
            
            $('#modal_form').modal('show');
                    //.on('shown.bs.modal', function () {
                    //    $('#cin').focus();
                    //}); // show bootstrap modal when complete loaded
            $('.modal-title').text("Modifier le salarié"); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from application');
        }
    });
}
 
</script>