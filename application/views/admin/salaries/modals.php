<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h6 class="modal-title"><?php if (isset($titre)) {echo $titre;} ?> : Ajouter</h6>
            </div>
            <div class="modal-body form">
              <form id="form" action="#">
                
              <div class="row">
                <div class="col-xs-3">
                  <div>Photo <span class="pull-right supp_photo_icon"><a onclick="supp_photo()"><i class="material-icons text-danger">delete</i></a></span></div>
                  <div id="image_preview"><img id="previewing" src="/uploads/photos/no-photo.png"  width="180px" height="180px"/></div>
                  <?php //echo form_upload('uploadedphotos[]','','multiple'); ?>
                  <?php echo form_upload('photo','', 'id="photo"'); ?>

                </div>
                <div class="col-xs-9">
                  <div class="row">
                  
                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Pièce d'identité</label>
                        <?php echo form_dropdown('identitetype_id', $comboTypesIdentite, '', 'class="form-control" id="identitetype_id"'); ?>
                      </fieldset>
                    </div>
                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">N° de la pièce d'identité</label>
                            <input type="text" class="form-control" id="cin" name="cin">
                      </fieldset>
                    </div>

                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Nom</label>
                            <input type="text" class="form-control" id="nom" name="nom">
                      </fieldset>
                    </div>
                    
                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Prénom</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="prenom" name="prenom">
                        </div>
                      </fieldset>
                    </div>

                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Email</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="email" name="email">
                        </div>
                      </fieldset>
                    </div>

                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Tél</label>
                        <div class="input-group">
                          <div class="row">
                            <div class="col-xs-3 indicatif" id="">
                              <input type="text" class="form-control" name="indicatif" id="indicatif">
                            </div>
                            <div class="col-xs-9 tel" id="">
                              <input type="text" class="form-control" name="tel" id="tel">
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </div>
              
                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Pays</label>
                        <?php echo form_dropdown('pays_id', $comboPays, '', 'class="form-control" id="pays_id"'); ?>
                      </fieldset>
                    </div>

                    <div class="col-xs-6">
                      <fieldset class="form-group">
                        <label for="">Ville</label>
                        <div id="ville_id_div"></div>
                        <input type="text" class="form-control" id="ville_id" name="ville_id">
                      </fieldset>
                    </div>

                    <div class="col-xs-12">
                      <fieldset class="form-group">
                        <label for="">Adresse</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="adresse" name="adresse">
                        </div>
                      </fieldset>
                    </div>



                  </div>
                </div>
              </div>

              <div class="row"> 
                <div class="col-xs-3">
                  <fieldset class="form-group">
                    <label for="">Fonction</label>
                    <?php echo form_dropdown('salariefonction_id', $comboSalarieFonctions, '', 'class="form-control" id="salariefonction_id"'); ?>
                  </fieldset>
                </div>

                <div class="col-xs-3">
                  <fieldset class="form-group">
                    <label for="">Statut</label>
                    <?php echo form_dropdown('salariestatut_id', $comboSalarieStatuts, '', 'class="form-control" id="salariestatut_id"'); ?>
                  </fieldset>
                </div>

                <div class="col-xs-3">
                  <fieldset class="form-group">
                    <label for="">Date d'intégration</label>
                        <input type="date" class="form-control" id="date_recrutement" name="date_recrutement">
                  </fieldset>
                </div>
                
                <div class="col-xs-3">
                  <fieldset class="form-group demission-group">
                    <label for="">Date de départ</label>
                        <input type="date" class="form-control" id="date_demission" name="date_demission">
                  </fieldset>
                </div>
                
              </div>



  
                <!--div class="row">
                  
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Nom</label>
                          <input type="text" class="form-control" id="nom" name="nom">
                    </fieldset>
                  </div>
                  
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Prénom</label>
                      <div class="input-group">
                          <input type="text" class="form-control" id="prenom" name="prenom">
                      </div>
                    </fieldset>
                  </div>

                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Pièce d'identité</label>
                      <?php echo form_dropdown('identitetype_id', $comboTypesIdentite, '', 'class="form-control" id="identitetype_id"'); ?>
                    </fieldset>
                  </div>

                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">N° de la pièce d'identité</label>
                          <input type="text" class="form-control" id="cin" name="cin">
                    </fieldset>
                  </div>

                  <div class="col-xs-6">
                    <div class="row">
                        
                      <div class="col-xs-6">
                        <fieldset class="form-group">
                          <label for="">Pays</label>
                          <?php echo form_dropdown('pays_id', $comboPays, '', 'class="form-control" id="pays_id"'); ?>
                        </fieldset>
                      </div>

                      <div class="col-xs-6">
                        <fieldset class="form-group">
                          <label for="">Ville</label>
                          <div id="ville_id_div"></div>
                          <input type="text" class="form-control" id="ville_id" name="ville_id">
                        </fieldset>
                      </div>

                      <div class="col-xs-6">
                        <fieldset class="form-group">
                          <label for="">Email</label>
                          <div class="input-group">
                              <input type="text" class="form-control" id="email" name="email">
                          </div>
                        </fieldset>
                      </div>

                      <div class="col-xs-6">
                        <fieldset class="form-group">
                          <label for="">Tél</label>
                          <div class="input-group">
                            <div class="row">
                              <div class="col-xs-4 indicatif" id="">
                                <input type="text" class="form-control" name="indicatif" id="indicatif">
                              </div>
                              <div class="col-xs-8 tel" id="">
                                <input type="text" class="form-control" name="tel" id="tel">
                              </div>
                            </div>
                          </div>
                        </fieldset>
                      </div>
                
                    </div>
                  </div>
                  
                  <div class="col-xs-6">
                    <fieldset class="form-group">
                      <label for="">Adresse</label>
                      <div class="input-group">
                        <!--input type="text" class="form-control" id="adresse" name="adresse"- ->
                        <textarea class="form-control" id="adresse" name="adresse" rows="4"></textarea>
                      </div>
                    </fieldset>
                  </div>
                </div>

                <hr class="m-b-0">
                
                <div class="row">  
                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Fonction</label>
                      <?php echo form_dropdown('salariefonction_id', $comboSalarieFonctions, '', 'class="form-control" id="salariefonction_id"'); ?>
                    </fieldset>
                  </div>

                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Statut</label>
                      <?php echo form_dropdown('salariestatut_id', $comboSalarieStatuts, '', 'class="form-control" id="salariestatut_id"'); ?>
                    </fieldset>
                  </div>

                  <div class="col-xs-3">
                    <fieldset class="form-group">
                      <label for="">Date d'intégration</label>
                          <input type="date" class="form-control" id="date_recrutement" name="date_recrutement">
                    </fieldset>
                  </div>
                  
                  <div class="col-xs-3">
                    <fieldset class="form-group demission-group">
                      <label for="">Date de départ</label>
                          <input type="date" class="form-control" id="date_demission" name="date_demission">
                    </fieldset>
                  </div>
            	  </div-->

              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Enregistrer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->