<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
        
    }
	
	// Types des cotisations
	public function cotisationtypes_get()
	{
		if(! $this->get('id'))
		{
			$tasks = $this->cotisationtype_model->get_all();
		} else {
			$tasks = $this->cotisationtype_model->get_task($this->get('id'));
		}
		
		if($tasks)
		{
			$this->response($tasks, 200);
		} else {
			$this->response([], 404);
		}
	}
	public function cotisationtypes_post()
	{
		if(! ($this->post('cotisation_type') && $this->post('valeur')) )
		{
			$this->response(array('error' => 'Veuillez saisir le type et le montant de la cotisation !'), 400);
		}
		else{
			$data = array(
				'cotisation_type' => $this->post('cotisation_type'),
				'valeur' => $this->post('valeur')
			);
		}
		$this->db->insert('cotisationtypes',$data);
		if($this->db->insert_id() > 0)
		{
			$message = array(
				'id' => $this->db->insert_id(), 
				'cotisation_type' => $this->post('cotisation_type'),
				'valeur' => $this->post('valeur')
				);
			$this->response($message, 200); // 200 being the HTTP response code
		}
	}
	public function cotisationtypes_delete($id=NULL)
	{
		if($id == NULL)
		{
			$message = array('error' => 'id invalide !');
			$this->response($message, 400);
		} else {
			$this->cotisationtype_model->delete_cotisationtype($id);
			$message = array('id' => $id, 'message' => 'Type supprimé !');
			$this->response($message, 200);
		}
	}
	public function cotisationtypes_put()
	{
		if(! ($this->put('cotisation_type') && $this->put('valeur')) )
		{
			$this->response(array('error' => 'Veuillez saisir le type et le montant de la cotisation !'), 400);
		}
		$data = array(
			'cotisation_type' => $this->put('cotisation_type'),
			'valeur' => $this->put('valeur')
		);
		//var_dump($this->put('cotisation_type'));die();
		
		$this->cotisationtype_model->update_cotisationtype($this->put('id'), $data);
		$message = array('success' => 'Le type "'.$this->put('cotisation_type').'" est bien enregistré.');
		$this->response($message, 200);
	}

	// Projets
	public function projets_get()
	{
		$this->load->model('projet_model');
		if(! $this->get('id'))
		{
			$projects = $this->projet_model->get_all();
		} else {
			$projects = $this->projet_model->get_projet($this->get('id'));
		}
		
		if($projects)
		{
			$this->response($projects, 200);
		} else {
			$this->response([], 404);
		}
	}
	public function projets_post()
	{
		if(! ($this->post('titre') && $this->post('code') && $this->post('date_entree')) )
		{
			$this->response(array('error' => 'Veuillez saisir toutes les informations du projet !'), 400);
		}
		else{
			$data = array(
				'titre' => $this->post('titre'),
				'code' => $this->post('code'),
				'date_entree' => $this->post('date_entree'),
				'description' => ($this->post('description'))?$this->post('description'):""
			);
		}
		$this->db->insert('projets',$data);
		if($this->db->insert_id() > 0)
		{
			$message = array(
				'id' => $this->db->insert_id(), 
				'cotisation_type' => $this->post('cotisation_type'),
				'valeur' => $this->post('valeur')
				);
			$this->response($message, 200); // 200 being the HTTP response code
		}
	}
	public function projets_put()
	{
		if(! ($this->post('titre') && $this->post('code') && $this->post('date_entree')) )
		{
			$this->response(array('error' => 'Veuillez saisir toutes les informations du projet !'), 400);
		}
		$data = array(
			'titre' => $this->post('titre'),
			'code' => $this->post('code'),
			'date_entree' => $this->post('date_entree'),
			'description' => ($this->post('description'))?$this->post('description'):""
		);
		
		$this->projet_model->update_projet($this->put('id'), $data);
		$message = array('success' => 'Le type "'.$this->put('cotisation_type').'" est bien enregistré.');
		$this->response($message, 200);
	}
	public function projets_delete($id=NULL)
	{
		if($id == NULL)
		{
			$message = array('error' => 'id invalide !');
			$this->response($message, 400);
		} else {
			$this->projet_model->delete_projet($id);
			$message = array('id' => $id, 'message' => 'Type supprimé !');
			$this->response($message, 200);
		}
	}


}