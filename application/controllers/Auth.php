<?php
class Auth extends CI_Controller {

    function index() {
        if( $this->session->userdata('isLoggedIn') ) {
            redirect('/');
        } else {
            $this->login(false);
        }
    }

    function check() {
        // Create an instance of the user model
        $this->load->model('auth_model');
        // Grab the email and password from the form POST
        //print_r($this->input->post());
        //    die("auth OK");
        $email = $this->input->post('login');
        $pass  = $this->input->post('password');
        //Ensure values exist for email and pass, and validate the user's credentials
        if( $email && $pass && $this->auth_model->validate($email,$pass)) {
            // If the user is valid, redirect to the main view
            redirect('/');
        } else {
            // Otherwise show the login screen with an error message.
            $this->login(true);
        }
    }

    function login( $show_error = false ) {

        if( $this->session->userdata('isLoggedIn') ) {
            redirect('/');
        } else {
            $data['error'] = $show_error;
            $this->load->helper('form');
            $this->load->view('admin/auth/login',$data);
        }
    }

    function logout() {
      $this->session->sess_destroy();
      $this->index();
    }

    function showphpinfo() {
        echo phpinfo();
    }
}