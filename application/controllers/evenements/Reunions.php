<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reunions extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('cnmembre_model');
        $this->load->model('reunion_model');
    }

    public function index() 
    {
		$this->data['titre'] = "Réunions";
		$this->data['entityname'] = "reunion";
		$this->data['ajaxurl'] = "reunions/json";
		$this->data['css'] = '
    <link rel="stylesheet" href="/assets/styles/gsi-step-indicator.css"/>
    <link rel="stylesheet" href="/assets/styles/tsf-step-form-wizard.css"/>';
		$this->data['scripts'] = '
    <script src="/assets/vendor/parsleyjs/dist/parsley.min.js"></script>
    <script src="/assets/scripts/helpers/tsf/js/tsf-wizard-plugin.js"></script>';
		$this->data['zonescripts'] = 'admin/reunions/dt';
		$this->data['zonecontenu'] = "admin/reunions/view";
		$this->data['zonemodals'] = "admin/reunions/modals";

        $membresObj = $this->cnmembre_model
                        ->where('mandat', date("Y"))
                        ->with_adherent(array('with'=>array(
                                                        'relation'=>'personne'
                                                        //,'fields'=>'id,nom,prenom'
                                                        )))
                        //->as_dropdown('nom')
                        ->get_all();
        $membresArr = array();                        
        foreach ($membresObj as $item) {
            $membresArr[$item->id] = $item->adherent->personne->nom ." ". $item->adherent->personne->prenom; 
        }
        //var_dump($membresArr);die();

        $this->data['comboMembresCN'] = $membresArr;
		$this->data['comboTypesReunion'] = $this->data2array("reuniontypes");
        //var_dump($this->comboArray("reuniontypes","id", "type_reunion"));die();
		$this->load->view('admin/layout',$this->data);
    }
    
    public function json($type=null) 
    {
        if ($type) {
            $list = $this->reunion_model->with_type()->with('sujet')->where('reuniontype_id',$type)->get_all();
        } else {
            $list = $this->reunion_model->with('type')->with('sujet')->get_all();
        }
        
        
        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;

                $row = array();
                $row[] = date_format(new DateTime($item->date_reunion), 'd/m/Y');
                $row[] = $item->lieu;
                $row[] = $item->objet;
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function ajax_edit($id)
    {
        $this->data = $this->reunion_model->get($id);
        echo json_encode($this->data);
    }
 
    public function ajax_add()
    {
        echo "<p>nouvelle réunion</p>";
        print_r($this->input->post());die();
        $rid = $this->reunion_model->insert(array(
            'reuniontype_id' => $this->input->post('reuniontype_id'),
            'objet' => $this->input->post('objet'),
            'sujet_id' => $this->input->post('sujet_id'),
            'date_reunion' => $this->input->post('date_reunion'),
            'lieu' => $this->input->post('lieu'),
            'chorum' => $this->input->post('chorum'),
        ));
        //echo $pid; die();
        if($pid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur d'ajout de la réunion"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "La réunion a été bien créé."));
        }
    }
 
    public function ajax_update($sid)
    {
        if($pid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur: réunion introuvable !"));
        }else{
            $irid = $this->reunion_model->update(array(
                'reuniontype_id' => $this->input->post('reuniontype_id'),
                'objet' => $this->input->post('objet'),
                'sujet_id' => $this->input->post('sujet_id'),
                'date_reunion' => $this->input->post('date_reunion'),
                'lieu' => $this->input->post('lieu'),
                'chorum' => $this->input->post('chorum'),
            ), $pid);
            //echo $ipid; die();
            if($irid === false){
                //error salarie
                echo json_encode(array("status" => FALSE,"msg" => "error salarie"));
            }else{
                echo json_encode(array("status" => TRUE));
            }
        }
    }
 
    public function ajax_delete($id)
    {
        if (($id == NULL) or ($this->reunion_model->delete($id) == 0)) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }
 
}
