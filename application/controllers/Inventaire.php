<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inventaire extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('matcat_model');
        $this->load->model('matsouscat_model');
        $this->load->model('materiel_model');
        $this->load->model('doccat_model');
        $this->load->model('docsouscat_model');
        $this->load->model('document_model');
    }

    public function index() 
    {
        //$this->output->enable_profiler(TRUE);
        //var_dump($this->cnmembre_model->mandats());die();

		$this->data['titre'] = "Inventaire";
		$this->data['entityname'] = "inventaire";
		$this->data['ajaxurl'] = "inventaire/json";
		$this->data['css'] = '';
		$this->data['scripts'] = '';
		$this->data['zonescripts'] = 'admin/inventaire/dt';
		$this->data['zonecontenu'] = "admin/inventaire/view";
		$this->data['zonemodals'] = "admin/inventaire/modals";

		$this->data['comboSouscat'] = $this->comboArray("mat_sous_cat","id", "designation");
		$this->data['comboCat'] = $this->comboArray("mat_cat","id", "designation");
		$this->data['comboDocSouscat'] = $this->comboArray("doc_sous_cat","id", "designation");
		$this->data['comboDocCat'] = $this->comboArray("doc_cat","id", "designation");
		//$this->data['comboReunions'] = $this->comboArray("reunions","id", "CONCAT('[', DATE_FORMAT(date_reunion,'%d/%m/%Y'), '] ', objet) as details_reunion");
		$this->load->view('admin/layout',$this->data);
    }
    
    public function jsonm() 
    {
        $list = $this->materiel_model
                        ->with_matcat(array(
                            'with'=>array(
                                'relation'=>'matcat'
                                )
                            ))
                        ->get_all();
        
        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //print_r($item);die();
                $row = array();
                $row[] = $item->code;
                $row[] = $item->intitule;
                $row[] = $item->matcat->matcat->designation." > ".$item->matcat->designation;
                $row[] = $item->caracteristique;
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier </a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="supprimer_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
    
    public function jsond() 
    {
        $list = $this->document_model
                        ->with_doccat(array(
                            'with'=>array(
                                'relation'=>'doccat'
                                )
                            ))
                        ->get_all();
        
        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //print_r($item);die();
                $row = array();
                $row[] = $item->ref;
                $row[] = $item->titre;
                $row[] = $item->doccat->doccat->designation." > ".$item->doccat->designation;
                $row[] = $item->annee;
                $row[] = $item->auteurs;
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_doc_entity('."'".$item->id."'".')">Modifier </a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_doc_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
    
    public function souscatmat($pid) 
    {
        $souscats = $this->matsouscat_model->where('cat_id',$pid)->as_dropdown('designation')->get_all();
        //var_dump($souscats);
        if ($souscats) echo form_dropdown('cat_id', $souscats, '', 'class="form-control" id="cat_id"');
        if (!$souscats) echo form_dropdown('cat_id', array("designation"=>"", "designation"=>""), '', 'class="form-control" id="cat_id"');
    }
    
    public function souscatdoc($pid) 
    {
        $souscats = $this->docsouscat_model->where('cat_id',$pid)->as_dropdown('designation')->get_all();
        //var_dump($souscats);
        if ($souscats) echo form_dropdown('cat_id', $souscats, '', 'class="form-control" id="cat_id"');
        if (!$souscats) echo form_dropdown('cat_id', array("designation"=>"", "designation"=>""), '', 'class="form-control" id="cat_id"');
    }
    
    public function ajax_edit_mat($id)
    {
        $this->data = $this->materiel_model
                        ->with_matcat(array(
                            'with'=>array(
                                'relation'=>'matcat'
                                )
                            ))
                        ->get($id);
        //print_r($this->data);
        echo json_encode($this->data);
    }
    
    public function ajax_edit_doc($id)
    {
        $this->data = $this->document_model
                        ->with_doccat(array(
                            'with'=>array(
                                'relation'=>'doccat'
                                )
                            ))
                        ->get($id);
        //print_r($this->data);
        echo json_encode($this->data);
    }
 
    public function ajax_add_mat()
    {
        $imid = $this->materiel_model->insert(array(
            'code' => $this->input->post('code'),
            'intitule' => $this->input->post('intitule'),
            'caracteristique' => $this->input->post('caracteristique'),
            'cat_id' => $this->input->post('cat_id'),
        ));

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur d'ajout du matériel"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Le matériel a été bien ajouté."));
        }
    }
 
    public function ajax_add_doc()
    {
        $imid = $this->document_model->insert(array(
            'ref' => $this->input->post('ref'),
            'titre' => $this->input->post('titre'),
            'annee' => $this->input->post('annee'),
            'auteurs' => $this->input->post('auteurs'),
            'cat_id' => $this->input->post('cat_id'),
        ));

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur d'ajout du document"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Le document a été bien ajouté."));
        }
    }
 
    public function ajax_update_mat($id)
    {            
        $iid = $this->materiel_model->update(array(
            'code' => $this->input->post('code'),
            'intitule' => $this->input->post('intitule'),
            'caracteristique' => $this->input->post('caracteristique'),
            'cat_id' => $this->input->post('cat_id'),
        ), $id);

        if($iid === false){
            echo json_encode(array("status" => FALSE,"msg" => "update error"));
        }else{
            echo json_encode(array("status" => TRUE));
        }
    }
 
    public function ajax_update_doc($id)
    {            
        $iid = $this->document_model->update(array(
            'ref' => $this->input->post('ref'),
            'titre' => $this->input->post('titre'),
            'annee' => $this->input->post('annee'),
            'auteurs' => $this->input->post('auteurs'),
            'cat_id' => $this->input->post('cat_id'),
        ), $id);

        if($iid === false){
            echo json_encode(array("status" => FALSE,"msg" => "update error"));
        }else{
            echo json_encode(array("status" => TRUE));
        }
    }
 
    public function ajax_delete_mat($id)
    {
        if ($this->materiel_model->delete($id) == 0) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }
    
    public function ajax_delete_doc($id)
    {
        if ($this->document_model->delete($id) == 0) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }
 
}
