<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parametrage extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function table($name string)
	{
		echo $name;
	}

	public function personnes()
	{
			//$this->output->enable_profiler(TRUE);
			
			$crud = new grocery_CRUD();
			$crud->unset_jquery();
			$crud->set_theme('flexigrid');
			$crud->set_table('personnes');
			$crud->set_relation('personnetype_id','personnetypes','type_personne');
			$crud->display_as('personnetype_id','Type');
			$crud->set_subject('Personne');

			$crud->required_fields('cin', 'nom', 'prenom');

			$outputData = $crud->render();
			
			$outputData->titre = "Gestion des personnes";
			$outputData->zonecontenu = "admin/crud";
			$outputData->menu_procedures = $this->data["menu_procedures"];
			
			$this->_output_data($outputData);
			
			//$outputData = (array)$outputData;
			//$this->data['zonecontenu'] = "admin/personnes/crud";
			//var_dump($outputData);die();
			//$this->load->view('admin/layout',$outputData);
	}

	public function types_personne()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('personnetypes');
			$crud->display_as('id','ID');
			$crud->set_subject('Type personne');

			//$crud->required_fields('designation');

			$outputData = $crud->render();
			
			$outputData->titre = "Types des personnes";
			$outputData->zonecontenu = "admin/crud";
			$outputData->menu_procedures = $this->data["menu_procedures"];
			
			$this->_output_data($outputData);
	}

	public function types_adherent()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('adherenttypes');
			$crud->display_as('id','ID');
			$crud->set_subject('Type adhérent');

			//$crud->required_fields('designation');

			$outputData = $crud->render();
			
			$outputData->titre = "Types des adhérents";
			$outputData->zonecontenu = "admin/crud";
			$outputData->menu_procedures = $this->data["menu_procedures"];
			
			$this->_output_data($outputData);
	}

	public function fonctions_salarie()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('salariefonctions');
			$crud->display_as('id','ID');
			$crud->set_subject('Fonction');

			//$crud->required_fields('designation');

			$outputData = $crud->render();
			
			$outputData->titre = "Fonctions des salariés";
			$outputData->zonecontenu = "admin/crud";
			$outputData->menu_procedures = $this->data["menu_procedures"];
			
			$this->_output_data($outputData);
	}
	
	public function fonctions_cn()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('cnfonctions');
			$crud->display_as('id','ID');
			$crud->set_subject('fonction');

			//$crud->required_fields('designation');

			$outputData = $crud->render();
			
			$outputData->titre = "Fonctions du bureau administratif";
			$outputData->zonecontenu = "admin/crud";
			$outputData->menu_procedures = $this->data["menu_procedures"];
			
			$this->_output_data($outputData);
	}
	
	public function modes_cotisation()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('cotisationmodes');
			$crud->display_as('id','ID');
			$crud->set_subject('mode');

			//$crud->required_fields('designation');

			$outputData = $crud->render();
			
			$outputData->titre = "Modes de cotisation";
			$outputData->zonecontenu = "admin/crud";
			$outputData->menu_procedures = $this->data["menu_procedures"];
			
			$this->_output_data($outputData);
	}
	
	public function types_cotisation()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('cotisationtypes');
			$crud->display_as('id','ID');
			$crud->set_subject('type');

			//$crud->required_fields('designation');

			$outputData = $crud->render();
			
			$outputData->titre = "Types de cotisation";
			$outputData->zonecontenu = "admin/crud";
			$outputData->menu_procedures = $this->data["menu_procedures"];
			
			$this->_output_data($outputData);
	}
	
	public function types_reunion()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('reuniontypes');
			$crud->display_as('id','ID');
			$crud->set_subject('type');

			//$crud->required_fields('designation');

			$outputData = $crud->render();
			
			$outputData->titre = "Types des réunions";
			$outputData->zonecontenu = "admin/crud";
			$outputData->menu_procedures = $this->data["menu_procedures"];
			
			$this->_output_data($outputData);
	}
	
	public function procedures()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('procedures');
			$crud->display_as('id','ID');
			$crud->set_subject('procedure');

			//$crud->required_fields('designation');

			$outputData = $crud->render();
			
			$outputData->titre = "Gestion des procedures";
			$outputData->zonecontenu = "admin/crud";
			$outputData->menu_procedures = $this->data["menu_procedures"];
			
			$this->_output_data($outputData);
	}
	
}
