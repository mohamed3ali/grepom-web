<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Cotisations extends MY_Controller {
    
    public $adh;
 
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('cotisation_model');
        $this->load->model('annee_model');
		//$this->load->view('admin/cotisations/view','cotisations');
    }

    public function index()
    {
		$this->data['titre'] = "Cotisations";
		$this->data['entityname'] = "cotisation";
		$this->data['ajaxurl'] = "cotisations/json";
		$this->data['css'] = '';
		$this->data['scripts'] = '';
		$this->data['zonescripts'] = 'admin/cotisations/dt';
		$this->data['zonecontenu'] = "admin/cotisations/view";
		$this->data['zonemodals'] = "admin/cotisations/modals";
        $this->data['comboAnneesCotisation'] = $this->comboArray("annees","annee", "annee");
		$this->data['comboModesPaiement'] = $this->comboArray("modepaiements","id", "designation");
		$this->data['comboTypesCotisation'] = $this->comboArray("cotisationtypes","id", "cotisation_type");
		$this->data['comboAdherents'] = $this->comboArray("adherents","id", "num_adh");
        $this->data['comboPersonnes'] = $this->comboArray("personnes","id", "CONCAT( nom, ' ', prenom ) AS nomprenom");

        //var_dump($totalsCotisations);die();
		$this->data['statsPanelContent'] = '
            <div id="slider-cotisations-bilan" class="carousel slide t-a-c" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
        ';   
        $i=0;
        $totalsCotisations = $this->cotisation_model->totalCotisations();
        //var_dump($totalsCotisations);die();
        foreach ($totalsCotisations as $tc) {
            if ($i<1) {
            $this->data['statsPanelContent'] .= "
                  <div class='carousel-item active'>
                        Total des cotisations en ".$tc->annee_id." : <strong>".$tc->montant." Dh</strong>
                </div>";
            } else {
            $this->data['statsPanelContent'] .= "
                  <div class='carousel-item'>
                        Total des cotisations en ".$tc->annee_id." : <strong>".$tc->montant."</strong>
                </div>";
            }
            
            $i++;
        }
                
        $this->data['statsPanelContent'] .= '
              </div>
              <a class="left carousel-control" href="#slider-cotisations-bilan" role="button" data-slide="prev">
                <span class="icon-prev" aria-hidden="true"></span>
                <span class="sr-only"><<</span>
              </a>
              <a class="right carousel-control" href="#slider-cotisations-bilan" role="button" data-slide="next">
                <span class="icon-next" aria-hidden="true"></span>
                <span class="sr-only">>></span>
              </a>
            </div>
        ';
		$this->load->view('admin/layout',$this->data);
    }
    
    public function adh($id)
    {
        $this->adh = $id;
        $this->session->set_userdata(array('popup-id'  => $id));
        redirect('/cotisations', 'refresh');
    }

    public function json()
    {
        $list = $this->cotisation_model
                        ->order_by('annee_id', 'desc')
                        ->with_adherent(array(
                            'with'=>array(
                                'relation'=>'personne'
                                )
                            ))
                        /*->with('adherent')*/
                        ->with('personne')
                        ->with('cotisationtype')
                        ->with('modepaiement')
                        ->with('annee')
                        ->get_all();

        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
            	//var_dump($item);die();
                $no++;
                $row = array();
                //$row[] = $item->id;
                $row[] = $item->numrecu;
                $row[] = ' <a href="/rh/adherents/adh/'.$item->adherent->id.'">'.$item->adherent->num_adh." / ".$item->adherent->personne->nom." ".$item->adherent->personne->prenom.'</a>';
                $row[] = $item->annee_id;
                $row[] = $item->cotisationtype->cotisation_type;
                $row[] = $item->modepaiement->designation;
                $row[] = $item->montant;
                $row[] = (isset($item->personne)) ? $item->personne->nom.' '.$item->personne->prenom : '' ;
                //$row[] = $item->personne->nom.' '.$item->personne->prenom;//$cotisation->receptionpar_id;
                //$row[] = $item->remarques;
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier </a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $this->data = $this->cotisation_model
                        ->with('adherent')
                        ->with('personne')
                        ->with('cotisationtype')
                        ->with('modepaiement')
                        ->get($id);
        echo json_encode($this->data);
    }
 
    public function nouvelle_annee($annee=null)
    {
        if ($annee) {
            $data = array(
                    'annee' => $annee
                );
            $inserted = $this->annee_model->insert($data);

        } else {
        }
        $annees = $this->annee_model->order_by("annee", "desc")->as_dropdown('annee')->get_all();
        //var_dump($villes);
        echo form_dropdown('annee_id', $annees, '', 'class="form-control" id="annee_id"');
    }

    public function ajax_add()
    {
        $this->data = array(
                'adherent_id' => $this->input->post('adherent_id'),
                'montant' => $this->input->post('montant'),
                'annee_id' => $this->input->post('annee_id'),
                'date_cotisation' => $this->input->post('date_cotisation'),
                'numrecu' => $this->input->post('numrecu'),
                'modepaiement_id' => $this->input->post('modepaiement_id'),
                'cotisationtype_id' => $this->input->post('cotisationtype_id'),
                'receptionpar_id' => $this->input->post('receptionpar_id'),
                'remarques' => $this->input->post('remarques'),
            );
        $insert = $this->cotisation_model->insert($this->data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update($id)
    {
        
        $this->data = array(
                'adherent_id' => $this->input->post('adherent_id'),
                'montant' => $this->input->post('montant'),
                'annee_id' => $this->input->post('annee_id'),
                'date_cotisation' => $this->input->post('date_cotisation'),
                'numrecu' => $this->input->post('numrecu'),
                'modepaiement_id' => $this->input->post('modepaiement_id'),
                'cotisationtype_id' => $this->input->post('cotisationtype_id'),
                'receptionpar_id' => $this->input->post('receptionpar_id'),
                'remarques' => $this->input->post('remarques'),
            );
        $this->cotisation_model->update($this->data, $id);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_delete($id)
    {
        $this->cotisation_model->delete($id);
        echo json_encode(array("status" => TRUE));
    }

}