<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Adherents extends MY_Controller {

    public $adh;

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('personne_model');
        $this->load->model('adherent_model');
        $this->load->model('pays_model');
        $this->load->model('ville_model');
        $this->load->model('identitetype_model');
    }

    public function index() 
    {
        $this->data['titre'] = "Liste des adhérents";
        $this->data['entityname'] = "adherent";
        $this->data['ajaxurl'] = "adherents/json";
        $this->data['css'] = '';
        $this->data['scripts'] = '';
        $this->data['zonescripts'] = 'admin/adherents/dt';
        $this->data['zonecontenu'] = "admin/adherents/view";
        $this->data['zonemodals'] = "admin/adherents/modals";
        $this->data['comboTypesIdentite'] = $this->comboArray("identitetypes","id", "type_identite");
        $this->data['comboTypesAdherent'] = $this->comboArray("adherenttypes","id", "type_adh");
        $this->data['comboPays'] = $this->comboArray("pays","Code", "Name");
        $this->data['comboTypesAdherent'] = $this->comboArray("adherenttypes","id", "type_adh");
        $this->load->view('admin/layout',$this->data);
    }

    public function json() 
    {
        $carte = array(
            array('class'=>'warning', 'txt'=>'En attente'),
            array('class'=>'info', 'txt'=>'Délivrée'),
            array('class'=>'success', 'txt'=>'Reçue'),
            );

        $list = $this->adherent_model
                        ->with_personne(array(
                            'with'=>array(
                                'relation'=>'identitetype'
                                )
                            ))
                        ->with('adherenttype')
                        ->get_all();
        
        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                $row = array();
                $row[] = '<a href="javascript:void(0)" onclick="show_adh('.$item->id.')">'.$item->num_adh.'</a>';
                $row[] = '<a href="javascript:void(0)" onclick="show_adh('.$item->id.')">'.$item->personne->nom.' '.$item->personne->prenom.'</a>';
                $row[] = '<a href="javascript:void(0)" onclick="show_adh('.$item->id.')">'.$item->personne->cin." (".$item->personne->identitetype->type_identite.")".'</a>';
                $row[] = date('d/m/Y', strtotime($item->date_adhesion));
                //$row[] = ((time()-(60*60*24)) < strtotime($item->date_depart)) ? "Inactif":"Actif";
                $row[] = $item->adherenttype->type_adh;
                $row[] = ($item->date_resiliation) ? "Départ le ".date('d/m/Y', strtotime($item->date_resiliation)):"Actif";
                $row[] = '<button type="button" class="btn btn-carte-status btn-'.$carte[$item->carte]['class'].' btn-round btn-sm m-r-xs m-b-xs"  onclick="carte_status('."'".$item->id."'".','."'".$item->carte."'".')">
                          '.$carte[$item->carte]['txt'].'
                        </button>';
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier </a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="/cotisations/adh/'.$item->id.'">Cotisation</a>
                    </div>
                </div>

                <!--a href="/cotisations/adh/'.$item->id.'" title="Cotisation" class="btn btn-success btn-sm m-r-xs"><i class="material-icons">attach_money</i></a>
                <a href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')" class="btn btn-warning btn-sm m-r-xs"><i class="material-icons">create</i></a>
                <a href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')" class="btn btn-danger btn-sm m-r-xs"><i class="material-icons">delete sweep</i></a-->';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function ajax_edit($id)
    {
        $this->data = $this->adherent_model->with('personne')->get($id);
        echo json_encode($this->data);
    }

    public function fiche($id)
    {
        $this->data = $this->adherent_model
                        ->with_personne(array(
                            'with'=>array(
                                array('relation'=>'pays'),
                                array('relation'=>'profession'),
                                array('relation'=>'identitetype')
                                )
                            ))
                        ->with('adherenttype')
                        ->get($id);
        $this->data->date_adhesion = date('d/m/Y', strtotime($this->data->date_adhesion));
        //var_dump($this->data);die();
        echo json_encode($this->data);
    }
 
    public function ajax_add()
    {
        //echo '<pre>';
        ////print_r($this->input->post());
        //print_r($_FILES);
        //print_r($_POST);
        //echo '</pre>';
        //die();
        if (isset($_POST['cin']) && $_POST['cin']) {

            $data['config'] = array(
                'file_name' => 'photo-'.strtolower($this->input->post('cin')),
                'upload_path' => FCPATH . 'uploads/photos/',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '2048',
                'multi' => 'all'
            );
            if (isset($_FILES['photo']['name']) && $_FILES['photo']['name']) {

                $this->load->library('upload', $data['config']);
                //$this->upload->initialize($config);

                if ($this->upload->do_upload('photo'))
                {
                    $data['uploads'] = $this->upload->data();
                }
                else
                {
                    $data['uploads']['file_name'] = null;
                    return $this->upload->display_errors();
                }

            }else{
                //echo json_encode(array("status" => FALSE,"msg" => "Photo vide"));
            }

            $pid = $this->personne_model->insert(array(
                'photo' => $data['uploads']['file_name'],
                'identitetype_id' => $this->input->post('identitetype_id'),
                'cin' => $this->input->post('cin'),
                'nom' => $this->input->post('nom'),
                'prenom' => $this->input->post('prenom'),
                'email' => $this->input->post('email'),
                'indicatif' => $this->input->post('indicatif'),
                'tel' => $this->input->post('tel'),
                'ville_id' => $this->input->post('ville_id'),
                'pays_id' => $this->input->post('pays_id'),
                'adresse' => $this->input->post('adresse'),
                'personnetype_id' => 1,
            ));
                //echo $pid; die();
            if($pid === false){
                //error personne
                unlink($data['full_path']);
                header('HTTP/1.1 500 Internal Server Error');
                echo json_encode(array("status" => FALSE,"msg" => "error personne"));
            }else{
                $aid = $this->adherent_model->insert(array(
                    'num_adh' => $this->input->post('num_adh'),
                    'personne_id' => $pid,
                    'date_adhesion' => $this->input->post('date_adhesion'),
                    'adherenttype_id' => $this->input->post('adherenttype_id'),
                ));
                //echo var_dump($this->input->post()); die();
                if($aid === false){
                    //error adherent, rollback
                    $this->personne_model->delete($pid);
                    header('HTTP/1.1 500 Internal Server Error');
                    echo json_encode(array("status" => FALSE,"msg" => "error adherent"));
                }else{
                    echo json_encode(array("status" => TRUE));
                }
            }
        }else{
            header('HTTP/1.1 500 Internal Server Error');
            echo json_encode(array("status" => FALSE,"msg" => "CIN vide"));
        }
    }
 
    public function ajax_update($aid)
    {

        if (isset($_POST['cin']) && $_POST['cin']) {
            

            $data['config'] = array(
                'file_name' => 'photo-'.strtolower($this->input->post('cin')),
                'upload_path' => FCPATH . 'uploads/photos/',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '2048',
                'multi' => 'all'
            );
            if (isset($_FILES['photo']['name']) && $_FILES['photo']['name']) {

                $this->load->library('upload', $data['config']);
                //$this->upload->initialize($config);

                if ($this->upload->do_upload('photo'))
                {
                    $data['uploads'] = $this->upload->data();

                    //echo '<pre>';
                    //print_r($data['uploads']);
                    //echo '</pre>';
                    //die();
                }
                else
                {
                    $data['uploads']['file_name'] = null;
                    //return $this->upload->display_errors();
                    header('HTTP/1.1 500 Internal Server Error');
                    echo json_encode(array("status" => FALSE,"msg" => $this->upload->display_errors()));
                }

            }else{
                //echo json_encode(array("status" => FALSE,"msg" => "Photo vide"));
            }



            // TODO: check if id is correctly returned
            $pid = $this->adherent_model->get($aid)->personne_id;
            
            if($pid === false){
                //error personne
                echo json_encode(array("status" => FALSE,"msg" => "error personne"));
            }else{
                if($data['uploads']['file_name']){
                    $ipid = $this->personne_model->update(array(
                        'photo' => $data['uploads']['file_name'],
                        'identitetype_id' => $this->input->post('identitetype_id'),
                        'cin' => $this->input->post('cin'),
                        'nom' => $this->input->post('nom'),
                        'prenom' => $this->input->post('prenom'),
                        'email' => $this->input->post('email'),
                        'indicatif' => $this->input->post('indicatif'),
                        'tel' => $this->input->post('tel'),
                        'ville_id' => $this->input->post('ville_id'),
                        'pays_id' => $this->input->post('pays_id'),
                        'adresse' => $this->input->post('adresse'),
                    ), $pid);
                }else{
                    $ipid = $this->personne_model->update(array(
                        'identitetype_id' => $this->input->post('identitetype_id'),
                        'cin' => $this->input->post('cin'),
                        'nom' => $this->input->post('nom'),
                        'prenom' => $this->input->post('prenom'),
                        'email' => $this->input->post('email'),
                        'indicatif' => $this->input->post('indicatif'),
                        'tel' => $this->input->post('tel'),
                        'ville_id' => $this->input->post('ville_id'),
                        'pays_id' => $this->input->post('pays_id'),
                        'adresse' => $this->input->post('adresse'),
                    ), $pid);
                }
                //echo $ipid; die();
                
                $iaid = $this->adherent_model->update(array(
                    'num_adh' => $this->input->post('num_adh'),
                    'personne_id' => $pid,
                    'date_adhesion' => $this->input->post('date_adhesion'),
                    'adherenttype_id' => $this->input->post('adherenttype_id'),
                ), $aid);
                //echo var_dump($this->input->post()); die();
                if($iaid === false){
                    //error adherent
                    echo json_encode(array("status" => FALSE,"msg" => "error adherent"));
                }else{
                    echo json_encode(array("status" => TRUE));
                }
            }
        }else{
            header('HTTP/1.1 500 Internal Server Error');
            echo json_encode(array("status" => FALSE,"msg" => "CIN vide"));
        }
    }
 
    public function ajax_supp_photo($aid)
    {
        // TODO: check if id is correctly returned
        $pid = $this->adherent_model->get($aid)->personne_id;
        
        if($pid === false){
            //error personne
            echo json_encode(array("status" => FALSE,"msg" => "error personne"));
        }else{
            $this->db->select('photo')->from('personnes')->where('id', $pid);
            $del_photo = $this->db->get()->row()->photo;
            if($del_photo) {
                unlink(FCPATH . "uploads/photos/".$del_photo);
            }
            $ipid = $this->personne_model->update(array(
                'photo' => NULL,
            ), $pid);

            //echo $ipid; die();
            
            if($ipid === false){
                //error adherent
                echo json_encode(array("status" => FALSE,"msg" => "error photo"));
            }else{
                echo json_encode(array("status" => TRUE));
            }
        }
    }

    private function supp_photo($aid)
    {
        $pid = $this->adherent_model->get($aid)->personne_id;        
        if($pid === false){
        }else{
            $this->db->select('photo')->from('personnes')->where('id', $pid);
            $del_photo = $this->db->get()->row()->photo;
            if($del_photo) {
                unlink(FCPATH . "uploads/photos/".$del_photo);
            }
            $ipid = $this->personne_model->update(array(
                'photo' => NULL,
            ), $pid);
        }
    }

    public function carte_status($cid)
    {        
        //echo var_dump($this->input->post()); die();
        $icid = $this->adherent_model->update(array(
            'carte' => $this->input->post('carte'),
        ), $cid);
        if($icid === false){
            //error adherent
            echo json_encode(array("status" => FALSE,"msg" => "error"));
        }else{
            echo json_encode(array("status" => TRUE));
        }
    }
 
    public function ajax_delete($id)
    {
        @@$this->supp_photo($id);
        if ($this->adherent_model->delete($id) == 0) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }
 
    public function ville($pid) 
    {
        $villes = $this->ville_model->where('Country',$pid)->as_dropdown('Name')->get_all();
        //var_dump($villes);
        if ($villes) echo form_dropdown('ville_id', $villes, '', 'class="form-control" id="ville_id"');
        if (!$villes) echo form_dropdown('ville_id', array("Name"=>"", "Name"=>""), '', 'class="form-control" id="ville_id"');
    }

    public function indicatif($pid) 
    {
        $pays = $this->pays_model->where('Code',$pid)->get();
        //var_dump($villes);
        echo $pays->Indicatif;
    }

    public function adh($id)
    {
        $this->adh = $id;
        $this->session->set_userdata(array('popup-id'  => $id));
        redirect('/rh/adherents', 'refresh');
    }
}
