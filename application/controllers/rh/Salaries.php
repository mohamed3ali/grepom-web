<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Salaries extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('personne_model');
        $this->load->model('salarie_model');
    }

    public function index() 
    {
		$this->data['titre'] = "Bureau Exécutif";
		$this->data['entityname'] = "salarie";
		$this->data['ajaxurl'] = "salaries/json";
		$this->data['css'] = '';
		$this->data['scripts'] = '';
		$this->data['zonescripts'] = 'admin/salaries/dt';
		$this->data['zonecontenu'] = "admin/salaries/view";
		$this->data['zonemodals'] = "admin/salaries/modals";
        $this->data['comboSalarieFonctions'] = $this->comboArray("salariefonctions","id", "nom_fonction");
		$this->data['comboSalarieStatuts'] = $this->comboArray("salariestatuts","id", "nom_statut");
        $this->data['comboTypesIdentite'] = $this->comboArray("identitetypes","id", "type_identite");
        $this->data['comboPays'] = $this->comboArray("pays","Code", "Name");
		$this->load->view('admin/layout',$this->data);
    }
    
    public function json() 
    {
        $list = $this->salarie_model
                        ->with_personne(array(
                            'with'=>array(
                                'relation'=>'identitetype'
                                )
                            ))
                        ->with('fonction')
                        ->with('statut')
                        ->get_all();
        
        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //var_dump($item);die();
                $row = array();
                $row[] = $item->personne->nom.' '.$item->personne->prenom;
                $row[] = $item->personne->cin." (".$item->personne->identitetype->type_identite.")";
                $row[] = $item->fonction->nom_fonction;
                $row[] = $item->statut->nom_statut;
                $row[] = date('d/m/Y', strtotime($item->date_recrutement));
                $row[] = $item->date_demission?date('d/m/Y', strtotime($item->date_demission)):"";
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <!--a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a-->
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier </a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function ajax_edit($id)
    {
        $this->data = $this->salarie_model
                        ->with_personne(array(
                            'with'=>array(
                                'relation'=>'identitetype'
                                )
                            ))
                        ->with('fonction')
                        ->get($id);
        echo json_encode($this->data);
    }
 
    public function ajax_add()
    {
        
        if (isset($_POST['cin']) && $_POST['cin']) {
            $data['config'] = array(
                'file_name' => 'photo-'.strtolower($this->input->post('cin')),
                'upload_path' => FCPATH . 'uploads/photos/',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '2048',
                'multi' => 'all'
            );
            if (isset($_FILES['photo']['name']) && $_FILES['photo']['name']) {

                $this->load->library('upload', $data['config']);
                //$this->upload->initialize($config);

                if ($this->upload->do_upload('photo'))
                {
                    $data['uploads'] = $this->upload->data();
                }
                else
                {
                    $data['uploads']['file_name'] = null;
                    header('HTTP/1.1 500 Internal Server Error');
                    echo $this->upload->display_errors();
                }

            }else{
                //header('HTTP/1.1 500 Internal Server Error');
                //echo json_encode(array("status" => FALSE,"msg" => "Photo vide"));
                //return;
            }
    
            $existingPerson = $this->personne_model->where('cin',$this->input->post('cin'))->get();
            $pid = null;

            if($existingPerson){
                //person Already exists
                header('HTTP/1.1 500 Internal Server Error');
                echo json_encode(array("status" => FALSE,"msg" => "Erreur: CIN déja existant!"));
                return;
                //$pid = $existingPerson->id;
            } else {
                // New person, new salarie, peace and love :)
                $pid = $this->personne_model->insert(array(
                    'identitetype_id' => $this->input->post('identitetype_id'),
                    'cin' => $this->input->post('cin'),
                    'nom' => $this->input->post('nom'),
                    'prenom' => $this->input->post('prenom'),
                    'email' => $this->input->post('email'),
                    'indicatif' => $this->input->post('indicatif'),
                    'tel' => $this->input->post('tel'),
                    'ville_id' => $this->input->post('ville_id'),
                    'pays_id' => $this->input->post('pays_id'),
                    'adresse' => $this->input->post('adresse'),
                ));
            }

            if($pid === false){
                //error salarie
                header('HTTP/1.1 500 Internal Server Error');
                echo json_encode(array("status" => FALSE,"msg" => "Erreur d'ajout de la personne"));
            }else{
                $aid = $this->salarie_model->insert(array(
                    'personne_id' => $pid,
                    'date_recrutement' => $this->input->post('date_recrutement'),
                    'salariefonction_id' => $this->input->post('salariefonction_id'),
                    'salariestatut_id' => $this->input->post('salariestatut_id'),
                ));
                //echo var_dump($this->input->post()); die();
                if($aid === false){
                    //roll-back and return error
                    $this->personne_model->delete($pid);
                    header('HTTP/1.1 500 Internal Server Error');
                    echo json_encode(array("status" => FALSE,"msg" => "Erreur d'ajout du salarié"));
                }else{
                    echo json_encode(array("status" => TRUE,"msg" => "Le salarié a été bien créé."));
                }
            }
        }else{
            header('HTTP/1.1 500 Internal Server Error');
            echo json_encode(array("status" => FALSE,"msg" => "CIN vide"));
        }
        
    }
 
    public function ajax_update($sid)
    {
        //$existingPerson = $this->personne_model->where('cin',$this->input->post('cin'))->get();
        //if CIN already exists return error
        //if($existingPerson){
            //person Already exists
            //echo json_encode(array("status" => FALSE,"msg" => "Erreur: CIN déja existant!"));
        //} else {
        //var_dump($this->salarie_model->get($sid));die();
        
        
        if (isset($_POST['cin']) && $_POST['cin']) {
            

            $data['config'] = array(
                'file_name' => 'photo-'.strtolower($this->input->post('cin')),
                'upload_path' => FCPATH . 'uploads/photos/',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '2048',
                'multi' => 'all'
            );
            if (isset($_FILES['photo']['name']) && $_FILES['photo']['name']) {

                $this->load->library('upload', $data['config']);
                //$this->upload->initialize($config);

                if ($this->upload->do_upload('photo'))
                {
                    $data['uploads'] = $this->upload->data();

                    //echo '<pre>';
                    //print_r($data['uploads']);
                    //echo '</pre>';
                    //die();
                }
                else
                {
                    $data['uploads']['file_name'] = null;
                    //return $this->upload->display_errors();
                    header('HTTP/1.1 500 Internal Server Error');
                    echo json_encode(array("status" => FALSE,"msg" => $this->upload->display_errors()));
                }

            }else{
                //echo json_encode(array("status" => FALSE,"msg" => "Photo vide"));
            }


            $pid = $this->salarie_model->get($sid)->personne_id;
            
            if($pid === false){
                //error salarie
                header('HTTP/1.1 500 Internal Server Error');
                echo json_encode(array("status" => FALSE,"msg" => "Erreur: Profile personne introuvable !"));
            }else{
                $ipid = $this->personne_model->update(array(
                    'photo' => $data['uploads']['file_name'],
                    'identitetype_id' => $this->input->post('identitetype_id'),
                    'cin' => $this->input->post('cin'),
                    'nom' => $this->input->post('nom'),
                    'prenom' => $this->input->post('prenom'),
                    'email' => $this->input->post('email'),
                    'indicatif' => $this->input->post('indicatif'),
                    'tel' => $this->input->post('tel'),
                    'ville_id' => $this->input->post('ville_id'),
                    'pays_id' => $this->input->post('pays_id'),
                    'adresse' => $this->input->post('adresse'),
                ), $pid);
                //echo $ipid; die();
                $isid = $this->salarie_model->update(array(
                    'personne_id' => $pid,
                    'date_recrutement' => $this->input->post('date_recrutement'),
                    'date_demission' => $this->input->post('date_demission') ? $this->input->post('date_demission') : null,
                    'salariefonction_id' => $this->input->post('salariefonction_id'),
                    'salariestatut_id' => $this->input->post('salariestatut_id'),
                ), $sid);
                //echo var_dump($this->input->post()); die();
                if($isid === false){
                    //error salarie
                    header('HTTP/1.1 500 Internal Server Error');
                    echo json_encode(array("status" => FALSE,"msg" => "error salarie"));
                }else{
                    echo json_encode(array("status" => TRUE));
                }
            }
        }else{
            echo json_encode(array("status" => FALSE,"msg" => "CIN vide"));
        }
        //}
    }
 
    public function ajax_delete($id)
    {
        $pid = $this->salarie_model->get($id)->personne_id;
        if (($this->salarie_model->delete($id) == 0) or ($this->personne_model->delete($pid) == 0)){ 
            header('HTTP/1.1 500 Internal Server Error');
            echo json_encode(array("status" => FALSE));
        }else  
            echo json_encode(array("status" => TRUE));
    }

    public function ajax_supp_photo($sid)
    {
        $pid = $this->salarie_model->get($sid)->personne_id;
        
        if($pid === false){
            //error personne
            echo json_encode(array("status" => FALSE,"msg" => "error personne"));
        }else{
            $this->db->select('photo')->from('personnes')->where('id', $pid);
            $del_photo = $this->db->get()->row()->photo;
            if($del_photo) {
                unlink(FCPATH . "uploads/photos/".$del_photo);
            }
            $ipid = $this->personne_model->update(array(
                'photo' => NULL,
            ), $pid);

            //echo $ipid; die();
            
            if($ipid === false){
                //error adherent
                echo json_encode(array("status" => FALSE,"msg" => "error photo"));
            }else{
                echo json_encode(array("status" => TRUE));
            }
        }
    } 
}
