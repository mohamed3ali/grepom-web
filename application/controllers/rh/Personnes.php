<?php

class Personnes extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model('personne_model');
        $this->load->model('adherent_model');
        $this->load->model('pays_model');
        $this->load->model('ville_model');
	}
	
	public function index()
	{
	    //$this->output->enable_profiler(TRUE);
	    echo var_dump($this->personnes_m->get_all());
	}

    public function ville($pid) 
    {
        $villes = $this->ville_model->where('Country',$pid)->as_dropdown('Name')->get_all();
        //var_dump($villes);
        if ($villes) echo form_dropdown('ville_id', $villes, '', 'class="form-control" id="ville_id"');
        if (!$villes) echo form_dropdown('ville_id', array("Name"=>"", "Name"=>""), '', 'class="form-control" id="ville_id"');
    }

    public function indicatif($pid) 
    {
		$pays = $this->pays_model->where('Code',$pid)->get();
        //var_dump($villes);
        echo $pays->Indicatif;
    }
    
}