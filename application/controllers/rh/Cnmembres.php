<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cnmembres extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('cnmembre_model');
        $this->load->model('reunion_model');
        $this->load->model('adherent_model');
    }

    public function index() 
    {
        //$this->output->enable_profiler(TRUE);
        //var_dump($this->cnmembre_model->mandats());die();

		$this->data['titre'] = "Conseil national";
		$this->data['entityname'] = "cnmembre";
		$this->data['ajaxurl'] = "cnmembres/json";
		$this->data['css'] = '';
		$this->data['scripts'] = '';
		$this->data['zonescripts'] = 'admin/cnmembres/dt';
		$this->data['zonecontenu'] = "admin/cnmembres/view";
		$this->data['zonemodals'] = "admin/cnmembres/modals";
        //$this->data['comboMandats'] = $this->cnmembre_model->fields('mandat')->as_dropdown('mandat')->get_all();
        $this->data['comboMandats'] = $this->cnmembre_model->mandats();
		$this->data['comboAdherents'] = $this->comboArray("adherents","id", "num_adh");
		$this->data['comboCnFonctions'] = $this->comboArray("cnfonctions","id", "cn_fonction");
		$this->data['comboReunions'] = $this->comboArray("reunions","id", "CONCAT('[', DATE_FORMAT(date_reunion,'%d/%m/%Y'), '] ', objet) as details_reunion");
		$this->load->view('admin/layout',$this->data);
    }
    
    public function jsonba() 
    {
        $list = $this->cnmembre_model
                        ->with_adherent(array(
                            'with'=>array(
                                'relation'=>'personne'
                                )
                            ))
                        ->with_cnfonction('where:`ba`=\'1\'')
                        ->get_all();
        
        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //var_dump($item);die();
                $row = array();
                $row[] = $item->mandat;
                $row[] = $item->adherent->personne->cin;
                $row[] = $item->adherent->personne->nom." ".$item->adherent->personne->prenom;
                $row[] = $item->cnfonction->cn_fonction;
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier </a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Cessation" onclick="cessation_entity('."'".$item->id."'".')">Cessation</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function jsonc() 
    {
        $list = $this->cnmembre_model
                        ->with_adherent(array(
                            'with'=>array(
                                'relation'=>'personne'
                                )
                            ))
                        //->with_cnfonction('where:`ba`=\'0\'')
                        ->with_cnfonction()
                        ->get_all();
        //print_r($list);die();
        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                $row = array();
                $row[] = $item->mandat;
                $row[] = $item->adherent->personne->cin;
                $row[] = $item->adherent->personne->nom." ".$item->adherent->personne->prenom;
                $row[] = $item->cnfonction->cn_fonction;
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier </a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Cessation" onclick="cessation_entity('."'".$item->id."'".')">Cessation</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function json() 
    {
        $list = $this->cnmembre_model->with('adherent')->with('personne')->with('cnfonction')->get_all();
        
        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //var_dump($item);die();
                $row = array();

                $row[] = $item->mandat;
                //get person info through adherent object
                $existingAdherent = $this->adherent_model->with('personne')->where('id',$item->adherent_id)->get();
                if($existingAdherent){
                    //adherent already exists
                    $row[] = $existingAdherent->num_adh;
                    $row[] = $existingAdherent->personne->cin;
                    $row[] = $existingAdherent->personne->nom.' '.$existingAdherent->personne->prenom;
                }else {
                    $row[] = "";
                    $row[] = "";
                    $row[] = "";
                }
                //$row[] = $item->personne->cin;
                //$row[] = $item->personne->nom.' '.$item->personne->prenom;
                $row[] = $item->cnfonction->cn_fonction;
                //$row[] = "";//date cn
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier </a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Cessation" onclick="cessation_entity('."'".$item->id."'".')">Cessation</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function ajax_edit($id)
    {
        $this->data = $this->cnmembre_model
                            //->with('adherent')
                            ->with_adherent(array(
                                'with'=>array(
                                    'relation'=>'personne'
                                    )
                                ))
                            ->with('cnfonction')
                            ->get($id);
        //print_r($this->data);
        echo json_encode($this->data);
    }
 
    public function ajax_add()
    {
        $imid = $this->cnmembre_model->insert(array(
            'mandat' => $this->input->post('mandat'),
            'date-cn' => $this->reunion_model->get($this->input->post('reunion_id'))->date_reunion,
            'reunion_id' => $this->input->post('reunion_id'),
            'adherent_id' => $this->input->post('adherent_id'),
            'cnfonction_id' => $this->input->post('cnfonction_id'),
        ));

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur d'ajout du membre"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Le membre a été bien créé."));
        }
    }
 
    public function ajax_update($mid)
    {            
        $imid = $this->cnmembre_model->update(array(
            'mandat' => $this->input->post('mandat'),
            'date-cn' => $this->reunion_model->get($this->input->post('reunion_id'))->date_reunion,
            'reunion_id' => $this->input->post('reunion_id'),
            'adherent_id' => $this->input->post('adherent_id'),
            'cnfonction_id' => $this->input->post('cnfonction_id'),
        ), $mid);

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "update error"));
        }else{
            echo json_encode(array("status" => TRUE));
        }
    }
 
    public function ajax_delete($id)
    {
        if ($this->cnmembre_model->delete($pid) == 0) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }
 
}
