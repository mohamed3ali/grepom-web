<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Json extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}
    
    public function test()
	{
	    $this->data = array();
	    
        //$this->db->select('*');
        //$this->db->from("cotisations");
    
        echo $this->db->count_all_results("cotisations");
        $query = $this->db->get("cotisations")->result();
        
        echo json_encode($query);
        die();
        
        $this->data["status"] = true;
        echo json_encode($this->data);
	}
    
    public function adherents()
	{
		$this->db->select('*');
        $this->db->from('adherents');
        //$this->db->join('personnes', 'personnes.cin = adherents.personne_id');
        
        $no = 0;
        foreach ($this->db->get() as $entity) {
        	//var_dump($entity);die();
            $no++;
            $row = array();
            $row[] = $entity->id;
            $row[] = $entity->numrecu;
            $row[] = $entity->num;
            //$row[] = $entity->personne_id;//CIN
            $row[] = $entity->saison;
            $row[] = $entity->cotisation_type;
            $row[] = $entity->cotisation_mode;
            $row[] = $entity->montant;
            $row[] = $entity->nom.' '.$entity->prenom;//$cotisation->receptionpar_id;
            $row[] = $entity->remarques;
 
            //add html for action
            $row[] = '
            <a href="javascript:void(0)" title="Modifier" onclick="edit_cotisation('."'".$cotisation->id."'".')" class="btn btn-warning btn-sm m-r-xs"><i class="material-icons">create</i></a>
            <a href="javascript:void(0)" title="Supprimer" onclick="delete_cotisation('."'".$cotisation->id."'".')" class="btn btn-danger btn-sm m-r-xs"><i class="material-icons">delete sweep</i></a>';

            $this->data[] = $row;
        }
 
        echo json_encode($this->db->get());
	}
	
	public function comboarray($tab, $key = "id", $val = null)
	{
		$rows = R::getAssoc( "SELECT $key, $val FROM $tab" );
		echo  "SELECT $key, $val FROM $tab<hr>";
		print_r($rows);
		
		/*$rows = R::find($tab);
		if ( !count( $rows ) ) return "Table is empty!" ;
		foreach( $rows as $row ) {
			//echo "* #{$row->id}: {$row->name}\n";
			foreach( $row as $k=>$v ) {
				echo "$k => $row[$k]\t";
			}
			echo "<hr>\n";
		}*/
	    /*$res = array();
	    
	    if($tab){
            if ($this->db->count_all_results($tab)>0){ 
        		if($key && $val){ $this->db->select($key .', '. $val); }
        		else{ $this->db->select('*'); }
        		$results = $this->db->get($tab)->result();
        		
                $res[] = $this->db->get($tab)->result();
            }else{
            }
		}else{
		}
		
        return $res;*/
	}
	
    public function combo($tab = null, $val = null, $txt = null)
	{
	    $this->data = array();
	    
	    if($tab){
            //$this->db->from($tab);
            //die($this->db->count_all_results($tab));
            
            if ($this->db->count_all_results($tab)>0){ 
        		if($val && $txt){ $this->db->select($val .', '. $txt); }
        		else{ $this->db->select('*'); }
        		
                $this->data["entities"] = json_encode($this->db->get($tab)->result());
                $this->data["status"] = true;
            }else{
                $this->data["status"] = false;
            }
		}else{
            $this->data["status"] = false;
		}
		
        echo json_encode($this->data);
	}
}