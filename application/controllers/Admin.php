<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model('cotisation_model');
	}
	
	function dashboard()
	{
		$this->data['zonecontenu'] = "admin/home";
		$this->data['mod'] = $this->session->userdata;
		$this->data['cotisationStats'] = $this->cotisation_model->totalCotisations();
		$this->data['adherantCountStats'] = $this->cotisation_model->adherantCount();
		$this->data['adherantsProStats'] = $this->cotisation_model->adherantsPro();
		$this->data['js_files'] = [
			base_url().'assets/vendor/Chart.js/dist/Chart.js',
		];
		$this->data['zonescripts'] = 'admin/dashboard/charts';
		
		//print_r($this->data['adherantCountStats']);die();
		$this->load->view('admin/layout', $this->data);
	}

	function profile($action = 'list', $id = null)
	{
		if($this->moderateurs_model->isLoggedIn()):
			$this->load->model('moderateurs_model');

				//$this->form_validation->set_rules('login','Login','required');
				$this->form_validation->set_rules('email','Email','required');
				//$this->form_validation->set_rules('psw','Mot de passe','required');
			
				if($this->form_validation->run()){
			
					if ($this->input->post('psw'))
					{
						$this->data = array(
							'login' => $this->input->post('login'),
							'email' => $this->input->post('email'),
							'psw' => sha1($this->input->post('psw')),
						);
					}else{
						$this->data = array(
							'login' => $this->input->post('login'),
							'email' => $this->input->post('email'),
						);
					}
			
					//enregistrement dans la base de données
					if($this->moderateurs_model->update($this->session->userdata['m_id'],$this->data)){
						
						$session_data = array('m_login' => $this->input->post('login'),$this->session->userdata['m_id'],'m_email' => $this->input->post('email'),'m_logged_in' => true);
						$this->session->set_userdata($session_data);
                        redirect('admin/profile');
					}
			
				} else {
					//certains champs obligatoires sont manquants
					$this->data['mod'] = $this->session->userdata;
					$this->data['zonecontenu'] = "profile";
					$this->load->view('template/tmpl-admin', $this->data);
				}

		else:
			redirect('admin/login');
		endif;
	}	

	/*
	function login()
	{
		//die("login");
		if($this->moderateurs_model->isLoggedIn()){
		  redirect('admin','refresh');
		} else {
		  //on charge la validation de formulaires
		  $this->load->library('form_validation');
		
		  //on définit les règles de succès
		  $this->form_validation->set_rules('login','Login','required');
		  $this->form_validation->set_rules('psw','Mot de passe','required');
		
		  //si la validation a échouée on redirige vers le formulaire de login
		  if(!$this->form_validation->run()){
			   $this->load->view('admin/auth/login');
		  } else {
		       $login = $this->input->post('login');
		       $psw = $this->input->post('psw');
		       $validCredentials = $this->moderateurs_model->validCredentials($login,$psw); 
		
		       if($validCredentials){
		       		$this->data['moderateur'] = $this->session->userdata('data');
		       		//die($this->input->get('location'));
		       		if($this->input->get('location')){
		            	redirect($this->input->get('location'),'refresh');
		       		} else {
		       			redirect('admin/dashboard','refresh');
		       		}
		       } else {
		            $this->data['error_credentials'] = 'Vérifiez votre Login / mot de passe';
		            //$this->load->view('loginform',$this->data);
		            $this->load->view('admin/auth/login',$this->data);
		       }
		  }
		}
	} 
	function logout()
	{
		if($this->moderateurs_model->isLoggedIn()){
			$session_data = array('m_login' => NULL,'m_logged_in' => FALSE);
			$this->session->set_userdata($session_data);
		}
		redirect('admin/login','refresh');
	} 
	
	function page($action = 'list', $id = null)
	{
		if($this->moderateurs_model->isLoggedIn()):
			$this->load->model('pages_model');

			switch($action){

				case 'lister': //affichage de la liste des pages
					$this->data['listePages'] = $this->pages_model->listerParent(0);
					//$this->load->view('pages-liste',$this->data);
					$this->data['mod'] = $this->session->userdata;
					$this->data['zonecontenu'] = "pages-liste";
					$this->load->view('template/tmpl-admin', $this->data);
				break;
				case 'filles': //affichage de la liste des pages
					$this->data['pageParent'] = $this->pages_model->afficher($id);
					$this->data['listePages'] = $this->pages_model->listerParent($id);
					//$this->load->view('pages-liste',$this->data);
					$this->data['mod'] = $this->session->userdata;
					$this->data['zonecontenu'] = "pages-filles";
					$this->load->view('template/tmpl-admin', $this->data);
				break;

				case 'editer': //affichage du formulaire de modification d'une page
					if(isset($id) && $id != 0){
							
						$this->data['page'] = $this->pages_model->afficher($id);
						$this->data['listePages'] = $this->pages_model->listerParent(0);
						//$this->load->view('pages-edit',$this->data);
						$this->data['mod'] = $this->session->userdata;
						$this->data['zonecontenu'] = "pages-edit";
						$this->load->view('template/tmpl-admin', $this->data);
					}
				break;

				case 'modifier':  //mise a jour de la page

					if(isset($id) && $id != 0){
						//définition des règles de validation
						$this->load->library('form_validation');
				
						$this->form_validation->set_rules('titre_page','Titre','required');
						$this->form_validation->set_rules('contenu_page','Contenu','required');
						
				
						if($this->form_validation->run()){
				
							$this->data = array(
								'titre' => $this->input->post('titre_page'),
								'contenu' => $this->input->post('contenu_page'),
								'parent' => $this->input->post('parent_page'),
							);
							//enregistrement dans la base de données
							if($this->pages_model->modifier($id,$this->data)){
								$session_msg = array('class' => 'success','message' => 'Page mise à jour');
	          					$this->session->set_userdata($session_msg);
	          					
								redirect('admin/page/lister');
								//echo "Page mise à jour";
							}
				
						} else {
							//certains champs obligatoires sont manquants
							redirect('admin/page/editer/'.$id);
						}
					}
				break;

				case 'ajouter': //ajout d'une page
					$this->form_validation->set_rules('titre_page','Titre','required');
					$this->form_validation->set_rules('contenu_page','Contenu','required');
				
					if($this->form_validation->run()){
				
						$this->data = array(
							'titre' => $this->input->post('titre_page'),
							'contenu' => $this->input->post('contenu_page'),
						);
				
						//enregistrement dans la base de données
						if($this->pages_model->ajouter($this->data)){
                                                    redirect('admin/page/lister');
                                                    //echo "page ajouté";
						}
				
					} else {
						//certains champs obligatoires sont manquants
						$this->data['listePages'] = $this->pages_model->listerParent(0);
						$this->data['mod'] = $this->session->userdata;
						$this->data['zonecontenu'] = "pages-ajout";
						$this->load->view('template/tmpl-admin', $this->data);
					}
				break;

				case 'supprimer':
					if(isset($id) && $id != 0){
						if($this->pages_model->supprimer($id)){
                                                    redirect('admin/page/lister');
                                                    //echo "page supprimée";
						}
					}
				break;
			}

		else:
			redirect('admin/login');
		endif;
	}	
	*/

        
	
}