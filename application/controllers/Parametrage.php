<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Parametrage extends MY_Controller {
    
    public $adh;
 
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('cotisation_model');
        $this->load->model('annee_model');
		//$this->load->view('admin/cotisations/view','cotisations');
    }

    public function index()
    {
        $this->data['titre'] = "Projets";
        $this->data['entityname'] = "projet";
        $this->data['ng_controller'] = "ProjetsCtrl";
        $this->data['js_files'] = array(
                '/assets/scripts/angular.min.js',
                '/assets/scripts/app.js'
            );

        $this->data['zonecontenu'] = "admin/parametrage/view";

        $this->load->view('admin/layout-ng',$this->data);
    }

    public function typespersonne()
    {
        $this->data['titre'] = "Types personnes";
        $this->data['entityname'] = "typepersonne";
        $this->data['ng_controller'] = "TypePersonneCtrl";
        $this->data['js_files'] = array(
                '/assets/scripts/angular.min.js',
                '/assets/scripts/app.js'
            );

        $this->data['zonecontenu'] = "admin/parametrage/view";

        $this->load->view('admin/layout-ng',$this->data);
    }
 
    public function json()
    {
        $list = $this->cotisation_model
                        ->order_by('annee_id', 'desc')
                        ->with_adherent(array(
                            'with'=>array(
                                'relation'=>'personne'
                                )
                            ))
                        /*->with('adherent')*/
                        ->with('personne')
                        ->with('cotisationtype')
                        ->with('cotisationmode')
                        ->with('annee')
                        ->get_all();

        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
            	//var_dump($item);die();
                $no++;
                $row = array();
                //$row[] = $item->id;
                $row[] = $item->numrecu;
                $row[] = "".$item->adherent->num_adh." / ".$item->adherent->personne->nom." ".$item->adherent->personne->prenom;
                $row[] = $item->annee_id;
                $row[] = $item->cotisationtype->cotisation_type;
                $row[] = $item->cotisationmode->cotisation_mode;
                $row[] = $item->montant;
                $row[] = (isset($item->personne)) ? $item->personne->nom.' '.$item->personne->prenom : '' ;
                //$row[] = $item->personne->nom.' '.$item->personne->prenom;//$cotisation->receptionpar_id;
                //$row[] = $item->remarques;
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier </a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $this->data = $this->cotisation_model
                        ->with('adherent')
                        ->with('personne')
                        ->with('cotisationtype')
                        ->with('cotisationmode')
                        ->get($id);
        echo json_encode($this->data);
    }
 
    public function nouvelle_annee($annee=null)
    {
        if ($annee) {
            $data = array(
                    'annee' => $annee
                );
            $inserted = $this->annee_model->insert($data);

        } else {
        }
        $annees = $this->annee_model->order_by("annee", "desc")->as_dropdown('annee')->get_all();
        //var_dump($villes);
        echo form_dropdown('annee_id', $annees, '', 'class="form-control" id="annee_id"');
    }
    public function ajax_add()
    {
        $this->data = array(
                'adherent_id' => $this->input->post('adherent_id'),
                'montant' => $this->input->post('montant'),
                'annee_id' => $this->input->post('annee_id'),
                'date_cotisation' => $this->input->post('date_cotisation'),
                'numrecu' => $this->input->post('numrecu'),
                'cotisationmode_id' => $this->input->post('cotisationmode_id'),
                'cotisationtype_id' => $this->input->post('cotisationtype_id'),
                'receptionpar_id' => $this->input->post('receptionpar_id'),
                'remarques' => $this->input->post('remarques'),
            );
        $insert = $this->cotisation_model->insert($this->data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_update($id)
    {
        
        $this->data = array(
                'adherent_id' => $this->input->post('adherent_id'),
                'montant' => $this->input->post('montant'),
                'annee_id' => $this->input->post('annee_id'),
                'date_cotisation' => $this->input->post('date_cotisation'),
                'numrecu' => $this->input->post('numrecu'),
                'cotisationmode_id' => $this->input->post('cotisationmode_id'),
                'cotisationtype_id' => $this->input->post('cotisationtype_id'),
                'receptionpar_id' => $this->input->post('receptionpar_id'),
                'remarques' => $this->input->post('remarques'),
            );
        $this->cotisation_model->update($this->data, $id);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_delete($id)
    {
        $this->cotisation_model->delete($id);
        echo json_encode(array("status" => TRUE));
    }
 
}