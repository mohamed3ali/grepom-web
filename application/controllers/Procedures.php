<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Procedures extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('procedure_model');
        $this->load->model('chapitre_model');
    }

    public function index() 
    {
    		$this->data['titre'] = "Gestion des procédures";
    		$this->data['entityname'] = "procedure";
    		$this->data['ajaxurl'] = "procedures/json";
    		$this->data['css'] = '';
    		$this->data['scripts'] = '';
    		$this->data['zonescripts'] = 'admin/procedures/dt';
    		$this->data['zonecontenu'] = "admin/procedures/view";
    		$this->data['zonemodals'] = "admin/procedures/modals";
    		$this->data['comboProcedures'] = $this->comboArray("procedures","id", "nom_procedure");
    		$this->load->view('admin/layout',$this->data);
    }
    public function details($id = null) 
    {
        if($id){
    		$this->data['titre'] = "Gestion procédures";
    		$this->data['zonescripts'] = 'admin/procedures/dt';
    		$this->data['zonecontenu'] = "admin/procedures/paragraphes";
    		$this->data['zonemodals'] = "admin/procedures/modals";
    		$this->data['pid'] = $id;
    		$this->data['baseurl'] = "/procedures";
    		
    		//$list = $this->procedure_model->with_chapitres()->get($id);
    		//var_dump($list);die();
    		$this->data['procedure'] = $this->procedure_model->with_chapitres()->get($id);
    		
    		$this->load->view('admin/layout',$this->data);
        } else {
            $this->index();
        }
    }
    
    public function json() 
    {
        $list = $this->procedure_model->with('chapitre')->get_all();
        
        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //var_dump($item); die();
                $row = array();
                $row[] = $item->nom_procedure;
                $row[] = $item->description_procedure;
                $row[] = '
                <a title="Détails" href="/procedures/details/'.$item->id.'" class="btn btn-warning btn-sm m-r-xs"><i class="material-icons">create</i></a>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function ajax_edit($id)
    {
        $data = $this->chapitre_model->get($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
        $pid = $this->chapitre_model->insert(array(
            'titre' => $this->input->post('titre'),
            'contenu' => $this->input->post('contenu'),
            'procedure_id' => $this->input->post('procedure_id'),
        ));

        if($pid === false){
            //error personne
            echo json_encode(array("status" => FALSE,"msg" => "insertion error"));
        }else{
            echo json_encode(array("status" => TRUE));
        }
    }
 
    public function ajax_update($id)
    {
            $ipid = $this->chapitre_model->update(array(
            'titre' => $this->input->post('titre'),
            'contenu' => $this->input->post('contenu'),
            'procedure_id' => $this->input->post('procedure_id'),
            ), $id);

            //echo var_dump($this->input->post()); die();
            if($ipid === false){
                //error chapitre
                echo json_encode(array("status" => FALSE,"msg" => "error chapitre"));
            }else{
                echo json_encode(array("status" => TRUE));
            }
    }
 
    public function ajax_delete($id)
    {
        if ($this->chapitre_model->delete($id) == 0) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }
 
}
