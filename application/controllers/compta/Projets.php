<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Projets extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('projet_model');
        $this->load->model('fondsbailleur_model');
    }

    public function index() 
    {
		$this->data['titre'] = "Comptabilité / Projets";
		$this->data['entityname'] = "projet";
		$this->data['ajaxurl'] = "projets/json";
		$this->data['css'] = '';
		$this->data['scripts'] = '';
		$this->data['zonescripts'] = 'admin/compta/projets/dt';
		$this->data['zonecontenu'] = "admin/compta/projets/view";
		$this->data['zonemodals'] = "admin/compta/projets/modals";
		$this->data['comboFondbailleurs'] = $this->comboArray("fondsbailleurs","id", "CONCAT( code, ' / ', designation ) AS bf");

		$this->load->view('admin/layout',$this->data);
    }
    
    public function json() 
    {
        $list = $this->projet_model
                        ->with('fondsbailleur')
                        ->get_all();
        
        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //var_dump($item);die();
                $row = array();

                $row[] = $item->titre;
                $row[] = $item->code;
                $row[] = isset($item->bf_id)?$item->fondsbailleur->designation." (".$item->fondsbailleur->code.")":" ";
                $row[] = $item->description;
                $row[] = $item->date_proposition;
                $row[] = $item->date_acceptation;
                
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier</a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function ajax_edit($id)
    {
        $this->data = $this->projet_model
                            ->get($id);
        //print_r($this->data);
        echo json_encode($this->data);
    }
 
    public function ajax_add()
    {
        $imid = $this->projet_model->insert(array(
            'titre' => $this->input->post('titre'),
            'code' => $this->input->post('code'),
            'bf_id' => $this->input->post('bf_id'),
            'description' => $this->input->post('description'),
            'date_proposition' => $this->input->post('date_proposition'),
            'date_acceptation' => $this->input->post('date_acceptation')
        ));

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur d'ajout"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Le membre a été bien créé."));
        }
    }
 
    public function ajax_update($mid)
    {
        $imid = $this->projet_model->update(array(
            'titre' => $this->input->post('titre'),
            'code' => $this->input->post('code'),
            'bf_id' => $this->input->post('bf_id'),
            'description' => $this->input->post('description'),
            'date_proposition' => $this->input->post('date_proposition'),
            'date_acceptation' => $this->input->post('date_acceptation')
        ), $mid);

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "update error"));
        }else{
            echo json_encode(array("status" => TRUE));
        }
    }
 
    public function ajax_delete($id)
    {
        if ($this->projet_model->delete($id) == 0) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }
 
}
