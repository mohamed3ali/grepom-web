<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Donrecettes extends MY_Controller {
    
    public $adh;
 
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('recette_model');
        $this->load->model('annee_model');
		//$this->load->view('admin/donrecettes/view','donrecettes');
    }

    public function index()
    {
		$this->data['titre'] = "Comptabilité / Recettes Dons";
		$this->data['entityname'] = "donrecette";
		$this->data['ajaxurl'] = "donrecettes/json";
		$this->data['css'] = '';
		$this->data['scripts'] = '';
		$this->data['zonescripts'] = 'admin/compta/donrecettes/dt';
		$this->data['zonecontenu'] = "admin/compta/donrecettes/view";
		$this->data['zonemodals'] = "admin/compta/donrecettes/modals";
		$this->data['comboModesPaiement'] = $this->comboArray("modepaiements","id", "designation");
        $this->data['comboDevises'] = $this->comboArray("devises","id", "code");
        $this->data['comboFondbailleurs'] = $this->comboArray("fondsbailleurs","id", "CONCAT( code, ' / ', designation ) AS bf");
        $this->data['comboDonateurs'] = $this->comboArray("donateurs","id", "CONCAT( code, ' / ', donateur ) AS donateur");

		$this->data['comboAdherents'] = $this->comboArray("adherents","id", "num_adh");
        $this->data['comboPersonnes'] = $this->comboArray("personnes","id", "CONCAT( nom, ' ', prenom ) AS nomprenom");

        //var_dump($totalsDonrecettes);die();
		$this->data['statsPanelContent'] = '
            <div id="slider-donrecettes-bilan" class="carousel slide t-a-c" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
        ';
        $i=0;
        //$totalsDonrecettes = $this->recette_model->totalDonrecettes();
        //var_dump($totalsDonrecettes);die();
        //foreach ($totalsDonrecettes as $tc) {
        //    if ($i<1) {
        //    $this->data['statsPanelContent'] .= "
        //          <div class='carousel-item active'>
        //                Total des donrecettes en ".$tc->annee_id." : <strong>".$tc->montant." Dh</strong>
        //        </div>";
        //    } else {
        //    $this->data['statsPanelContent'] .= "
        //          <div class='carousel-item'>
        //                Total des donrecettes en ".$tc->annee_id." : <strong>".$tc->montant."</strong>
        //        </div>";
        //    }
        //    
        //    $i++;
        //}
                
        $this->data['statsPanelContent'] .= '
              </div>
              <a class="left carousel-control" href="#slider-donrecettes-bilan" role="button" data-slide="prev">
                <span class="icon-prev" aria-hidden="true"></span>
                <span class="sr-only"><<</span>
              </a>
              <a class="right carousel-control" href="#slider-donrecettes-bilan" role="button" data-slide="next">
                <span class="icon-next" aria-hidden="true"></span>
                <span class="sr-only">>></span>
              </a>
            </div>
        ';
		$this->load->view('admin/layout',$this->data);
    }
    
    public function adh($id)
    {
        $this->adh = $id;
        $this->session->set_userdata(array('popup-id'  => $id));
        redirect('/donrecettes', 'refresh');
    }

    public function json()
    {
        $list = $this->recette_model
                        ->where('type','don')
                        //->order_by('annee_id', 'desc')
                        /*->with('adherent')*/
                        //->with('personne')
                        //->with('donrecettetype')
                        ->with('modepaiement')
                        ->with('devise')
                        ->with('donateur')
                        ->with('fondsbailleur')
                        ->get_all();

        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
            	//var_dump($item);die();
                $no++;
                $row = array();
                if($item->source_type == "donateurs"){
                    $row[] = $item->donateur->donateur." (".$item->donateur->code.")";
                }elseif($item->source_type == "fondsbailleurs"){
                    $row[] = $item->fondsbailleur->designation." (".$item->fondsbailleur->code.")";
                }else{
                    $row[] = " ";
                }
                $row[] = $item->montant." ".$item->devise->code;
                $row[] = $item->date_virement;
                $row[] = $item->date_reception;
                $row[] = isset($item->modepaiement)?$item->modepaiement->designation:"";

                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier </a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $this->data = $this->recette_model
                        //->with('adherent')
                        //->with('personne')
                        ->with('devise')
                        ->with('modepaiement')
                        ->with('donateur')
                        ->get($id);
        echo json_encode($this->data);
    }
 
    public function nouvelle_annee($annee=null)
    {
        if ($annee) {
            $data = array(
                    'annee' => $annee
                );
            $inserted = $this->annee_model->insert($data);

        } else {
        }
        $annees = $this->annee_model->order_by("annee", "desc")->as_dropdown('annee')->get_all();
        //var_dump($villes);
        echo form_dropdown('annee_id', $annees, '', 'class="form-control" id="annee_id"');
    }

    public function ajax_add()
    {
        //echo json_encode($this->input->post('type_id_'.$this->input->post('source_type'))); return;
        $this->data = array(
                'type' => 'don',
                'source_type' => $this->input->post('source_type'),
                'type_id' => $this->input->post('type_id_'.$this->input->post('source_type')),
                'montant' => $this->input->post('montant'),
                'devise_id' => $this->input->post('devise_id'),
                'date_virement' => $this->input->post('date_virement'),
                'modepaiement_id' => $this->input->post('modepaiement_id'),
                'date_reception' => $this->input->post('date_reception')
            );
        $insert = $this->recette_model->insert($this->data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update($id)
    {
        
        $this->data = array(
                'source_type' => $this->input->post('source_type'),
                'type_id' => $this->input->post('type_id_'.$this->input->post('source_type')),
                'montant' => $this->input->post('montant'),
                'devise_id' => $this->input->post('devise_id'),
                'date_virement' => $this->input->post('date_virement'),
                'modepaiement_id' => $this->input->post('modepaiement_id'),
                'date_reception' => $this->input->post('date_reception')
            );
        $this->recette_model->update($this->data, $id);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_delete($id)
    {
        $this->recette_model->delete($id);
        echo json_encode(array("status" => TRUE));
    }

}