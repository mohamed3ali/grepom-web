<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Commandebons extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('commandebon_model');
    }

    public function index() 
    {
		$this->data['titre'] = "Comptabilité / Bons de commande";
		$this->data['entityname'] = "commandebon";
		$this->data['ajaxurl'] = "commandebons/json";
		$this->data['css'] = '';
		$this->data['scripts'] = '';
        $this->data['comboCommandes'] = $this->comboArray("commandes","id", "numCommande");
		$this->data['zonescripts'] = 'admin/compta/commandebons/dt';
		$this->data['zonecontenu'] = "admin/compta/commandebons/view";
		$this->data['zonemodals'] = "admin/compta/commandebons/modals";
		$this->load->view('admin/layout',$this->data);
    }
     
    public function json() 
    {
        $list = $this->commandebon_model
                        ->with('commande')
                        ->get_all();
        //var_dump($list);die();

        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //var_dump($item);die();
                $row = array();

                $row[] = $item->designation;
                $row[] = isset($item->commande)?$item->commande->numCommande:"";
                $row[] = $item->prixU;
                $row[] = $item->quantite;
                $row[] = $item->prixT;
                
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier</a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function ajax_edit($id)
    {
        $this->data = $this->commandebon_model
                            ->get($id);
        //print_r($this->data);
        echo json_encode($this->data);
    }
 
    public function ajax_add()
    {
        $imid = $this->commandebon_model->insert(array(
            'designation' => $this->input->post('designation'),
            'commande_id' => $this->input->post('commande_id'),
            'prixU' => $this->input->post('prixU'),
            'quantite' => $this->input->post('quantite'),
            'prixT' => $this->input->post('prixT')
        ));

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur d'enregistrement !"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Enregistrement réussi."));
        }
    }
 
    public function ajax_update($mid)
    {
        $imid = $this->commandebon_model->update(array(
            'designation' => $this->input->post('designation'),
            'commande_id' => $this->input->post('commande_id'),
            'prixU' => $this->input->post('prixU'),
            'quantite' => $this->input->post('quantite'),
            'prixT' => $this->input->post('prixT')
        ), $mid);

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur de mise à jour !"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Mise à jour réussi."));
        }
    }
 
    public function ajax_delete($id)
    {
        if ($this->commandebon_model->delete($id) == 0) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }
 
}
