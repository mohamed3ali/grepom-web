<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Commandes extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('commande_model');
    }

    public function index() 
    {
		$this->data['titre'] = "Comptabilité / Commandes";
		$this->data['entityname'] = "commande";
		$this->data['ajaxurl'] = "commandes/json";
		$this->data['css'] = '';
		$this->data['scripts'] = '';
		$this->data['zonescripts'] = 'admin/compta/commandes/dt';
		$this->data['zonecontenu'] = "admin/compta/commandes/view";
		$this->data['zonemodals'] = "admin/compta/commandes/modals";
		$this->load->view('admin/layout',$this->data);
    }
     
    public function json() 
    {
        $list = $this->commande_model->get_all();
        //var_dump($list);die();

        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //var_dump($item);die();
                $row = array();

                $row[] = $item->client;
                $row[] = $item->fournisseur;
                $row[] = $item->numCommande;
                $row[] = $item->date_arrivee;
                $row[] = $item->description;
                
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier</a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function ajax_edit($id)
    {
        $this->data = $this->commande_model
                            ->get($id);
        //print_r($this->data);
        echo json_encode($this->data);
    }
 
    public function ajax_add()
    {
        $imid = $this->commande_model->insert(array(
            'client' => $this->input->post('client'),
            'fournisseur' => $this->input->post('fournisseur'),
            'numCommande' => $this->input->post('numCommande'),
            'date_arrivee' => $this->input->post('date_arrivee'),
            'description' => $this->input->post('description')
        ));

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur d'enregistrement !"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Enregistrement réussi."));
        }
    }
 
    public function ajax_update($mid)
    {
        $imid = $this->commande_model->update(array(
            'client' => $this->input->post('client'),
            'fournisseur' => $this->input->post('fournisseur'),
            'numCommande' => $this->input->post('numCommande'),
            'date_arrivee' => $this->input->post('date_arrivee'),
            'description' => $this->input->post('description')
        ), $mid);

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur de mise à jour !"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Mise à jour réussi."));
        }
    }
 
    public function ajax_delete($id)
    {
        if ($this->commande_model->delete($id) == 0) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }
 
}
