<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Commandes extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('commande_model');
        $this->load->model('lignecommande_model');
    }

    public function index() 
    {
		$this->data['titre'] = "Comptabilité / Commandes";
		$this->data['entityname'] = "commande";
		$this->data['ajaxurl'] = "commandes/json";
		$this->data['css'] = '';
		$this->data['scripts'] = '';
		$this->data['zonescripts'] = 'admin/compta/commandes/dt';
		$this->data['zonecontenu'] = "admin/compta/commandes/view";
		$this->data['zonemodals'] = "admin/compta/commandes/modals";
		$this->load->view('admin/layout',$this->data);
    }

    public function json() 
    {
        $list = $this->commande_model->get_all();
        //var_dump($list);die();

        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //var_dump($item);die();
                $row = array();

                $row[] = $item->fournisseur;
                //$row[] = $item->fournisseur_adr;
                $row[] = $item->num_commande;
                $row[] = $item->objet;
                $row[] = date('d/m/Y', strtotime($item->date_commande));
                $row[] = number_format($this->commande_model->get_grand_total($item->id)[0]->grand_total * (1 + ($item->tva/100)), 2, ',', ' ');//total;//1 234,56
                $row[] = $item->id_facture;
                $row[] = $item->id_bl;
                
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" target="_blank" href="'.base_url().'compta/commandes/html_cmd/'.$item->id.'" title="Modifier">Imprimer la commande</a>
                        <a class="dropdown-item" target="_blank" href="'.base_url().'compta/commandes/html_fact/'.$item->id.'" title="Modifier">Imprimer la facture</a>
                        <a class="dropdown-item" target="_blank" href="'.base_url().'compta/commandes/html_bl/'.$item->id.'" title="Modifier">Imprimer BL</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier</a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }

    public function ajax_edit($id)
    {
        $this->data = $this->commande_model
                            ->with_lignescommande()
                            ->get($id);
        //print_r($this->data);
        echo json_encode($this->data);
    }

    public function ajax_add0()
    {
        $imid = $this->commande_model->insert(array(
            'client' => $this->input->post('client'),
            'fournisseur' => $this->input->post('fournisseur'),
            'num_commande' => $this->input->post('num_commande'),
            'date_arrivee' => $this->input->post('date_arrivee'),
            'description' => $this->input->post('description')
        ));

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur d'enregistrement !"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Enregistrement réussi."));
        }
    }

    public function ajax_add()
    {
        //echo '<pre>';
        ////print_r($this->input->post());
        ////print_r($_FILES);
        ////print_r($_POST);
        //print_r(count($_POST['l_designation']));
        ////print_r($this->input->post('l_designation'));
        //echo '</pre>';
        //die();
        //if (isset($_POST['cin']) && $_POST['cin']) {

            $data['config'] = array(
                'file_name' => 'devis-',//strtolower($this->input->post('cin')),
                'upload_path' => FCPATH . 'uploads/devis/',
                'allowed_types' => 'pdf|doc|docx|xsl|xslx|jpg|jpeg|png|gif',
                'max_size' => '4048',
                'multi' => 'all'
            );
            //if (isset($_FILES['devis1']['name']) && $_FILES['devis1']['name']) {

            //    $this->load->library('upload', $data['config']);
            //    //$this->upload->initialize($config);

            //    if ($this->upload->do_upload('devis1'))
            //    {
            //        $data['uploads'] = $this->upload->data();
            //    }
            //    else
            //    {
            //        $data['uploads']['file_name'] = null;
            //        return $this->upload->display_errors();
            //    }
            //    
            //    if ($this->upload->do_upload('devis2'))
            //    {
            //        $data['uploads'] = $this->upload->data();
            //    }
            //    else
            //    {
            //        $data['uploads']['file_name'] = null;
            //        return $this->upload->display_errors();
            //    }
            //    
            //    if ($this->upload->do_upload('devis3'))
            //    {
            //        $data['uploads'] = $this->upload->data();
            //    }
            //    else
            //    {
            //        $data['uploads']['file_name'] = null;
            //        return $this->upload->display_errors();
            //    }
//
            //}else{
            //    //echo json_encode(array("status" => FALSE,"msg" => "Devis vide"));
            //}

            $cmd_id = $this->commande_model->insert(array(
                //'' => $data['uploads']['file_name'],
                'fournisseur' => $this->input->post('fournisseur'),
                'fournisseur_adr' => $this->input->post('fournisseur_adr'),
                'num_commande' => $this->input->post('num_commande'),
                'objet' => $this->input->post('objet'),
                'date_commande' => $this->input->post('date_commande'),
                'tva' => $this->input->post('tva'),
                'id_facture' => $this->input->post('id_facture'),
                'id_bl' => $this->input->post('id_bl'),
            ));
                //echo $pid; die();
            if($cmd_id === false){
                //error
                //unlink($data['full_path']);
                header('HTTP/1.1 500 Internal Server Error');
                echo json_encode(array("status" => FALSE,"msg" => "insert error"));
            }else{
                for ($i = 0; $i < count($_POST['l_designation']); $i++) {
                    $l_id[] = $this->lignecommande_model->insert(array(
                        'commande_id' => $cmd_id,
                        'designation' => $_POST['l_designation'][$i],
                        'qte' => $_POST['l_qte'][$i],
                        'pu' => $_POST['l_pu'][$i],
                    ));
                }
                //echo var_dump($this->input->post()); die();
                if(count($l_id)){
                    echo json_encode(array("status" => TRUE));
                }else{
                    //error, rollback
                    $this->commande_model->delete($cmd_id);
                    header('HTTP/1.1 500 Internal Server Error');
                    echo json_encode(array("status" => FALSE,"msg" => "error"));
                }
            }
        //}else{
        //    header('HTTP/1.1 500 Internal Server Error');
        //    echo json_encode(array("status" => FALSE,"msg" => "CIN vide"));
        //}
    }

    public function ajax_update($cmd_id)
    {
        
        //$db_ids = null;
        //foreach ($this->db->from('lignes_commande')->where('commande_id', $cmd_id)->get()->result() as $field)
        //{
        //    $db_ids[] = $field->id;
        //    if (in_array($field->id, $_POST['l_id'])) {
        //        // UPDATES
        //        echo "to update: ". // ." | ".$_POST['l_designation'][array_search($field->id, $_POST['l_id'])]."\n";
        //    }else{
        //        // DELETES
        //        echo "To delete: ".$field->id." | ".$field->designation."\n";
        //    }
        //}
        //// INSERTS
        //foreach (array_keys($_POST['l_id'], 0) as $ligne) {
        //    echo "\nTo Insert: ".$_POST['l_designation'][$ligne];
        //}
        //
        //die();
        
        $icmd_id = $this->commande_model->update(array(
            //'' => $data['uploads']['file_name'],
            'fournisseur' => $this->input->post('fournisseur'),
            'fournisseur_adr' => $this->input->post('fournisseur_adr'),
            'num_commande' => $this->input->post('num_commande'),
            'objet' => $this->input->post('objet'),
            'date_commande' => $this->input->post('date_commande'),
            'tva' => $this->input->post('tva'),
            'id_facture' => $this->input->post('id_facture'),
            'id_bl' => $this->input->post('id_bl'),
        ), $cmd_id);

        //if($icmd_id === false){
        //    echo json_encode(array("status" => FALSE,"msg" => "Erreur de mise à jour !"));
        //}else{
        //    echo json_encode(array("status" => TRUE,"msg" => "Mise à jour réussi."));
        //}
    
            //echo $pid; die();
        if($icmd_id === false){
            //error
            //unlink($data['full_path']);
            header('HTTP/1.1 500 Internal Server Error');
            echo json_encode(array("status" => FALSE,"msg" => "update error"));
        }else{
            foreach ($this->db->from('lignes_commande')->where('commande_id', $cmd_id)->get()->result() as $field)
            {
                $db_ids[] = $field->id;
                if (in_array($field->id, $_POST['l_id'])) {
                    
                    // UPDATES
                    $this->lignecommande_model->update(array(
                        //'commande_id' => $cmd_id, //pas besoin de mettre à jour
                        'designation' => $_POST['l_designation'][array_search($field->id, $_POST['l_id'])],
                        'qte' => $_POST['l_qte'][array_search($field->id, $_POST['l_id'])],
                        'pu' => $_POST['l_pu'][array_search($field->id, $_POST['l_id'])],
                    ), $_POST['l_id'][array_search($field->id, $_POST['l_id'])]);
                    //), $cmd_id);
                }else{
                    
                    // DELETES
                    $this->lignecommande_model->delete($field->id);
                }
            }
            
            // INSERTS
            foreach (array_keys($_POST['l_id'], 0) as $ligne) {
                $this->lignecommande_model->insert(array(
                    'commande_id' => $cmd_id,
                    'designation' => $_POST['l_designation'][$ligne],
                    'qte' => $_POST['l_qte'][$ligne],
                    'pu' => $_POST['l_pu'][$ligne],
                ));
            }
            echo json_encode(array("status" => TRUE,"msg" => "Mise à jour réussi."));
        }
    }

    public function ajax_delete($id)
    {
        $this->db->delete('lignes_commande', array('commande_id' => $id));
        if ($this->commande_model->delete($id) == 0) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }

    public function html_cmd($id)
    {
        $this->data["commande"] = $this->commande_model
                           ->with_lignescommande()
                           ->get($id);
        //print_r($this->data);
        //echo json_encode($this->data);
		$this->load->view('admin/prints/commande',$this->data);
    }
    public function html_fact($id)
    {
        $this->data["commande"] = $this->commande_model
                           ->with_lignescommande()
                           ->get($id);
        //print_r($this->data);
        //echo json_encode($this->data);
		$this->load->view('admin/prints/facture',$this->data);
    }
    public function html_bl($id)
    {
        $this->data["commande"] = $this->commande_model
                           ->with_lignescommande()
                           ->get($id);
        //print_r($this->data);
        //echo json_encode($this->data);
		$this->load->view('admin/prints/bl',$this->data);
    }
    
    public function print_cmd($id)
    {
        $this->data = $this->commande_model
                           ->with_lignescommande()
                           ->get($id);
        //print_r($this->data);
        //echo json_encode($this->data);
		
		
		$filename = time()."_order.pdf";
        $html = $this->load->view('admin/prints/commande',$this->data,true);
        // unpaid_voucher is unpaid_voucher.php file in view directory and $data variable has infor mation that you want to render on view.
        $this->load->library('m_pdf');
        $this->m_pdf->pdf->WriteHTML($html);
        //download it D save F.
        $this->m_pdf->pdf->Output("./uploads/".$filename, "F");

		
//        //load the view and saved it into $html variable
//        $html = $this->load->view('admin/prints/commande', $this->data, true);
//        //this the the PDF filename that user will get to download
//        $pdfFilePath = "output_pdf_name.pdf";
//        //load mPDF library
//        $this->load->library('mpdf');
//        //generate the PDF from the given html
//        $this->mpdf->pdf->WriteHTML($html);
//        //download it.
//        $this->mpdf->pdf->Output($pdfFilePath, "D"); 
        
        
        //$this->load->helper('pdfexport');

        //$data['title'] = "Annual Report"; // it can be any variable with content that the code will use
        
        //$fileName = date('YmdHis') . "_report";
        //$pdfView  = $this->load->view('admin/prints/commande', $data, TRUE); // we need to use a view as PDF content
        
        //exportMeAsMPDF($fileName, $pdfView, '', 'P'); // then define the content and filename
    }

}
