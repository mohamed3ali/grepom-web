<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Donateurs extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('donateur_model');
    }

    public function index() 
    {
		$this->data['titre'] = "Comptabilité / Donateurs";
		$this->data['entityname'] = "donateur";
		$this->data['ajaxurl'] = "donateurs/json";
		$this->data['css'] = '';
		$this->data['scripts'] = '';
		$this->data['zonescripts'] = 'admin/compta/donateurs/dt';
		$this->data['zonecontenu'] = "admin/compta/donateurs/view";
		$this->data['zonemodals'] = "admin/compta/donateurs/modals";
		$this->load->view('admin/layout',$this->data);
    }
     
    public function json() 
    {
        $list = $this->donateur_model->get_all();
        //var_dump($list);die();

        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //var_dump($item);die();
                $row = array();

                $row[] = $item->donateur;
                $row[] = $item->code;
                $row[] = $item->description;
                
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier</a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function ajax_edit($id)
    {
        $this->data = $this->donateur_model
                            ->get($id);
        //print_r($this->data);
        echo json_encode($this->data);
    }
 
    public function ajax_add()
    {
        $imid = $this->donateur_model->insert(array(
            'donateur' => $this->input->post('donateur'),
            'code' => $this->input->post('code'),
            'description' => $this->input->post('description')
        ));

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur d'enregistrement !"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Enregistrement réussi."));
        }
    }
 
    public function ajax_update($mid)
    {
        $imid = $this->donateur_model->update(array(
            'donateur' => $this->input->post('donateur'),
            'code' => $this->input->post('code'),
            'description' => $this->input->post('description'),
        ), $mid);

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur de mise à jour !"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Mise à jour réussi."));
        }
    }
 
    public function ajax_delete($id)
    {
        if ($this->donateur_model->delete($id) == 0) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }
 
}
