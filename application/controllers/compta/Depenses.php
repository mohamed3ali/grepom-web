<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Depenses extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();

        $this->load->model('depense_model');
    }

    public function index() 
    {
		$this->data['titre'] = "Comptabilité / Dépenses";
		$this->data['entityname'] = "depense";
		$this->data['ajaxurl'] = "depenses/json";
		$this->data['css'] = '';
		$this->data['scripts'] = '';
        $this->data['comboModesPaiement'] = $this->comboArray("modepaiements","id", "designation");
        $this->data['comboRubriques'] = $this->comboArray("depensesnomenclatures","id", "designation");
        $this->data['comboDevises'] = $this->comboArray("devises","id", "monnaie");
        $this->data['comboCommandes'] = $this->comboArray("commandes","id", "numCommande");
        $this->data['comboProjets'] = $this->comboArray("projets","id", "CONCAT( code, ' / ', titre ) AS projet");
        $this->data['comboJustificatifs'] = $this->comboArray("justificatifs","id", "designation");
        
		$this->data['zonescripts'] = 'admin/compta/depenses/dt';
		$this->data['zonecontenu'] = "admin/compta/depenses/view";
		$this->data['zonemodals'] = "admin/compta/depenses/modals";
		$this->load->view('admin/layout',$this->data);
    }
     
    public function json() 
    {
        $list = $this->depense_model
                        ->with('devise')
                        ->with('depensesnomenclature')
                        ->with('modepaiement')
                        ->with('projet')
                        ->with('commande')
                        ->with('justificatif')
                        ->get_all();
        //var_dump($list);die();

        $this->data = array();
        $no = (isset($_POST['start']))?$_POST['start']:null;
        if($list){
            foreach ($list as $item) {
                $no++;
                //var_dump($item);die();
                $row = array();

                $row[] = $item->nature;
                $row[] = isset($item->devise)?$item->montant." ".$item->devise->code:$item->montant;
                $row[] = $item->modepaiement->designation;
                //$row[] = isset($item->devise)?$item->devise->code:"";
                $row[] = isset($item->depensesnomenclature)?$item->depensesnomenclature->code:"";
                $row[] = $item->projet->code .' / '. $item->projet->titre;
                $row[] = $item->beneficiaire;
                $row[] = $item->date;
                $row[] = $item->num_facture;
                $row[] = isset($item->commande)?$item->commande->numCommande:"";
                $row[] = $item->justificatif->designation." N°".$item->num_facture;
                
                $row[] = '
                <div class="dropdown btn-group">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options <span class="caret"></span></button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" title="Modifier" onclick="edit_entity('."'".$item->id."'".')">Modifier</a>
                        <a class="dropdown-item" href="javascript:void(0)" title="Supprimer" onclick="delete_entity('."'".$item->id."'".')">Supprimer</a>
                    </div>
                </div>';
                
                $this->data[] = $row;
            }
        }

        $output = array(
            "data" => $this->data,
        );
        
        echo json_encode($output);
    }
     
    public function ajax_edit($id)
    {
        $this->data = $this->depense_model
                            ->with('devise')
                            ->with('depensesnomenclature')
                            ->with('modepaiement')
                            ->with('commande')
                            ->get($id);
        //print_r($this->data);
        echo json_encode($this->data);
    }
 
    public function ajax_add()
    {
        $imid = $this->depense_model->insert(array(
            'montant' => $this->input->post('montant'),
            'devise_id' => $this->input->post('devise_id'),
            'modepaiement_id' => $this->input->post('modepaiement_id'),
            'depensesnomenclature_id' => $this->input->post('depensesnomenclature_id'),
            'nature' => $this->input->post('nature'),
            'source' => $this->input->post('source'),
            'beneficiaire' => $this->input->post('beneficiaire'),
            'date' => $this->input->post('date'),
            'commande_id' => $this->input->post('commande_id'),
            'num_facture' => $this->input->post('num_facture'),
            'justificatif_id' => $this->input->post('justificatif_id')
        ));

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur d'enregistrement !"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Enregistrement réussi."));
        }
    }
 
    public function ajax_update($mid)
    {
        $imid = $this->depense_model->update(array(
            'montant' => $this->input->post('montant'),
            'devise_id' => $this->input->post('devise_id'),
            'modepaiement_id' => $this->input->post('modepaiement_id'),
            'depensesnomenclature_id' => $this->input->post('depensesnomenclature_id'),
            'nature' => $this->input->post('nature'),
            'source' => $this->input->post('source'),
            'beneficiaire' => $this->input->post('beneficiaire'),
            'date' => $this->input->post('date'),
            'commande_id' => $this->input->post('commande_id'),
            'num_facture' => $this->input->post('num_facture'),
            'justificatif_id' => $this->input->post('justificatif_id')
        ), $mid);

        if($imid === false){
            echo json_encode(array("status" => FALSE,"msg" => "Erreur de mise à jour !"));
        }else{
            echo json_encode(array("status" => TRUE,"msg" => "Mise à jour réussi."));
        }
    }
 
    public function ajax_delete($id)
    {
        if ($this->depense_model->delete($id) == 0) 
            echo json_encode(array("status" => FALSE));
        else  
            echo json_encode(array("status" => TRUE));
    }
 
}
