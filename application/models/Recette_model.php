<?php

class Recette_model extends MY_Model {

	public function __construct()
	{

		$this->timestamps = false;

        $this->soft_deletes = FALSE;

		//$this->has_one['personne'] = array('personne_model','id','receptionpar_id');
		//$this->has_one['adherent'] = array('adherent_model','id','adherent_id');
        $this->has_one['modepaiement'] = array('modepaiement_model','id','modepaiement_id');
        $this->has_one['devise'] = array('devise_model','id','devise_id');
        $this->has_one['projet'] = array('projet_model','id','type_id');
        $this->has_one['donateur'] = array('donateur_model','id','type_id');
        $this->has_one['fondsbailleur'] = array('fondsbailleur_model','id','type_id');
		
		parent::__construct();
 	}
    
    public function totalRecettes()
    {
        //$this->db->select('SELECT annee_id, sum(montant) FROM cotisations GROUP BY annee_id', FALSE);
        $this->db->select('annee_id,'); 
        $this->db->select_sum('montant'); 
        $this->db->group_by('annee_id'); 
        //$this->db->from('cotisations');
        $query = $this->db->get('cotisations');
        //var_dump($query->result());die();
        return $query->result();
    }	
    public function adherantCount()
    {
        $this->db->select('annee_id, COUNT(montant) as total'); 
        $this->db->group_by('annee_id'); 
        //$this->db->from('cotisations');
        $query = $this->db->get('cotisations');
        //var_dump($query->result());die();
        return $query->result();
    }
    public function adherantsPro()
    {
        $query = $this->db->query('Select pr.nom_profession as "profession", count(p.id) as "count"
                                    from personnes as p 
                                    left join professions as pr on p.profession_id = pr.id 
                                    group by profession_id');
                                    //order by count desc
        //var_dump($query->result());die();
        return $query->result();
    }
    
}