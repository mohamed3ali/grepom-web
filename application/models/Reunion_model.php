<?php

class Reunion_model extends MY_Model
{
	public function __construct()
	{
		$this->timestamps = false;

        $this->soft_deletes = FALSE;

		$this->has_one['type'] = array('reuniontype_model','id','reuniontype_id');
		$this->has_one['sujet'] = array('reunionsujet_model','id','sujet_id');
		
		
		parent::__construct();
 	}
}