<?php

class Document_model extends MY_Model
{
    public $table = 'documents';
	public function __construct()
	{
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;

		$this->has_one['doccat'] = array('docsouscat_model','id','cat_id');

		parent::__construct();
 	}

}