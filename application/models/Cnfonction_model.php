<?php

class Cnfonction_model extends MY_Model
{
    
	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		
		$this->has_many['cnmembres'] = array(
			'foreign_model'=>'cnmembre_model',
			'foreign_table'=>'cnmembres',
			'foreign_key'=>'cnfonction_id',
			'local_key'=>'id'
			);

		parent::__construct();
 	}

}