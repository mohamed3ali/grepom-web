<?php

class Depense_model extends MY_Model
{
	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;

		$this->has_one['depensesnomenclature'] = array('depensesnomenclature_model','id','depensesnomenclature_id');
		$this->has_one['devise'] = array('devise_model','id','devise_id');
		$this->has_one['modepaiement'] = array('modepaiement_model','id','modepaiement_id');
		$this->has_one['commande'] = array('commande_model','id','commande_id');
		$this->has_one['projet'] = array('projet_model','id','source');
		$this->has_one['justificatif'] = array('justificatif_model','id','justificatif_id');
		
		parent::__construct();
 	}
	
	public function get_depense($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('depenses');
		return $query->result();
	}
	
	public function delete_depense($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('depenses');
	}
	
	public function update_depense($id, $data)
	{
		$this->db->where('id',$id);
		$this->db->update('depenses',$data);
	}

}