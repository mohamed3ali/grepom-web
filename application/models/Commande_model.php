<?php

class Commande_model extends MY_Model
{
	public function __construct()
	{
		
		$this->timestamps = false;
        $this->soft_deletes = FALSE;

		$this->has_many['lignescommande'] = array(
			'foreign_model'=>'lignecommande_model',
			'foreign_table'=>'lignes_commande',
			'foreign_key'=>'commande_id',
			'local_key'=>'id'
		);
		$this->has_many['commandebons'] = array(
			'foreign_model'=>'commandebon_model',
			'foreign_table'=>'commandebons',
			'foreign_key'=>'commande_id',
			'local_key'=>'id'
		);
		parent::__construct();
 	}
 	
	/*public function get_all()
	{
		$query = $this->db->get('commande');
		return $query->result();
	}*/
	
	public function get_grand_total($cid)
	{
	    $this->db->select('ROUND((SUM(pu * qte)), 2) AS grand_total', FALSE);
	    $this->db->from('lignes_commande');
	
	    $this->db->where('commande_id', $cid);
	    $query = $this->db->get();
    	
    	return $query->result();
	}
    
	public function get_commande($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('commandes');
		return $query->result();
	}
	
	public function delete_commande($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('commandes');
	}
	
	public function update_commande($id, $data)
	{
		$this->db->where('id',$id);
		$this->db->update('commandes',$data);
	}

}