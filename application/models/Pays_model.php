<?php

class Pays_model extends MY_Model
{
	public $primary_key = 'Code';

	public function __construct()
	{
		
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;
		
		$this->has_many['villes'] = array(
			'foreign_model'=>'Ville_model',
			'foreign_table'=>'villes',
			'foreign_key'=>'Country',
			'local_key'=>'Code');

		$this->has_many['personnes'] = array(
			'foreign_model'=>'Personne_model',
			'foreign_table'=>'personnes',
			'foreign_key'=>'pays_id',
			'local_key'=>'Code');

		parent::__construct();
 	}
 	

}