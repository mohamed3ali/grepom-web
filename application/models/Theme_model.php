<?php

class Theme_model extends MY_Model
{
    
	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		
		$this->has_many['sujet'] = array('foreign_model'=>'sujet_model','foreign_table'=>'sujets','foreign_key'=>'theme_id','local_key'=>'id');

		parent::__construct();
 	}

}