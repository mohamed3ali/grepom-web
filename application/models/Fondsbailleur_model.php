<?php

class Fondsbailleur_model extends MY_Model
{
	public function __construct()
	{
		
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;

		parent::__construct();
 	}
 	
	/*public function get_all()
	{
		$query = $this->db->get('fondsbailleur');
		return $query->result();
	}*/
	
	public function get_fondsbailleur($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('fondsbailleurs');
		return $query->result();
	}
	
	public function delete_fondsbailleur($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('fondsbailleurs');
	}
	
	public function update_fondsbailleur($id, $data)
	{
		$this->db->where('id',$id);
		$this->db->update('fondsbailleurs',$data);
	}

}