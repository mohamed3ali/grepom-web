<?php
class Auth_model extends MY_Model {
    
    var $details;

    function validate( $email, $password ) {
        // Build a query to retrieve the user's details
        // based on the received username and password
        $this->db->from('moderateurs');
        $this->db->where('email',$email );
        $this->db->where( 'password', sha1($password) );
        $login = $this->db->get()->result();
        // The results of the query are stored in $login.
        // If a value exists, then the user account exists and is validated
        if ( is_array($login) && count($login) == 1 ) {
            // Set the users details into the $details property of this class
            $this->details = $login[0];
            // Call set_session to set the user's session vars via CodeIgniter
            $this->set_session();
            return true;
        }
        return false;
    }
    function set_session() {
        // session->set_userdata is a CodeIgniter function that
        // stores data in CodeIgniter's session storage.  Some of the values are built in
        // to CodeIgniter, others are added.  See CodeIgniter's documentation for details.
        $this->session->set_userdata( array(
                'id'=>$this->details->id,
                'login'=> $this->details->login,
                'email'=>$this->details->email,
                'isLoggedIn'=>true
            )
        );
    }
    function  create( $userData ) {
      $data['login'] = $userData['login'];
      $data['email'] = $userData['email'];
      $data['password'] = sha1($userData['password1']);
      return $this->db->insert('moderateurs',$data);
    }
}