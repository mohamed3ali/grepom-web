<?php

class Devise_model extends MY_Model
{
	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		$this->has_many['depenses'] = array(
			'foreign_model'=>'depense_model',
			'foreign_table'=>'depenses',
			'foreign_key'=>'devise_id',
			'local_key'=>'id'
		);
		parent::__construct();
 	}
 	
	/*public function get_all()
	{
		$query = $this->db->get('devise');
		return $query->result();
	}*/
	
	public function get_devise($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('devises');
		return $query->result();
	}
	
	public function delete_devise($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('devises');
	}
	
	public function update_devise($id, $data)
	{
		$this->db->where('id',$id);
		$this->db->update('devises',$data);
	}

}