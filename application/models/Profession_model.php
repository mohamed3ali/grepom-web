<?php

class Profession_model extends MY_Model
{
    
	public function __construct()
	{
		
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		
		$this->has_many['personnes'] = array(
			'foreign_model'=>'Personne_model',
			'foreign_table'=>'personnes',
			'foreign_key'=>'profession_id',
			'local_key'=>'id');

		parent::__construct();
 	}
 	

}