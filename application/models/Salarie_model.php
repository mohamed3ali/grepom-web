<?php

class Salarie_model extends MY_Model
{
	public function __construct()
	{

		$this->timestamps = false;

        $this->soft_deletes = FALSE;

		$this->has_one['personne'] = array('personne_model','id','personne_id');
		$this->has_one['fonction'] = array('salariefonction_model','id','salariefonction_id');
		$this->has_one['statut'] = array('salariestatut_model','id','salariestatut_id');

		parent::__construct();
 	}
}