<?php

class Sujet_model extends MY_Model
{
    
	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		
		$this->has_one['theme'] = array('theme_model','id','theme_id');
		$this->has_many['reunion'] = array(
			'foreign_model'=>'reunion_model',
			'foreign_table'=>'reunions',
			'foreign_key'=>'sujet_id',
			'local_key'=>'id'
			);

		parent::__construct();
 	}

}