<?php

class Matcat_model extends MY_Model
{
    public $table = 'mat_cat';
	public function __construct()
	{
		
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		
		$this->has_many['matsouscats'] = array('foreign_model'=>'Matsouscat_model','foreign_table'=>'mat_sous_cat','foreign_key'=>'cat_id','local_key'=>'id');

		parent::__construct();
 	}
 	

}