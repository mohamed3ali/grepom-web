<?php

class Materiel_model extends MY_Model
{
    public $table = 'materiels';
	public function __construct()
	{
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;

		$this->has_one['matcat'] = array('matsouscat_model','id','cat_id');

		parent::__construct();
 	}

}