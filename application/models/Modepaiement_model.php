<?php

class Modepaiement_model extends MY_Model
{
	public function __construct()
	{
		
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;

		$this->has_many['cotisations'] = array(
			'foreign_model'=>'cotisation_model',
			'foreign_table'=>'cotisations',
			'foreign_key'=>'modepaiement_id',
			'local_key'=>'id'
		);
		
		parent::__construct();
 	}
 	

}