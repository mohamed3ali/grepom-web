<?php
class Moderateurs_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function validCredentials($login,$psw){
	     $this->load->library('encrypt');
	
	     $psw = $this->encrypt->sha1($psw);
	
	     $q = "SELECT * FROM moderateurs WHERE email = ? AND psw = ?";
	
	     $data = array($login,$psw);
	     $q = $this->db->query($q,$data);
	
	     if($q->num_rows() > 0){
	          $r = $q->result();
	          //$session_data = array('m_login' => $r[0]->login,'m_id' => $r[0]->id,'m_email' => $r[0]->email,'m_logged_in' => true);
	          $session_data = array('data' => $r[0],'m_logged_in' => true);
	          $this->session->set_userdata($session_data);
	          //var_dump($session_data);die();
	          return true;
	     } else { return false; }
	}
	function isLoggedIn(){
	     if($this->session->userdata('m_logged_in'))
	     { return true; } else { return false; }
	}
	
	function read()
	{
		$q = $this->db->get('moderateurs');
		if ($q->num_rows()>0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
	}
	
	function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('moderateurs');
	}
	
	function update($id,$data)
	{
		$this->db->where('id',$id);
		return $this->db->update('moderateurs', $data);
	}
	
	function get_mod($id)
	{
		$this->db->where('id', $id);
		$q = $this->db->get('moderateurs');
		if ($q->num_rows >0)
		{
			return $q->row();
		}
	}
	
	function create($data)
	{
		$this->db->insert('moderateurs', $data);
	}

}
?>