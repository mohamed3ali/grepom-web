<?php

class Chapitre_model extends MY_Model
{

	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;

		$this->has_one['procedure'] = array('procedure_model','id','procedure_id');
		$this->has_one['parent'] = array('chapitre_model','parent_id','id');

		$this->has_many['chapitres'] = array(
			'foreign_model'=>'chapitre_model',
			'foreign_table'=>'chapitres',
			'foreign_key'=>'parent_id',
			'local_key'=>'id'
		);

		parent::__construct();
 	}
}