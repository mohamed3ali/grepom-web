<?php

class Salariefonction_model extends MY_Model
{
    
	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		
		$this->has_many['salarie'] = array('foreign_model'=>'salarie_model','foreign_table'=>'salaries','foreign_key'=>'salariefonction_id','local_key'=>'id');

		parent::__construct();
 	}

}