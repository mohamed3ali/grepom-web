<?php

class Justificatif_model extends MY_Model
{
    
	public function __construct()
	{
		
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		
		$this->has_many['depense'] = array('foreign_model'=>'Depense_model','foreign_table'=>'depenses','foreign_key'=>'justificatif_id','local_key'=>'id');

		parent::__construct();
 	}
 	

}