<?php

class Docsouscat_model extends MY_Model
{
    public $table = 'doc_sous_cat';
	public function __construct()
	{
		
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;

		$this->has_one['doccat'] = array('doccat_model','id','cat_id');

		$this->has_many['documents'] = array(
			'foreign_model'=>'document_model',
			'foreign_table'=>'documents',
			'foreign_key'=>'cat_id',
			'local_key'=>'id'
		);
		parent::__construct();
 	}
 	

}