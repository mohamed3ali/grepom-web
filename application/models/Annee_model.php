<?php

class Annee_model extends MY_Model
{
	public $primary_key = 'annee';
	public function __construct()
	{
		
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;

		$this->has_many['cotisations'] = array(
			'foreign_model'=>'cotisation_model',
			'foreign_table'=>'cotisations',
			'foreign_key'=>'annee_id',
			'local_key'=>'annee'
		);
		
		$this->has_many['cnmembres'] = array(
			'foreign_model'=>'cnmembre_model',
			'foreign_table'=>'cnmembres',
			'foreign_key'=>'mandat',
			'local_key'=>'annee'
		);
		
		parent::__construct();
 	}
 	

}