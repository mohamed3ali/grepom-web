<?php

class Adherenttype_model extends MY_Model
{
    
	public function __construct()
	{
		
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		
		$this->has_many['adherent'] = array('foreign_model'=>'Adherent_model','foreign_table'=>'adherents','foreign_key'=>'adherenttype_id','local_key'=>'id');

		parent::__construct();
 	}
 	

}