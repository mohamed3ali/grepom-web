<?php

class Matsouscat_model extends MY_Model
{
    public $table = 'mat_sous_cat';
	public function __construct()
	{
		
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;

		$this->has_one['matcat'] = array('matcat_model','id','cat_id');

		$this->has_many['materiels'] = array(
			'foreign_model'=>'materiel_model',
			'foreign_table'=>'materiels',
			'foreign_key'=>'cat_id',
			'local_key'=>'id'
		);
		parent::__construct();
 	}
 	

}