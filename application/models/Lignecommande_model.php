<?php

class Lignecommande_model extends MY_Model
{
	public $table = 'lignes_commande';
	
	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		$this->has_one['commande'] = array('commande_model','id','commande_id');

		parent::__construct();
 	}
	
}