<?php

class Cotisationtype_model extends MY_Model
{
	public function __construct()
	{
		
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;

		$this->has_many['cotisations'] = array(
			'foreign_model'=>'cotisation_model',
			'foreign_table'=>'cotisations',
			'foreign_key'=>'cotisationtype_id',
			'local_key'=>'id'
		);
		parent::__construct();
 	}
 	
	/*public function get_all()
	{
		$query = $this->db->get('cotisationtypes');
		return $query->result();
	}*/
	
	public function get_cotisationtypes($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('cotisationtypes');
		return $query->result();
	}
	
	public function delete_cotisationtype($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('cotisationtypes');
	}
	
	public function update_cotisationtype($id, $data)
	{
		$this->db->where('id',$id);
		$this->db->update('cotisationtypes',$data);
	}

}