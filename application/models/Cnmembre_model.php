<?php

class Cnmembre_model extends MY_Model
{	
	public function __construct()
	{
		
		$this->timestamps = false;

        $this->soft_deletes = FALSE;

		$this->has_one['adherent'] = array('adherent_model','id','adherent_id');
		$this->has_one['cnfonction'] = array('cnfonction_model','id','cnfonction_id');
		$this->has_one['mandat'] = array('annee_model','annee','mandat');

		parent::__construct();
 	}

 	public function mandats()
	{
		$mandats = $this->db->query('SELECT DISTINCT mandat FROM cnmembres');
		$options = array(""=>"Tous");
		foreach ($mandats->result() as $mandat)
		{
		    $options[$mandat->mandat]=$mandat->mandat;
		}
        return $options;
 	}
}