<?php

class Depensesnomenclature_model extends MY_Model
{
	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		$this->has_many['depenses'] = array(
			'foreign_model'=>'depense_model',
			'foreign_table'=>'depenses',
			'foreign_key'=>'depensesnomenclature_id',
			'local_key'=>'id'
		);
		parent::__construct();
 	}
 	
	/*public function get_all()
	{
		$query = $this->db->get('depensesnomenclature');
		return $query->result();
	}*/
	
	public function get_depensesnomenclature($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('depensesnomenclatures');
		return $query->result();
	}
	
	public function delete_depensesnomenclature($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('depensesnomenclatures');
	}
	
	public function update_depensesnomenclature($id, $data)
	{
		$this->db->where('id',$id);
		$this->db->update('depensesnomenclatures',$data);
	}

}