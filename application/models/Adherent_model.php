<?php

class Adherent_model extends MY_Model
{
	public function __construct()
	{
		
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;

		$this->has_one['personne'] = array('personne_model','id','personne_id');
		//$this->has_one['adherenttype'] = array('adherent_model','id','adherenttype_id');
		$this->has_one['adherenttype'] = array('foreign_model'=>'Adherenttype_model','foreign_table'=>'adherenttypes','foreign_key'=>'id','local_key'=>'adherenttype_id');
		
		$this->has_many['cotisations'] = array(
			'foreign_model'=>'cotisation_model',
			'foreign_table'=>'cotisations',
			'foreign_key'=>'adherent_id',
			'local_key'=>'id'
		);
		parent::__construct();
 	}
 	

}