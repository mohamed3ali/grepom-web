<?php

class Cotisation_model extends MY_Model {

	public function __construct()
	{

		$this->timestamps = false;

        $this->soft_deletes = FALSE;

		$this->has_one['personne'] = array('personne_model','id','receptionpar_id');
		$this->has_one['adherent'] = array('adherent_model','id','adherent_id');
		$this->has_one['cotisationtype'] = array('cotisationtype_model','id','cotisationtype_id');
        $this->has_one['modepaiement'] = array('modepaiement_model','id','modepaiement_id');
        $this->has_one['annee'] = array('annee_model','annee','annee_id');
		
		parent::__construct();
 	}
    
    public function totalCotisations()
    {
        //$this->db->select('SELECT annee_id, sum(montant) FROM cotisations GROUP BY annee_id', FALSE);
        $this->db->select('annee_id,'); 
        $this->db->select_sum('montant'); 
        $this->db->group_by('annee_id'); 
        //$this->db->from('cotisations');
        $query = $this->db->get('cotisations');
        //var_dump($query->result());die();
        return $query->result();
    }	
    public function adherantCount()
    {
        $this->db->select('annee_id, COUNT(montant) as total'); 
        $this->db->group_by('annee_id'); 
        //$this->db->from('cotisations');
        $query = $this->db->get('cotisations');
        //var_dump($query->result());die();
        return $query->result();
    }
    public function adherantsPro()
    {
        $query = $this->db->query('Select pr.nom_profession as "profession", count(p.id) as "count"
from personnes as p 
left join professions as pr on p.profession_id = pr.id 
group by profession_id');
//order by count desc
        //var_dump($query->result());die();
        return $query->result();
    }
/*    var $table = 'cotisations';
    var $column_order = array('cotisations.id as cotisationId ','numrecu','adherent_id','saison','typecotisation_id','modecotisation_id','montant','receptionpar_id','remarques',null); //set column field database for datatable orderable
    var $column_search = array('numrecu','remarques'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('cotisations.id' => 'desc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {
        //$this->db->select('cotisations.id as id','cotisations.numrecu as numrecu','adherents.cin as cin','cotisations.saison','cotisations.typecotisation_id','cotisations.modecotisation_id','cotisations.montant','cotisations.receptionpar_id','cotisations.remarques');
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('adherents', 'adherents.id = '.$this->table.'.adherent_id');
        $this->db->join('cotisationmodes', 'cotisationmodes.id = '.$this->table.'.modecotisation_id');
        $this->db->join('cotisationtypes', 'cotisationtypes.id = '.$this->table.'.typecotisation_id');
        $this->db->join('personnes', 'personnes.cin = '.$this->table.'.receptionpar_id');

        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if(isset($_POST['search']) && $_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(isset($_POST['length']) && ($_POST['length'] != -1))
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        var_dump($query->result());die();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $query = $this->db->get();
 
        return $query->row();
    }
 
    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
 
    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
 
    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
 
 */
}