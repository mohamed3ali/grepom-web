<?php

class Ville_model extends MY_Model
{
	public $primary_key = 'Name';

	public function __construct()
	{
		
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;

		$this->has_one['pays'] = array('pays_model','Code','Country');
		
		$this->has_many['personnes'] = array(
			'foreign_model'=>'Personne_model',
			'foreign_table'=>'personnes',
			'foreign_key'=>'ville_id',
			'local_key'=>'Name');

		parent::__construct();
 	}

 	public function pays_nom(){
        $this->load->model('pays_model');
 		return $this->pays_model->get($this->Country)->Name;
 	}
 	

}