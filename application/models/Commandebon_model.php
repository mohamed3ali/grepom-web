<?php

class Commandebon_model extends MY_Model
{
	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		$this->has_one['commande'] = array('commande_model','id','commande_id');

		parent::__construct();
 	}
 	
	/*public function get_all()
	{
		$query = $this->db->get('commandebon');
		return $query->result();
	}*/
	
	public function get_commandebon($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('commandebons');
		return $query->result();
	}
	
	public function delete_commandebon($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('commandebons');
	}
	
	public function update_commandebon($id, $data)
	{
		$this->db->where('id',$id);
		$this->db->update('commandebons',$data);
	}

}