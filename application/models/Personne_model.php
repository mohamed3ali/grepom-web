<?php

class Personne_model extends MY_Model
{
    //public $primary_key = 'cin';
    
	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;

		$this->has_one['identitetype'] = array('identitetype_model','id','identitetype_id');
		$this->has_one['profession'] = array('profession_model','id','profession_id');
		$this->has_one['adherent'] = array('adherent_model','personne_id','id');
		$this->has_one['salarie'] = array('salarie_model','personne_id','id');
		$this->has_one['ville'] = array('ville_model','Name','ville_id');
		$this->has_one['pays'] = array('pays_model','Code','pays_id');

		$this->has_many['cotisations'] = array(
			'foreign_model'=>'cotisation_model',
			'foreign_table'=>'cotisations',
			'foreign_key'=>'receptionpar_id',
			'local_key'=>'id'
		);

		parent::__construct();
 	}
}