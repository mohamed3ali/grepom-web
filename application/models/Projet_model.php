<?php

class Projet_model extends MY_Model
{
	public function __construct()
	{
		
		$this->timestamps = false;
		
        $this->soft_deletes = FALSE;

        $this->has_one['fondsbailleur'] = array('fondsbailleur_model','id','bf_id');
		parent::__construct();
 	}
 	
	/*public function get_all()
	{
		$query = $this->db->get('projet');
		return $query->result();
	}*/
	
	public function get_projet($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('projets');
		return $query->result();
	}
	
	public function delete_projet($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('projets');
	}
	
	public function update_projet($id, $data)
	{
		$this->db->where('id',$id);
		$this->db->update('projets',$data);
	}

}