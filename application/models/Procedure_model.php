<?php

class Procedure_model extends MY_Model
{

	public function __construct()
	{
		$this->timestamps = false;
        $this->soft_deletes = FALSE;

		//$this->has_one['reunion'] = array('aherant_model','personne_id','id');

		$this->has_many['chapitres'] = array(
			'foreign_model'=>'chapitre_model',
			'foreign_table'=>'chapitres',
			'foreign_key'=>'procedure_id',
			'local_key'=>'id'
		);
	
		//$this->has_many['chapitres'] = "chapitre_model";
		
		parent::__construct();
 	}
}