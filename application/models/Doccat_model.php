<?php

class Doccat_model extends MY_Model
{
    public $table = 'doc_cat';
	public function __construct()
	{
		
		$this->timestamps = false;
        $this->soft_deletes = FALSE;
		
		$this->has_many['docsouscats'] = array('foreign_model'=>'Docsouscat_model','foreign_table'=>'doc_sous_cat','foreign_key'=>'cat_id','local_key'=>'id');

		parent::__construct();
 	}
 	

}