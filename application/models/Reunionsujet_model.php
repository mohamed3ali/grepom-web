<?php

class Reunionsujet_model extends MY_Model
{
	public $table = 'sujets';
		
	public function __construct()
	{
		$this->timestamps = false;

        $this->soft_deletes = FALSE;
        
		$this->has_many['reunions'] = array(
			'foreign_model'=>'reunion_model',
			'foreign_table'=>'reunions',
			'foreign_key'=>'sujet_id',
			'local_key'=>'id'
		);

		parent::__construct();
 	}
}